﻿
    function enableSwipeRefreshLayout() {
        Android.enableSwipeRefreshLayout();
    }

    function disableSwipeRefreshLayout() {
        Android.disableSwipeRefreshLayout();
    }

    function touchmove(event) {
        console.log("touchmove=" + document.body.scrollTop)
    }

    function touchcancel(event) {
        console.log("touchcancel=" + document.body.scrollTop)
        if (typeof Android !== 'undefined'){
            if (document.body.scrollTop === 0) {
                console.log("enabling")
                enableSwipeRefreshLayout();
            } else {
                console.log("disabling")
                disableSwipeRefreshLayout();
                }
        }

    }

    //document.addEventListener('touchstart', handleTouchStart, false);
    //document.addEventListener('touchmove', touchmove, false);
    document.addEventListener('touchcancel', touchcancel, false);

  

