<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $organization = factory(Modules\Models\Organization::class)->create();

        $defineRoles = factory(Modules\Models\Rool::class)->create();

        $userWorker = factory(Modules\Models\User::class)->create();

        $userPatient = factory(Modules\Models\Patient::class, 25)->create()->each(
            function ($post) {
                $post->save();
            });

        foreach (['Indicaciones', 'Inserción', 'Mantenimiento'] as $key => $value) {
            $activeProcedures = Modules\Models\Procedure::create([
                'procedure_title' => $value,
            ]);
        }

        $faker = Faker\Factory::create();

        for ($i = 0; $i < 20; $i++) {
            $activityLists = Modules\Models\Activity::create([
                'activity_title'       => $faker->text(100),
                'activity_type'        => 0, #rand(0, 1),
                'activity_id'          => rand(1, 3),
                'activity_recordatory' => 0,
                'activity_level'       => 0,
            ]);
        }

        foreach (['Operación', 'UCI', 'UTI'] as $key => $value) {
            $activeProcedures = Modules\Models\Room::create([
                'room_name'     => $value,
                'room_identity' => rand(1111, 9999),
            ]);
        }
    }
}
