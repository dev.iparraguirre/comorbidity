<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Modules\Models\Organization::class, function (Faker $faker) {
    $titleOrganization = $faker->name;

    return [
        'organization_name' => $titleOrganization,
        'organization_title' => $titleOrganization,
        'logo' => '',
        'background_color' => '',
    ];
});

$factory->define(Modules\Models\Rool::class, function (Faker $faker) {
    $listLevels = array('enfermeras');

    return [
        'rol_title' => 'enfermeras',
        'rol_level' => 2,
        'organization' => 1,
    ];
});

$factory->define(Modules\Models\User::class, function (Faker $faker) {
    static $password;

    return [
        'user_firstname' => $faker->name,
        'user_lastname' => $faker->name,
        'user_email' => $faker->unique()->safeEmail,
        'user_picture' => '',
        'user_type' => 1,
        'main_organization' => 1,
        'last_login' => \Carbon\Carbon::today(),
        'user_password' => $password ?: $password = bcrypt('secret'),
    ];
});

