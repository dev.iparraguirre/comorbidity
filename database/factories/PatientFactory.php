<?php

use Faker\Generator as Faker;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Modules\Models\Room::class, function (Faker $faker) {

    return [
        'room_identity' => $faker->title,
        'room_name' => $faker->name,
    ];
});

$factory->define(Modules\Models\Activity::class, function (Faker $faker) {

    return [
        'activity_title' => $faker->title,
        'activity_type' => 0,
    ];
});

$factory->define(Modules\Models\Patient::class, function (Faker $faker) {
    $patient_identity = str_replace(' ', 'j', substr($faker->name, 0, 5)).'-'.rand(1111,9999);

    return [
        'patient_identity' => $patient_identity,
        'patient_firstname' => $faker->name,
        'patient_age' => 23,
        'patient_gender' => 'M',
        'last_interaction' => \Carbon\Carbon::today(),
    ];
});
