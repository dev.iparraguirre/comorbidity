-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: co_morbidity
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meg_activity_procedures`
--

DROP TABLE IF EXISTS `meg_activity_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_activity_procedures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activity_recordatory` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `activity_id` int(10) unsigned NOT NULL,
  `activity_level` int(11) NOT NULL,
  `activity_type` int(11) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_procedures_id_index` (`id`),
  KEY `activity_procedures_activity_id_foreign` (`activity_id`),
  CONSTRAINT `activity_procedures_activity_id_foreign` FOREIGN KEY (`activity_id`) REFERENCES `meg_procedures` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_activity_procedures`
--

LOCK TABLES `meg_activity_procedures` WRITE;
/*!40000 ALTER TABLE `meg_activity_procedures` DISABLE KEYS */;
INSERT INTO `meg_activity_procedures` VALUES (1,'indicaciones','0',1,1,1,'2017-11-06 16:37:19','2017-11-06 19:57:31'),(2,'mantenimiento','0',3,2,1,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(3,'Iure ut quibusdam voluptas voluptas voluptas. Est provident ipsa veritatis modi recusandae.','0',3,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(4,'Et dolore qui ut. Mollitia repudiandae et ut recusandae aut dolores.','0',1,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(5,'Deserunt ea neque omnis et et fugiat. Repellendus animi voluptas sint quod.','0',3,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(6,'Ut ipsa saepe ducimus sit voluptatem veniam amet rerum. Ut et quasi ut quam quia quos deserunt.','0',3,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(7,'Totam fugit quis odio doloribus est. Nobis aliquam deleniti iste.','0',3,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(8,'Totam necessitatibus suscipit sit esse labore. Provident eius culpa aut.','0',3,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(9,'Doloribus voluptas eaque iure quidem. Aut et non ullam nostrum.','0',1,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(10,'Neque nihil perferendis dolores laboriosam amet. Sed qui cumque neque esse voluptas ea quas.','0',3,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(11,'Labore ex nesciunt sed molestiae enim minima aperiam omnis. Id repellat nihil possimus ut autem.','0',3,0,0,'2017-11-06 16:37:19','2017-11-06 16:37:19'),(12,'Vel praesentium explicabo ut. Dolores ullam repudiandae blanditiis adipisci cupiditate.','0',3,0,0,'2017-11-06 16:37:20','2017-11-06 16:37:20'),(13,'Voluptas fugiat ad reiciendis aliquid non. Pariatur enim eius dolorem qui inventore soluta.','0',1,0,0,'2017-11-06 16:37:20','2017-11-06 16:37:20'),(14,'Rerum ad magnam enim saepe veritatis vitae. Est ipsam rem quae nihil.','0',1,0,0,'2017-11-06 16:37:20','2017-11-06 16:37:20'),(15,'Accusantium minus aspernatur sunt et ad. Quod et aut at incidunt. Maiores a iure doloremque sunt.','0',2,0,0,'2017-11-06 16:37:20','2017-11-06 16:37:20'),(16,'Aliquid voluptas inventore natus saepe voluptas odit commodi. Laboriosam maxime sapiente id.','0',2,0,0,'2017-11-06 16:37:20','2017-11-06 16:37:20'),(17,'Commodi laborum nisi aperiam dolorem. Sed rerum commodi ad optio. Dolorem esse ab at aut.','0',3,0,0,'2017-11-06 16:37:20','2017-11-06 16:37:20'),(18,'Fugit dolore modi asperiores voluptatem harum sit ad. Unde iusto commodi quia accusamus.','0',2,0,0,'2017-11-06 16:37:20','2017-11-06 16:37:20'),(19,'inserccion','0',2,1,1,'2017-11-06 16:37:20','2017-11-06 16:37:20'),(20,'Deleniti possimus illo eius est eos et quos. Animi quam accusamus omnis eligendi.','0',3,0,0,'2017-11-06 16:37:20','2017-11-06 16:37:20');
/*!40000 ALTER TABLE `meg_activity_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_alerts_activities`
--

DROP TABLE IF EXISTS `meg_alerts_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_alerts_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `alert_title` varchar(250) COLLATE utf8mb4_unicode_ci NOT NULL,
  `alert_main` int(10) unsigned NOT NULL,
  `alert_type` int(11) NOT NULL,
  `alert_content` char(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `alerts_activities_id_index` (`id`),
  KEY `alerts_activities_alert_main_foreign` (`alert_main`),
  CONSTRAINT `alerts_activities_alert_main_foreign` FOREIGN KEY (`alert_main`) REFERENCES `meg_activity_procedures` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_alerts_activities`
--

LOCK TABLES `meg_alerts_activities` WRITE;
/*!40000 ALTER TABLE `meg_alerts_activities` DISABLE KEYS */;
INSERT INTO `meg_alerts_activities` VALUES (1,'',1,2,'',NULL,NULL),(2,'',2,2,'',NULL,NULL),(3,'',15,2,'',NULL,NULL),(4,'',16,2,'',NULL,NULL),(5,'',18,2,'',NULL,NULL),(6,'',19,2,'',NULL,NULL),(7,'',2,2,'',NULL,NULL),(8,'',3,2,'',NULL,NULL),(9,'',15,2,'',NULL,NULL),(10,'',16,2,'',NULL,NULL),(11,'',2,2,'',NULL,NULL),(12,'',19,2,'',NULL,NULL),(13,'',2,2,'',NULL,NULL),(14,'',19,2,'',NULL,NULL),(15,'',2,2,'',NULL,NULL),(16,'',2,2,'',NULL,NULL),(17,'',1,2,'',NULL,NULL),(18,'',4,2,'',NULL,NULL),(19,'',2,2,'',NULL,NULL),(20,'',3,2,'',NULL,NULL),(21,'',5,2,'',NULL,NULL),(22,'',15,2,'',NULL,NULL),(23,'',16,2,'',NULL,NULL),(24,'',1,2,'',NULL,NULL),(25,'',4,2,'',NULL,NULL),(26,'',15,2,'',NULL,NULL),(27,'',16,2,'',NULL,NULL),(28,'',19,2,'',NULL,NULL);
/*!40000 ALTER TABLE `meg_alerts_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_config_platform`
--

DROP TABLE IF EXISTS `meg_config_platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_config_platform` (
  `alerts_dashboard` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_config_platform`
--

LOCK TABLES `meg_config_platform` WRITE;
/*!40000 ALTER TABLE `meg_config_platform` DISABLE KEYS */;
INSERT INTO `meg_config_platform` VALUES ('1/1-2/3-19/2'),('');
/*!40000 ALTER TABLE `meg_config_platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_log_access`
--

DROP TABLE IF EXISTS `meg_log_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_log_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `main_user` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_access_id_index` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_log_access`
--

LOCK TABLES `meg_log_access` WRITE;
/*!40000 ALTER TABLE `meg_log_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `meg_log_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_logs_activities`
--

DROP TABLE IF EXISTS `meg_logs_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_logs_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) NOT NULL,
  `main_activity` int(10) unsigned NOT NULL,
  `main_patient` int(10) unsigned NOT NULL,
  `main_user` int(10) unsigned NOT NULL,
  `main_room` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_activities_id_index` (`id`),
  KEY `logs_activities_main_activity_foreign` (`main_activity`),
  KEY `logs_activities_main_patient_foreign` (`main_patient`),
  KEY `logs_activities_main_user_foreign` (`main_user`),
  KEY `logs_activities_main_room_foreign` (`main_room`),
  CONSTRAINT `logs_activities_main_activity_foreign` FOREIGN KEY (`main_activity`) REFERENCES `meg_activity_procedures` (`id`),
  CONSTRAINT `logs_activities_main_patient_foreign` FOREIGN KEY (`main_patient`) REFERENCES `meg_patients` (`id`),
  CONSTRAINT `logs_activities_main_room_foreign` FOREIGN KEY (`main_room`) REFERENCES `meg_rooms` (`id`),
  CONSTRAINT `logs_activities_main_user_foreign` FOREIGN KEY (`main_user`) REFERENCES `meg_users` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=121 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_logs_activities`
--

LOCK TABLES `meg_logs_activities` WRITE;
/*!40000 ALTER TABLE `meg_logs_activities` DISABLE KEYS */;
INSERT INTO `meg_logs_activities` VALUES (1,1,1,3,1,1,'2017-11-06 19:54:23',NULL),(2,0,4,3,1,1,'2017-11-06 19:54:23',NULL),(3,0,9,3,1,1,'2017-11-06 19:54:23',NULL),(4,0,13,3,1,1,'2017-11-06 19:54:23',NULL),(5,0,14,3,1,1,'2017-11-06 19:54:23',NULL),(6,1,2,3,1,3,'2017-11-06 20:29:44',NULL),(7,0,3,3,1,3,'2017-11-06 20:29:44',NULL),(8,0,5,3,1,3,'2017-11-06 20:29:44',NULL),(9,0,6,3,1,3,'2017-11-06 20:29:44',NULL),(10,0,7,3,1,3,'2017-11-06 20:29:44',NULL),(11,0,8,3,1,3,'2017-11-06 20:29:44',NULL),(12,0,10,3,1,3,'2017-11-06 20:29:44',NULL),(13,0,11,3,1,3,'2017-11-06 20:29:44',NULL),(14,0,12,3,1,3,'2017-11-06 20:29:44',NULL),(15,0,17,3,1,3,'2017-11-06 20:29:44',NULL),(16,0,20,3,1,3,'2017-11-06 20:29:44',NULL),(17,1,15,3,1,3,'2017-11-06 20:29:48',NULL),(18,0,16,3,1,3,'2017-11-06 20:29:48',NULL),(19,0,18,3,1,3,'2017-11-06 20:29:48',NULL),(20,0,19,3,1,3,'2017-11-06 20:29:48',NULL),(21,0,15,3,1,3,'2017-11-06 20:29:53',NULL),(22,1,16,3,1,3,'2017-11-06 20:29:53',NULL),(23,1,18,3,1,3,'2017-11-06 20:29:53',NULL),(24,1,19,3,1,3,'2017-11-06 20:29:53',NULL),(25,1,2,3,1,3,'2017-11-06 20:32:41',NULL),(26,1,3,3,1,3,'2017-11-06 20:32:41',NULL),(27,0,5,3,1,3,'2017-11-06 20:32:41',NULL),(28,0,6,3,1,3,'2017-11-06 20:32:41',NULL),(29,0,7,3,1,3,'2017-11-06 20:32:41',NULL),(30,0,8,3,1,3,'2017-11-06 20:32:41',NULL),(31,0,10,3,1,3,'2017-11-06 20:32:41',NULL),(32,0,11,3,1,3,'2017-11-06 20:32:41',NULL),(33,0,12,3,1,3,'2017-11-06 20:32:41',NULL),(34,0,17,3,1,3,'2017-11-06 20:32:41',NULL),(35,0,20,3,1,3,'2017-11-06 20:32:41',NULL),(36,1,15,3,1,3,'2017-11-06 20:32:46',NULL),(37,1,16,3,1,3,'2017-11-06 20:32:46',NULL),(38,0,18,3,1,3,'2017-11-06 20:32:46',NULL),(39,0,19,3,1,3,'2017-11-06 20:32:46',NULL),(40,1,2,3,1,3,'2017-11-06 20:33:11',NULL),(41,0,3,3,1,3,'2017-11-06 20:33:11',NULL),(42,0,5,3,1,3,'2017-11-06 20:33:11',NULL),(43,0,6,3,1,3,'2017-11-06 20:33:11',NULL),(44,0,7,3,1,3,'2017-11-06 20:33:11',NULL),(45,0,8,3,1,3,'2017-11-06 20:33:11',NULL),(46,0,10,3,1,3,'2017-11-06 20:33:11',NULL),(47,0,11,3,1,3,'2017-11-06 20:33:11',NULL),(48,0,12,3,1,3,'2017-11-06 20:33:11',NULL),(49,0,17,3,1,3,'2017-11-06 20:33:11',NULL),(50,0,20,3,1,3,'2017-11-06 20:33:11',NULL),(51,0,15,3,1,3,'2017-11-06 20:33:22',NULL),(52,0,16,3,1,3,'2017-11-06 20:33:22',NULL),(53,0,18,3,1,3,'2017-11-06 20:33:22',NULL),(54,1,19,3,1,3,'2017-11-06 20:33:22',NULL),(55,1,2,4,1,3,'2017-11-06 20:48:10',NULL),(56,0,3,4,1,3,'2017-11-06 20:48:10',NULL),(57,0,5,4,1,3,'2017-11-06 20:48:10',NULL),(58,0,6,4,1,3,'2017-11-06 20:48:10',NULL),(59,0,7,4,1,3,'2017-11-06 20:48:10',NULL),(60,0,8,4,1,3,'2017-11-06 20:48:10',NULL),(61,0,10,4,1,3,'2017-11-06 20:48:10',NULL),(62,0,11,4,1,3,'2017-11-06 20:48:10',NULL),(63,0,12,4,1,3,'2017-11-06 20:48:10',NULL),(64,0,17,4,1,3,'2017-11-06 20:48:10',NULL),(65,0,20,4,1,3,'2017-11-06 20:48:10',NULL),(66,0,15,3,1,2,'2017-11-06 21:10:28',NULL),(67,0,16,3,1,2,'2017-11-06 21:10:28',NULL),(68,0,18,3,1,2,'2017-11-06 21:10:28',NULL),(69,1,19,3,1,2,'2017-11-06 21:10:28',NULL),(70,1,2,3,1,2,'2017-11-06 21:10:33',NULL),(71,0,3,3,1,2,'2017-11-06 21:10:33',NULL),(72,0,5,3,1,2,'2017-11-06 21:10:33',NULL),(73,0,6,3,1,2,'2017-11-06 21:10:33',NULL),(74,0,7,3,1,2,'2017-11-06 21:10:33',NULL),(75,0,8,3,1,2,'2017-11-06 21:10:33',NULL),(76,0,10,3,1,2,'2017-11-06 21:10:33',NULL),(77,0,11,3,1,2,'2017-11-06 21:10:33',NULL),(78,0,12,3,1,2,'2017-11-06 21:10:33',NULL),(79,0,17,3,1,2,'2017-11-06 21:10:33',NULL),(80,0,20,3,1,2,'2017-11-06 21:10:33',NULL),(81,1,2,3,1,2,'2017-11-06 22:04:54',NULL),(82,0,3,3,1,2,'2017-11-06 22:04:54',NULL),(83,0,5,3,1,2,'2017-11-06 22:04:54',NULL),(84,0,6,3,1,2,'2017-11-06 22:04:54',NULL),(85,0,7,3,1,2,'2017-11-06 22:04:54',NULL),(86,0,8,3,1,2,'2017-11-06 22:04:54',NULL),(87,0,10,3,1,2,'2017-11-06 22:04:54',NULL),(88,0,11,3,1,2,'2017-11-06 22:04:54',NULL),(89,0,12,3,1,2,'2017-11-06 22:04:54',NULL),(90,0,17,3,1,2,'2017-11-06 22:04:54',NULL),(91,0,20,3,1,2,'2017-11-06 22:04:54',NULL),(92,1,1,7,1,2,'2017-11-07 14:26:13',NULL),(93,1,4,7,1,2,'2017-11-07 14:26:13',NULL),(94,0,9,7,1,2,'2017-11-07 14:26:13',NULL),(95,0,13,7,1,2,'2017-11-07 14:26:13',NULL),(96,0,14,7,1,2,'2017-11-07 14:26:13',NULL),(97,1,2,7,1,2,'2017-11-07 14:26:20',NULL),(98,1,3,7,1,2,'2017-11-07 14:26:20',NULL),(99,1,5,7,1,2,'2017-11-07 14:26:20',NULL),(100,0,6,7,1,2,'2017-11-07 14:26:20',NULL),(101,0,7,7,1,2,'2017-11-07 14:26:20',NULL),(102,0,8,7,1,2,'2017-11-07 14:26:20',NULL),(103,0,10,7,1,2,'2017-11-07 14:26:20',NULL),(104,0,11,7,1,2,'2017-11-07 14:26:20',NULL),(105,0,12,7,1,2,'2017-11-07 14:26:20',NULL),(106,0,17,7,1,2,'2017-11-07 14:26:20',NULL),(107,0,20,7,1,2,'2017-11-07 14:26:20',NULL),(108,1,15,7,1,2,'2017-11-07 14:26:38',NULL),(109,1,16,7,1,2,'2017-11-07 14:26:38',NULL),(110,0,18,7,1,2,'2017-11-07 14:26:38',NULL),(111,0,19,7,1,2,'2017-11-07 14:26:38',NULL),(112,1,1,7,1,2,'2017-11-07 14:26:45',NULL),(113,1,4,7,1,2,'2017-11-07 14:26:45',NULL),(114,0,9,7,1,2,'2017-11-07 14:26:45',NULL),(115,0,13,7,1,2,'2017-11-07 14:26:45',NULL),(116,0,14,7,1,2,'2017-11-07 14:26:45',NULL),(117,1,15,7,1,2,'2017-11-07 14:26:55',NULL),(118,1,16,7,1,2,'2017-11-07 14:26:55',NULL),(119,0,18,7,1,2,'2017-11-07 14:26:55',NULL),(120,1,19,7,1,2,'2017-11-07 14:26:55',NULL);
/*!40000 ALTER TABLE `meg_logs_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_main_organizations`
--

DROP TABLE IF EXISTS `meg_main_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_main_organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background_color` char(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_organizations_organization_title_unique` (`organization_title`),
  KEY `main_organizations_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_main_organizations`
--

LOCK TABLES `meg_main_organizations` WRITE;
/*!40000 ALTER TABLE `meg_main_organizations` DISABLE KEYS */;
INSERT INTO `meg_main_organizations` VALUES (1,'Hermann Hilll','Hermann Hilll','','','2017-11-06 16:37:16','2017-11-06 16:37:16');
/*!40000 ALTER TABLE `meg_main_organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_migrations`
--

DROP TABLE IF EXISTS `meg_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_migrations`
--

LOCK TABLES `meg_migrations` WRITE;
/*!40000 ALTER TABLE `meg_migrations` DISABLE KEYS */;
INSERT INTO `meg_migrations` VALUES (1,'2017_10_30_165049_create_main_migrations_app',1);
/*!40000 ALTER TABLE `meg_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_patients`
--

DROP TABLE IF EXISTS `meg_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_patients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_identity` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_firstname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_age` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_gender` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_interaction` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `patients_patient_identity_unique` (`patient_identity`),
  KEY `patients_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_patients`
--

LOCK TABLES `meg_patients` WRITE;
/*!40000 ALTER TABLE `meg_patients` DISABLE KEYS */;
INSERT INTO `meg_patients` VALUES (1,'Missj-4854','Howell Ortiz','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(2,'Naomi-8578','Sylvan Feil','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(3,'Elyse-9248','Frederik Waters','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(4,'Royal-2877','Cooper Deckow','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(5,'Patri-1173','Ms. Diana Halvorson','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(6,'Treve-7465','Amaya Harvey','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(7,'Caraj-3442','Karen Bogan','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(8,'Prof.-5149','Joaquin Marquardt','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(9,'Margo-4643','Denis Beahan','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(10,'Prof.-3639','Kenneth West','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(11,'Berna-3128','Aurore Hirthe IV','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(12,'Chels-3482','Mr. Henderson Toy PhD','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(13,'Conne-7737','Boyd Hamill','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(14,'Manle-5054','Prof. Margret Schmidt','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(15,'Lucie-3472','Lexus Spinka','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(16,'Missj-6419','Rebekah O\'Hara','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(17,'Prof.-1225','Rosendo Will','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(18,'Tyree-2073','Ms. Marielle Gutmann','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(19,'Jazmy-1744','Dr. Timothy Murazik Jr.','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(20,'Modes-2486','Jovan O\'Conner','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(21,'Orion-4963','Elza Davis PhD','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(22,'Dr.jN-5755','Demarcus Cronin','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(23,'EvajE-1180','Prof. Kaitlyn Bartoletti','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(24,'Saige-4813','Dion Wisozk','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(25,'Missj-4132','Abbey Harber','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18');
/*!40000 ALTER TABLE `meg_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_procedures`
--

DROP TABLE IF EXISTS `meg_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_procedures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `procedure_title` varchar(60) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `procedures_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_procedures`
--

LOCK TABLES `meg_procedures` WRITE;
/*!40000 ALTER TABLE `meg_procedures` DISABLE KEYS */;
INSERT INTO `meg_procedures` VALUES (1,'Indicaciones','2017-11-06 16:37:18','2017-11-06 16:37:18'),(2,'Inserccion','2017-11-06 16:37:18','2017-11-06 16:37:18'),(3,'Mantenimiento','2017-11-06 16:37:19','2017-11-06 16:37:19');
/*!40000 ALTER TABLE `meg_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_roles`
--

DROP TABLE IF EXISTS `meg_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rol_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol_level` int(11) NOT NULL,
  `organization` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_rol_title_unique` (`rol_title`),
  KEY `roles_id_index` (`id`),
  KEY `roles_organization_foreign` (`organization`),
  CONSTRAINT `roles_organization_foreign` FOREIGN KEY (`organization`) REFERENCES `meg_main_organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_roles`
--

LOCK TABLES `meg_roles` WRITE;
/*!40000 ALTER TABLE `meg_roles` DISABLE KEYS */;
INSERT INTO `meg_roles` VALUES (1,'enfermeras',2,1,'2017-11-06 16:37:16','2017-11-06 16:37:16');
/*!40000 ALTER TABLE `meg_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_rooms`
--

DROP TABLE IF EXISTS `meg_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_identity` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rooms_room_identity_unique` (`room_identity`),
  KEY `rooms_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_rooms`
--

LOCK TABLES `meg_rooms` WRITE;
/*!40000 ALTER TABLE `meg_rooms` DISABLE KEYS */;
INSERT INTO `meg_rooms` VALUES (1,'2668','Emergencia','2017-11-06 16:37:20','2017-11-06 16:37:20'),(2,'8976','Uci','2017-11-06 16:37:20','2017-11-06 16:37:20'),(3,'5781','Maternidad','2017-11-06 16:37:20','2017-11-06 16:37:20');
/*!40000 ALTER TABLE `meg_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_users`
--

DROP TABLE IF EXISTS `meg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_picture` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` int(10) unsigned NOT NULL,
  `main_organization` int(10) unsigned NOT NULL,
  `last_login` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_email_unique` (`user_email`),
  KEY `users_id_index` (`id`),
  KEY `users_user_type_foreign` (`user_type`),
  KEY `users_main_organization_foreign` (`main_organization`),
  CONSTRAINT `users_main_organization_foreign` FOREIGN KEY (`main_organization`) REFERENCES `meg_main_organizations` (`id`),
  CONSTRAINT `users_user_type_foreign` FOREIGN KEY (`user_type`) REFERENCES `meg_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_users`
--

LOCK TABLES `meg_users` WRITE;
/*!40000 ALTER TABLE `meg_users` DISABLE KEYS */;
INSERT INTO `meg_users` VALUES (1,'Rhea Bernier','Selena Jenkins Sr.','$2y$10$BLqz9oru8k8oQZUZpbHpcepcKeICplANSJVfpmTddh50pWTBWJrrO','','norberto44@example.com',1,1,'2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17');
/*!40000 ALTER TABLE `meg_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-07 10:31:15
