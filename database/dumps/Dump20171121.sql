-- MySQL dump 10.16  Distrib 10.1.28-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: co_morbidity
-- ------------------------------------------------------
-- Server version	10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meg_alerts_activities`
--

DROP TABLE IF EXISTS `meg_alerts_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_alerts_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_procedures_id` int(10) unsigned NOT NULL,
  `alert_type_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `patients_id` int(11) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `next_alert` timestamp NULL DEFAULT NULL,
  KEY `alerts_activities_id_index` (`id`),
  KEY `alerts_activities_alert_main_foreign` (`activity_procedures_id`),
  KEY `meg_patients_id_idx` (`patients_id`),
  CONSTRAINT `alerts_activities_meg_activity_procedures_id_foreign` FOREIGN KEY (`activity_procedures_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alerts_activities_meg_patients_id_foreign` FOREIGN KEY (`patients_id`) REFERENCES `meg_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_alerts_activities`
--

LOCK TABLES `meg_alerts_activities` WRITE;
/*!40000 ALTER TABLE `meg_alerts_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `meg_alerts_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_config_platform`
--

DROP TABLE IF EXISTS `meg_config_platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_config_platform` (
  `alerts_dashboard` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bgcolor` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_domain` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  KEY `fk_meg_config_platform_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_config_platform_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_config_platform`
--

LOCK TABLES `meg_config_platform` WRITE;
/*!40000 ALTER TABLE `meg_config_platform` DISABLE KEYS */;
INSERT INTO `meg_config_platform` VALUES ('2-19/1-2/2',NULL,NULL,1,'2017-11-15 15:01:28','0000-00-00 00:00:00',1),('',NULL,NULL,1,'2017-11-15 15:01:28','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `meg_config_platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_devices`
--

DROP TABLE IF EXISTS `meg_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `procedures_id_index` (`id`),
  KEY `fk_meg_devices_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_devices_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_devices`
--

LOCK TABLES `meg_devices` WRITE;
/*!40000 ALTER TABLE `meg_devices` DISABLE KEYS */;
INSERT INTO `meg_devices` VALUES (1,'Sonda Foley',1,1,'2017-11-14 16:30:56','0000-00-00 00:00:00',1),(2,'Sonda Nelaton',2,1,'2017-11-14 16:30:56','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `meg_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_devices_procedures`
--

DROP TABLE IF EXISTS `meg_devices_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_devices_procedures` (
  `meg_devices_id` int(10) unsigned NOT NULL,
  `meg_procedures_id` int(10) unsigned NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`meg_devices_id`,`meg_procedures_id`),
  KEY `fk_meg_devices_procedures_meg_devices1_idx` (`meg_devices_id`),
  KEY `fk_meg_devices_procedures_meg_procedures1` (`meg_procedures_id`),
  CONSTRAINT `fk_meg_devices_procedures_meg_devices1` FOREIGN KEY (`meg_devices_id`) REFERENCES `meg_devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_meg_devices_procedures_meg_procedures1` FOREIGN KEY (`meg_procedures_id`) REFERENCES `meg_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_devices_procedures`
--

LOCK TABLES `meg_devices_procedures` WRITE;
/*!40000 ALTER TABLE `meg_devices_procedures` DISABLE KEYS */;
INSERT INTO `meg_devices_procedures` VALUES (1,1,1),(1,2,2),(1,3,3),(2,1,1),(2,2,2),(2,3,3);
/*!40000 ALTER TABLE `meg_devices_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_log_access`
--

DROP TABLE IF EXISTS `meg_log_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_log_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acces_type_id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_access_id_index` (`id`),
  KEY `meg_log_access_user_id_FK_idx` (`user_id`),
  CONSTRAINT `meg_log_access_user_id_FK` FOREIGN KEY (`user_id`) REFERENCES `meg_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_log_access`
--

LOCK TABLES `meg_log_access` WRITE;
/*!40000 ALTER TABLE `meg_log_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `meg_log_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_logs_activities`
--

DROP TABLE IF EXISTS `meg_logs_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_logs_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `generate_alert` tinyint(4) NOT NULL,
  `activity_procedures_id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_activities_id_index` (`id`),
  KEY `logs_activities_main_activity_foreign` (`activity_procedures_id`),
  KEY `logs_activities_main_patient_foreign` (`patient_id`),
  KEY `logs_activities_main_user_foreign` (`user_id`),
  KEY `logs_activities_main_room_foreign` (`room_id`),
  KEY `logs_activities_devices_id_FK` (`device_id`),
  CONSTRAINT `logs_activities_activity_procedures_id_FK` FOREIGN KEY (`activity_procedures_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_devices_id_FK` FOREIGN KEY (`device_id`) REFERENCES `meg_devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_patient_id_FK` FOREIGN KEY (`patient_id`) REFERENCES `meg_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_room_id_FK` FOREIGN KEY (`room_id`) REFERENCES `meg_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_user_id_FK` FOREIGN KEY (`user_id`) REFERENCES `meg_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_logs_activities`
--

LOCK TABLES `meg_logs_activities` WRITE;
/*!40000 ALTER TABLE `meg_logs_activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `meg_logs_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_messages`
--

DROP TABLE IF EXISTS `meg_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `title_message` varchar(45) NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_meg_messages_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_messages_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_messages`
--

LOCK TABLES `meg_messages` WRITE;
/*!40000 ALTER TABLE `meg_messages` DISABLE KEYS */;
INSERT INTO `meg_messages` VALUES (1,1,'El Catéter está insertado más de :x horas',1,'2017-11-15 20:16:43',NULL,1),(2,1,'Vacíar la Bolsa urinaria en :x  horas',1,'2017-11-15 20:16:43',NULL,1),(3,1,'La Bolsa urinaria está llena más de :x horas',1,'2017-11-15 20:16:43',NULL,1);
/*!40000 ALTER TABLE `meg_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_migrations`
--

DROP TABLE IF EXISTS `meg_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_migrations`
--

LOCK TABLES `meg_migrations` WRITE;
/*!40000 ALTER TABLE `meg_migrations` DISABLE KEYS */;
INSERT INTO `meg_migrations` VALUES (1,'2017_10_30_165049_create_main_migrations_app',1);
/*!40000 ALTER TABLE `meg_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_organizations`
--

DROP TABLE IF EXISTS `meg_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bgcolor` char(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_organizations_organization_title_unique` (`title`),
  KEY `main_organizations_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_organizations`
--

LOCK TABLES `meg_organizations` WRITE;
/*!40000 ALTER TABLE `meg_organizations` DISABLE KEYS */;
INSERT INTO `meg_organizations` VALUES (1,'Hermann Hilll','Hermann Hilll','','','2017-11-06 16:37:16','2017-11-06 16:37:16',1);
/*!40000 ALTER TABLE `meg_organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_patient_admission process`
--

DROP TABLE IF EXISTS `meg_patient_admission process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_patient_admission process` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(10) unsigned NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `reason_process` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_meg_patient_admission process_meg_patients1_idx` (`patient_id`),
  CONSTRAINT `fk_meg_patient_admission process_meg_patients1` FOREIGN KEY (`patient_id`) REFERENCES `meg_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_patient_admission process`
--

LOCK TABLES `meg_patient_admission process` WRITE;
/*!40000 ALTER TABLE `meg_patient_admission process` DISABLE KEYS */;
/*!40000 ALTER TABLE `meg_patient_admission process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_patients`
--

DROP TABLE IF EXISTS `meg_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_patients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identity` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` char(1) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `patients_patient_identity_unique` (`identity`),
  KEY `fk_meg_patients_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_patients_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_patients`
--

LOCK TABLES `meg_patients` WRITE;
/*!40000 ALTER TABLE `meg_patients` DISABLE KEYS */;
INSERT INTO `meg_patients` VALUES (1,'Missj-4854','Howell Ortiz','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(2,'Naomi-8578','Sylvan Feil','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(3,'Elyse-9248','Frederik Waters','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(4,'Royal-2877','Cooper Deckow','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(5,'Patri-1173','Ms. Diana Halvorson','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(6,'Treve-7465','Amaya Harvey','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(7,'Caraj-3442','Karen Bogan','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(8,'Prof.-5149','Joaquin Marquardt','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(9,'Margo-4643','Denis Beahan','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(10,'Prof.-3639','Kenneth West','23','M',1,'2017-11-06 16:37:17','2017-11-06 16:37:17',1),(11,'Berna-3128','Aurore Hirthe IV','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(12,'Chels-3482','Mr. Henderson Toy PhD','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(13,'Conne-7737','Boyd Hamill','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(14,'Manle-5054','Prof. Margret Schmidt','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(15,'Lucie-3472','Lexus Spinka','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(16,'Missj-6419','Rebekah O\'Hara','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(17,'Prof.-1225','Rosendo Will','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(18,'Tyree-2073','Ms. Marielle Gutmann','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(19,'Jazmy-1744','Dr. Timothy Murazik Jr.','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(20,'Modes-2486','Jovan O\'Conner','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(21,'Orion-4963','Elza Davis PhD','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(22,'Dr.jN-5755','Demarcus Cronin','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(23,'EvajE-1180','Prof. Kaitlyn Bartoletti','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(24,'Saige-4813','Dion Wisozk','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(25,'Missj-4132','Abbey Harber','23','M',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1);
/*!40000 ALTER TABLE `meg_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_procedures`
--

DROP TABLE IF EXISTS `meg_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_procedures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `procedures_id_index` (`id`),
  KEY `fk_meg_procedures_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_procedures_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_procedures`
--

LOCK TABLES `meg_procedures` WRITE;
/*!40000 ALTER TABLE `meg_procedures` DISABLE KEYS */;
INSERT INTO `meg_procedures` VALUES (1,'Indicaciones',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(2,'Inserción',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(3,'Mantenimiento',1,'2017-11-06 16:37:19','2017-11-06 16:37:19',1);
/*!40000 ALTER TABLE `meg_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_roles`
--

DROP TABLE IF EXISTS `meg_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `parent_role_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_rol_title_unique` (`title`),
  KEY `roles_id_index` (`id`),
  KEY `roles_organization_foreign` (`organization_id`),
  KEY `fk_meg_roles_parent_role_id1` (`parent_role_id`),
  CONSTRAINT `fk_meg_roles_parent_role_id1` FOREIGN KEY (`parent_role_id`) REFERENCES `meg_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `roles_organization_foreign` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_roles`
--

LOCK TABLES `meg_roles` WRITE;
/*!40000 ALTER TABLE `meg_roles` DISABLE KEYS */;
INSERT INTO `meg_roles` VALUES (1,'enfermeras',2,1,1,NULL,'2017-11-06 16:37:16','2017-11-06 16:37:16');
/*!40000 ALTER TABLE `meg_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_rooms`
--

DROP TABLE IF EXISTS `meg_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identity` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rooms_room_identity_unique` (`identity`),
  KEY `rooms_id_index` (`id`),
  KEY `fk_meg_rooms_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_rooms_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_rooms`
--

LOCK TABLES `meg_rooms` WRITE;
/*!40000 ALTER TABLE `meg_rooms` DISABLE KEYS */;
INSERT INTO `meg_rooms` VALUES (1,'2668','Operación',1,'2017-11-06 16:37:20','2017-11-06 16:37:20',1),(2,'8976','UCI',1,'2017-11-06 16:37:20','2017-11-06 16:37:20',1),(3,'5781','UTI',1,'2017-11-06 16:37:20','2017-11-06 16:37:20',1);
/*!40000 ALTER TABLE `meg_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_users`
--

DROP TABLE IF EXISTS `meg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_phone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_email_unique` (`email`),
  UNIQUE KEY `mobile_phone_UNIQUE` (`mobile_phone`),
  KEY `users_user_type_foreign` (`role_id`),
  KEY `users_main_organization_foreign` (`organization_id`),
  CONSTRAINT `users_organization_id_FK` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_role_id_FK` FOREIGN KEY (`role_id`) REFERENCES `meg_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_users`
--

LOCK TABLES `meg_users` WRITE;
/*!40000 ALTER TABLE `meg_users` DISABLE KEYS */;
INSERT INTO `meg_users` VALUES (1,'Rhea Bernier','Selena Jenkins Sr.','$2y$10$BLqz9oru8k8oQZUZpbHpcepcKeICplANSJVfpmTddh50pWTBWJrrO','','norberto44@example.com',NULL,1,1,1,'2017-11-06 05:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17');
/*!40000 ALTER TABLE `meg_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'co_morbidity'
--

--
-- Dumping routines for database 'co_morbidity'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-21 11:15:36

--
-- Table structure for table `meg_activity_procedures`
--

DROP TABLE IF EXISTS `meg_activity_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_activity_procedures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_type_id` int(10) unsigned NOT NULL,
  `procedures_id` int(10) unsigned NOT NULL,
  `alert_type_id` int(10) unsigned NOT NULL,
  `has_alert` tinyint(4) NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `messages_id` int(10) unsigned DEFAULT NULL,
  `type_id` int(10) unsigned NOT NULL DEFAULT '2',
  `parent_activity_id` int(10) unsigned DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_procedures_id_index` (`id`),
  KEY `activity_procedures_activity_id_foreign` (`procedures_id`),
  KEY `activity_procedures_parent_activity_id_idx` (`parent_activity_id`),
  KEY `activity_procedures_messages_id_foreign_idx` (`messages_id`),
  KEY `fk_meg_activity_procedures_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `activity_procedures_activity_id_foreign` FOREIGN KEY (`procedures_id`) REFERENCES `meg_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activity_procedures_messages_id_foreign` FOREIGN KEY (`messages_id`) REFERENCES `meg_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activity_procedures_parent_activity_id_foreign` FOREIGN KEY (`parent_activity_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_meg_activity_procedures_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_activity_procedures`
--

LOCK TABLES `meg_activity_procedures` WRITE;
/*!40000 ALTER TABLE `meg_activity_procedures` DISABLE KEYS */;
INSERT INTO `meg_activity_procedures` VALUES (1,'Retención urinaria aguda',0,1,1,1,1,'2017-11-06 16:37:19','2017-11-06 19:57:31',2,2,NULL,1,1),(2,'Inserte tubo a la longitud apropiada',0,2,1,1,6,'2017-11-06 16:37:19','2017-11-06 16:37:19',1,3,29,1,1),(3,'Bolsa situada debajo de la vejiga',0,3,0,0,1,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(4,'Herida sacra abierta o perinal',0,1,0,0,2,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(5,'Flujo sin obstrucciones',0,3,0,0,2,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(6,'Sistema de drenaje cerrado',0,3,0,0,3,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(7,'Contención fecal apropiada',0,3,0,0,4,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(8,'La familia entiende el mantenimiento',0,3,0,0,5,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,3,NULL,1,1),(9,'Comodidad al final de la vida',0,1,0,0,3,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(10,'Vacíe la bolsa cada ocho (8) horas',0,3,0,0,7,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,3,NULL,1,1),(11,'Tubo no toca la bolsa al vaciar',0,3,0,0,8,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,3,NULL,1,1),(13,'Necesidad de una medición precisa',0,1,0,0,4,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,2,NULL,1,1),(14,'Inmovilización prolongada',0,1,0,0,5,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,2,NULL,1,1),(15,'IUC más pequeño seleccionado',0,2,0,0,2,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,2,28,1,1),(16,'Cuidado personal e higiene de manos',0,2,0,0,3,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,2,28,1,1),(19,'Vacíe la bolsa antes que se llene',0,3,2,1,6,'2017-11-06 16:37:20','2017-11-06 16:37:20',3,3,NULL,1,1),(21,'Indicación no conforme',0,1,0,0,6,'2017-11-10 11:05:20','2017-11-10 11:05:20',NULL,2,NULL,1,1),(23,'Guantes y equipo estériles',0,2,0,0,5,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,3,29,1,1),(24,'Inflar el globo según las instrucciones',0,2,0,0,7,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,3,29,1,1),(25,'Bolsa de denaje a nivel de la cintura',0,2,0,0,8,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,3,29,1,1),(26,'Conexión cerrada',0,2,0,0,9,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,3,29,1,1),(28,'Antes de la Inserción',0,2,0,0,1,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,1,NULL,1,1),(29,'Inserción',0,2,0,0,4,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,1,NULL,1,1);
/*!40000 ALTER TABLE `meg_activity_procedures` ENABLE KEYS */;
UNLOCK TABLES;
