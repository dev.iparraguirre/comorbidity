-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: co_morbidity
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meg_roles`
--

DROP TABLE IF EXISTS `meg_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `parent_role_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_rol_title_unique` (`title`),
  KEY `roles_id_index` (`id`),
  KEY `roles_organization_foreign` (`organization_id`),
  KEY `fk_meg_roles_parent_role_id1` (`parent_role_id`),
  CONSTRAINT `fk_meg_roles_parent_role_id1` FOREIGN KEY (`parent_role_id`) REFERENCES `meg_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `roles_organization_foreign` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_roles`
--

LOCK TABLES `meg_roles` WRITE;
/*!40000 ALTER TABLE `meg_roles` DISABLE KEYS */;
INSERT INTO `meg_roles` VALUES (1,'enfermeras',2,1,1,NULL,'2017-11-06 16:37:16','2017-11-06 16:37:16'),(2,'administrador',1,1,1,NULL,'2017-11-28 16:12:41',NULL);
/*!40000 ALTER TABLE `meg_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_users`
--

DROP TABLE IF EXISTS `meg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_phone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `mobile_phone_UNIQUE` (`mobile_phone`),
  KEY `users_user_type_foreign` (`role_id`),
  KEY `users_main_organization_foreign` (`organization_id`),
  CONSTRAINT `users_organization_id_FK` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_role_id_FK` FOREIGN KEY (`role_id`) REFERENCES `meg_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_users`
--

LOCK TABLES `meg_users` WRITE;
/*!40000 ALTER TABLE `meg_users` DISABLE KEYS */;
INSERT INTO `meg_users` VALUES (1,'eyJpdiI6IllPWFNMbWdcL1NTMk1hS0luU01OVXFBPT0iLCJ2YWx1ZSI6IkpLaERaU1hZRnFEQllLeUtZV2NEZUE9PSIsIm1hYyI6IjkxYjhjMzk3NmUyNWIzYmI5MzQ3OGJlODU2NzEzNTQzNDNjMGM1MDQyOTNlMzlhMWNlNzYzMjc0MmI3YmE0NTIifQ==','eyJpdiI6IjVIcVIxdjlVTTJ0a1kyZk44bTUyWFE9PSIsInZhbHVlIjoiR2ZpamJWbGJMRHIxXC92WXNjeGgzRnBuUWxNMlwvcHZRSmZ1U29xNGtJNFpZPSIsIm1hYyI6ImYzOGVmMGU3YWM3YWI1ZDEzNWQwYzBmMjk4NWI0MDBmNjA5MTVlZDFjODc2YTlkZGE1YTdiZGVhOTQ5ZmU0YzIifQ==','$2y$10$woZVP0CRRgitc.j3AXUQRu/DSWY/EFjQiB/o6.M0MirLEkpzpAKwG','','norberto44@example.com',NULL,1,1,1,'2017-11-06 05:00:00','2017-11-06 16:37:17','2017-11-24 07:01:37'),(3,'eyJpdiI6Im1vN2JIN1EyS0NsRFZtc0VCZWR3ckE9PSIsInZhbHVlIjoiMXNaNzZwOGpyKzNMQldUNlM0Y2M3Zz09IiwibWFjIjoiMzdmY2VlMmYwNjM4ZjcyYjViZWJmOGYyMmU4NTIwMjE2YTU4OTMzMDQ5MGI1NWVjYjBlNjFjNDEzM2VjNWY5ZCJ9','eyJpdiI6IlROQ1F3VjJPVkFCd3hGUEhLbHJwXC9RPT0iLCJ2YWx1ZSI6IlRyb1NBUmkwMFVNcTRqSkl6a3h1MHc9PSIsIm1hYyI6IjliYWVlM2E3OTc4YzM1Y2MwNDQ2NjM0ZmVkOTI4YWI0NjgwZDczNDYwYTNmNTY4MjI4NWI0MTY1NDMxOWZlNzcifQ==','$2y$10$nDunJxPo4rryc3dpFL5Ma.2Dz9fQDLVi1bL7A1fU5HMTwWgV1euSm','','nono@dada.com',NULL,1,1,0,NULL,'2017-11-28 17:28:41',NULL),(4,'eyJpdiI6InBWQUFMaEk4UVVJVktXbDNleGhzZmc9PSIsInZhbHVlIjoiYmowZXF4YXVSVUJEcUtRU0I5NHpjZz09IiwibWFjIjoiYzE1MmI0ZWFlZDMwYzYyZGQ1YjA3YTljYTZkODVmZmNhNzQwZmQxYTI0Y2YwZDUyNDI5MjM2OTZjOGUyMTQxZSJ9','eyJpdiI6ImtwUHNTR0hDbXhpbythTHB0SzFIUVE9PSIsInZhbHVlIjoiMUs2OExac0o2WlNBNkV1M1dpN3h3QT09IiwibWFjIjoiZjI4YjE1MjBmZjBmNTYwMTA0Zjc2MDExYzhjYTUwNjM0OTBiZjJjN2U4OThkYjkyZWQ1NGY3ZGMyMDM0ZmM0YSJ9','$2y$10$Hxr2OLu30bK9mHFwTtU5v.J4ACmls15kMgG/74Z8Ld9hOxNY436su','','demo@demo.com',NULL,2,1,1,NULL,'2017-11-28 18:01:27',NULL);
/*!40000 ALTER TABLE `meg_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-28 13:43:04
