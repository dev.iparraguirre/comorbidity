-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: co_morbidity
-- ------------------------------------------------------
-- Server version	5.5.5-10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meg_activity_procedures`
--

DROP TABLE IF EXISTS `meg_activity_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_activity_procedures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_type_id` int(10) unsigned NOT NULL,
  `procedures_id` int(10) unsigned NOT NULL,
  `alert_type_id` int(10) unsigned NOT NULL,
  `has_alert` tinyint(4) NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `messages_id` int(10) unsigned DEFAULT NULL,
  `type_id` int(10) unsigned NOT NULL DEFAULT '2',
  `parent_activity_id` int(10) unsigned DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `activity_procedures_id_index` (`id`),
  KEY `activity_procedures_activity_id_foreign` (`procedures_id`),
  KEY `activity_procedures_parent_activity_id_idx` (`parent_activity_id`),
  KEY `activity_procedures_messages_id_foreign_idx` (`messages_id`),
  KEY `fk_meg_activity_procedures_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `activity_procedures_activity_id_foreign` FOREIGN KEY (`procedures_id`) REFERENCES `meg_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activity_procedures_messages_id_foreign` FOREIGN KEY (`messages_id`) REFERENCES `meg_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activity_procedures_parent_activity_id_foreign` FOREIGN KEY (`parent_activity_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_meg_activity_procedures_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_activity_procedures`
--

LOCK TABLES `meg_activity_procedures` WRITE;
/*!40000 ALTER TABLE `meg_activity_procedures` DISABLE KEYS */;
INSERT INTO `meg_activity_procedures` VALUES (1,'Retención urinaria aguda',0,1,1,1,1,'2017-11-06 16:37:19','2017-11-06 19:57:31',2,2,NULL,1,1),(2,'Inserte tubo a la longitud apropiada',0,2,1,1,6,'2017-11-06 16:37:19','2017-11-06 16:37:19',1,3,29,1,1),(3,'Bolsa situada debajo de la vejiga',0,3,0,0,1,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(4,'Herida sacra abierta o perinal',0,1,0,0,2,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(5,'Flujo sin obstrucciones',0,3,0,0,2,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(6,'Sistema de drenaje cerrado',0,3,0,0,3,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(7,'Contención fecal apropiada',0,3,0,0,4,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(8,'La familia entiende el mantenimiento',0,3,0,0,5,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,3,NULL,1,1),(9,'Comodidad al final de la vida',0,1,0,0,3,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,2,NULL,1,1),(10,'Vacíe la bolsa cada ocho (8) horas',0,3,0,0,7,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,3,NULL,1,1),(11,'Tubo no toca la bolsa al vaciar',0,3,0,0,8,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,3,NULL,1,1),(13,'Necesidad de una medición precisa',0,1,0,0,4,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,2,NULL,1,1),(14,'Inmovilización prolongada',0,1,0,0,5,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,2,NULL,1,1),(15,'IUC más pequeño seleccionado',0,2,0,0,2,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,2,28,1,1),(16,'Cuidado personal e higiene de manos',0,2,0,0,3,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,2,28,1,1),(19,'Vacíe la bolsa antes que se llene',0,3,2,1,6,'2017-11-06 16:37:20','2017-11-06 16:37:20',3,3,NULL,1,1),(21,'Indicación no conforme',0,1,0,0,6,'2017-11-10 11:05:20','2017-11-10 11:05:20',NULL,2,NULL,1,1),(23,'Guantes y equipo estériles',0,2,0,0,5,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,3,29,1,1),(24,'Inflar el globo según las instrucciones',0,2,0,0,7,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,3,29,1,1),(25,'Bolsa de denaje a nivel de la cintura',0,2,0,0,8,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,3,29,1,1),(26,'Conexión cerrada',0,2,0,0,9,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,3,29,1,1),(28,'Antes de la Inserción',0,2,0,0,1,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,1,NULL,1,1),(29,'Inserción',0,2,0,0,4,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,1,NULL,1,1);
/*!40000 ALTER TABLE `meg_activity_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_alerts_activities`
--

DROP TABLE IF EXISTS `meg_alerts_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_alerts_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_procedures_id` int(10) unsigned NOT NULL,
  `alert_type_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `patients_id` int(11) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `next_alert` timestamp NULL DEFAULT NULL,
  KEY `alerts_activities_id_index` (`id`),
  KEY `alerts_activities_alert_main_foreign` (`activity_procedures_id`),
  KEY `meg_patients_id_idx` (`patients_id`),
  CONSTRAINT `alerts_activities_meg_activity_procedures_id_foreign` FOREIGN KEY (`activity_procedures_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alerts_activities_meg_patients_id_foreign` FOREIGN KEY (`patients_id`) REFERENCES `meg_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_alerts_activities`
--

LOCK TABLES `meg_alerts_activities` WRITE;
/*!40000 ALTER TABLE `meg_alerts_activities` DISABLE KEYS */;
INSERT INTO `meg_alerts_activities` VALUES (1,1,1,'2017-11-21 19:22:44',NULL,1,0,NULL),(2,14,1,'2017-11-21 19:22:44',NULL,1,0,NULL),(3,15,2,'2017-11-21 19:22:56',NULL,1,0,NULL),(4,16,2,'2017-11-21 19:22:56',NULL,1,0,NULL),(5,23,2,'2017-11-21 19:22:56',NULL,1,0,NULL),(6,2,2,'2017-11-21 19:22:56',NULL,1,0,NULL),(7,19,3,'2017-11-21 19:23:09',NULL,1,0,NULL);
/*!40000 ALTER TABLE `meg_alerts_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_config_platform`
--

DROP TABLE IF EXISTS `meg_config_platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_config_platform` (
  `alerts_dashboard` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bgcolor` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_domain` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  KEY `fk_meg_config_platform_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_config_platform_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_config_platform`
--

LOCK TABLES `meg_config_platform` WRITE;
/*!40000 ALTER TABLE `meg_config_platform` DISABLE KEYS */;
INSERT INTO `meg_config_platform` VALUES ('2-19/1-2/2',NULL,NULL,1,'2017-11-15 15:01:28','0000-00-00 00:00:00',1),('',NULL,NULL,1,'2017-11-15 15:01:28','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `meg_config_platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_devices`
--

DROP TABLE IF EXISTS `meg_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `procedures_id_index` (`id`),
  KEY `fk_meg_devices_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_devices_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_devices`
--

LOCK TABLES `meg_devices` WRITE;
/*!40000 ALTER TABLE `meg_devices` DISABLE KEYS */;
INSERT INTO `meg_devices` VALUES (1,'Sonda Foley',1,1,'2017-11-14 16:30:56','0000-00-00 00:00:00',1),(2,'Sonda Nelaton',2,1,'2017-11-14 16:30:56','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `meg_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_devices_procedures`
--

DROP TABLE IF EXISTS `meg_devices_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_devices_procedures` (
  `meg_devices_id` int(10) unsigned NOT NULL,
  `meg_procedures_id` int(10) unsigned NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`meg_devices_id`,`meg_procedures_id`),
  KEY `fk_meg_devices_procedures_meg_devices1_idx` (`meg_devices_id`),
  KEY `fk_meg_devices_procedures_meg_procedures1` (`meg_procedures_id`),
  CONSTRAINT `fk_meg_devices_procedures_meg_devices1` FOREIGN KEY (`meg_devices_id`) REFERENCES `meg_devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_meg_devices_procedures_meg_procedures1` FOREIGN KEY (`meg_procedures_id`) REFERENCES `meg_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_devices_procedures`
--

LOCK TABLES `meg_devices_procedures` WRITE;
/*!40000 ALTER TABLE `meg_devices_procedures` DISABLE KEYS */;
INSERT INTO `meg_devices_procedures` VALUES (1,1,1),(1,2,2),(1,3,3),(2,1,1),(2,2,2),(2,3,3);
/*!40000 ALTER TABLE `meg_devices_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_log_access`
--

DROP TABLE IF EXISTS `meg_log_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_log_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acces_type_id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_access_id_index` (`id`),
  KEY `meg_log_access_user_id_FK_idx` (`user_id`),
  CONSTRAINT `meg_log_access_user_id_FK` FOREIGN KEY (`user_id`) REFERENCES `meg_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_log_access`
--

LOCK TABLES `meg_log_access` WRITE;
/*!40000 ALTER TABLE `meg_log_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `meg_log_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_logs_activities`
--

DROP TABLE IF EXISTS `meg_logs_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_logs_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `generate_alert` tinyint(4) NOT NULL,
  `activity_procedures_id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `device_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_activities_id_index` (`id`),
  KEY `logs_activities_main_activity_foreign` (`activity_procedures_id`),
  KEY `logs_activities_main_patient_foreign` (`patient_id`),
  KEY `logs_activities_main_user_foreign` (`user_id`),
  KEY `logs_activities_main_room_foreign` (`room_id`),
  KEY `logs_activities_devices_id_FK` (`device_id`),
  CONSTRAINT `logs_activities_activity_procedures_id_FK` FOREIGN KEY (`activity_procedures_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_devices_id_FK` FOREIGN KEY (`device_id`) REFERENCES `meg_devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_patient_id_FK` FOREIGN KEY (`patient_id`) REFERENCES `meg_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_room_id_FK` FOREIGN KEY (`room_id`) REFERENCES `meg_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_user_id_FK` FOREIGN KEY (`user_id`) REFERENCES `meg_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_logs_activities`
--

LOCK TABLES `meg_logs_activities` WRITE;
/*!40000 ALTER TABLE `meg_logs_activities` DISABLE KEYS */;
INSERT INTO `meg_logs_activities` VALUES (1,1,1,1,1,2,2,'2017-11-21 19:22:43',NULL),(2,0,4,1,1,2,2,'2017-11-21 19:22:43',NULL),(3,0,9,1,1,2,2,'2017-11-21 19:22:43',NULL),(4,0,13,1,1,2,2,'2017-11-21 19:22:43',NULL),(5,1,14,1,1,2,2,'2017-11-21 19:22:43',NULL),(6,0,21,1,1,2,2,'2017-11-21 19:22:43',NULL),(7,1,15,1,1,2,2,'2017-11-21 19:22:56',NULL),(8,1,16,1,1,2,2,'2017-11-21 19:22:56',NULL),(9,1,23,1,1,2,2,'2017-11-21 19:22:56',NULL),(10,1,2,1,1,2,2,'2017-11-21 19:22:56',NULL),(11,0,24,1,1,2,2,'2017-11-21 19:22:56',NULL),(12,0,25,1,1,2,2,'2017-11-21 19:22:56',NULL),(13,0,26,1,1,2,2,'2017-11-21 19:22:56',NULL),(14,0,3,1,1,2,2,'2017-11-21 19:23:08',NULL),(15,0,5,1,1,2,2,'2017-11-21 19:23:08',NULL),(16,0,6,1,1,2,2,'2017-11-21 19:23:08',NULL),(17,0,7,1,1,2,2,'2017-11-21 19:23:08',NULL),(18,0,8,1,1,2,2,'2017-11-21 19:23:08',NULL),(19,1,19,1,1,2,2,'2017-11-21 19:23:08',NULL),(20,0,10,1,1,2,2,'2017-11-21 19:23:08',NULL),(21,0,11,1,1,2,2,'2017-11-21 19:23:08',NULL);
/*!40000 ALTER TABLE `meg_logs_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_main_organizations`
--

DROP TABLE IF EXISTS `meg_main_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_main_organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background_color` char(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_organizations_organization_title_unique` (`organization_title`),
  KEY `main_organizations_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_main_organizations`
--

LOCK TABLES `meg_main_organizations` WRITE;
/*!40000 ALTER TABLE `meg_main_organizations` DISABLE KEYS */;
INSERT INTO `meg_main_organizations` VALUES (1,'Hermann Hilll','Hermann Hilll','','','2017-11-06 16:37:16','2017-11-06 16:37:16');
/*!40000 ALTER TABLE `meg_main_organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_messages`
--

DROP TABLE IF EXISTS `meg_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type_id` int(11) NOT NULL,
  `title_message` varchar(45) NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_meg_messages_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_messages_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_messages`
--

LOCK TABLES `meg_messages` WRITE;
/*!40000 ALTER TABLE `meg_messages` DISABLE KEYS */;
INSERT INTO `meg_messages` VALUES (1,1,'El Catéter está insertado más de :x horas',1,'2017-11-15 20:16:43',NULL,1),(2,1,'Vacíar la Bolsa urinaria en :x  horas',1,'2017-11-15 20:16:43',NULL,1),(3,1,'La Bolsa urinaria está llena más de :x horas',1,'2017-11-15 20:16:43',NULL,1);
/*!40000 ALTER TABLE `meg_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_migrations`
--

DROP TABLE IF EXISTS `meg_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_migrations`
--

LOCK TABLES `meg_migrations` WRITE;
/*!40000 ALTER TABLE `meg_migrations` DISABLE KEYS */;
INSERT INTO `meg_migrations` VALUES (1,'2017_10_30_165049_create_main_migrations_app',1);
/*!40000 ALTER TABLE `meg_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_organizations`
--

DROP TABLE IF EXISTS `meg_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `bgcolor` char(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_organizations_organization_title_unique` (`title`),
  KEY `main_organizations_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_organizations`
--

LOCK TABLES `meg_organizations` WRITE;
/*!40000 ALTER TABLE `meg_organizations` DISABLE KEYS */;
INSERT INTO `meg_organizations` VALUES (1,'Hermann Hilll','Hermann Hilll','','','2017-11-06 16:37:16','2017-11-06 16:37:16',1);
/*!40000 ALTER TABLE `meg_organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_patient_admission process`
--

DROP TABLE IF EXISTS `meg_patient_admission process`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_patient_admission process` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_id` int(10) unsigned NOT NULL,
  `status_id` int(10) unsigned NOT NULL,
  `reason_process` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `fk_meg_patient_admission process_meg_patients1_idx` (`patient_id`),
  CONSTRAINT `fk_meg_patient_admission process_meg_patients1` FOREIGN KEY (`patient_id`) REFERENCES `meg_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_patient_admission process`
--

LOCK TABLES `meg_patient_admission process` WRITE;
/*!40000 ALTER TABLE `meg_patient_admission process` DISABLE KEYS */;
/*!40000 ALTER TABLE `meg_patient_admission process` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_patients`
--

DROP TABLE IF EXISTS `meg_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_patients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identity` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `firstname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `age` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `gender` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `patients_patient_identity_unique` (`identity`),
  KEY `fk_meg_patients_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_patients_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_patients`
--

LOCK TABLES `meg_patients` WRITE;
/*!40000 ALTER TABLE `meg_patients` DISABLE KEYS */;
INSERT INTO `meg_patients` VALUES (1,'Missj-4854','eyJpdiI6IjNUUHRIaUVEek11alwvU2lqNmdBRDl3PT0iLCJ2YWx1ZSI6InRva3lCRHNnUmRGV1RTVzRaNFFGdXc9PSIsIm1hYyI6ImU2MWE4YTQ3MmM0ZDlhOWQ3MDNhYWViMTY2ODRkODUxMjM5NDUwYjdhZGZmN2ViMDA5MDljZmViMGI5YWRmN2QifQ==','eyJpdiI6Imk4OEI1eFJ1dmRBMWw2TStKSGMybUE9PSIsInZhbHVlIjoiYTdNRno4ZlJkRW9NY2JVdTBLWnZVQT09IiwibWFjIjoiMjQxZjE5NDc0MzhmYjQ5ZTI4ZmY4ZWFiNjdlMWUzYmU3ZTQ5YmIyMzIwN2Y1MjgyODNmNzNhZGJiOTk4ZTc0YSJ9','eyJpdiI6IllJR3E4bGI3VmZ4dWw3dXNXVTlwTFE9PSIsInZhbHVlIjoiemNyWU9ieXRaaDFwS2lmdlhNQUZcL1E9PSIsIm1hYyI6ImFlNDZiODkwZTc3NjM2YzU4Yjg5M2FmYWFhN2ZmYjgyNjMzYjMzMDFmZTNmN2Q3ODIzNzVmMDlkMmUwOGEwMmIifQ==',1,'2017-11-06 16:37:17','2017-11-21 19:49:23',1),(2,'Naomi-8578','eyJpdiI6ImI3bDBYVW5IYTQ4UTl1K3hQYXBNMlE9PSIsInZhbHVlIjoiTkQ4QnNlZ0FvRlZSQit5TStnZ0Fxdz09IiwibWFjIjoiZTE3MDQyOWZmNjEwZmQ2NGRiMWE0MjRlOTMzMDlmZDRhZWE2ZjhhZGM1OTZkNzVhYzA1ZGEyNzJlNjliZjE0OCJ9','eyJpdiI6ImZjTUlKQjZGSEZ1bHk2b1NXMng5OWc9PSIsInZhbHVlIjoiNkcrekNoZWVYWVlGM1J1ODBTcU9zZz09IiwibWFjIjoiZmQzM2NmNDg4OWJkYjJkMmMyY2U5NWQ1ZDM5ZTgxODViNmU2NTUzMmUzYjc5YzViYzY0ZTVlYzg2NjdmNzg4OCJ9','eyJpdiI6IjdEREYwNXdXSVVMYUo3eDhQMVVwY0E9PSIsInZhbHVlIjoiV3dKSGtHUzNqYUJYM0lDWVpQN1Vsdz09IiwibWFjIjoiNzA2N2Q3OTA2NDYwMTJiNTdkNGFlNGFlZDc5YmM0M2FhNDJhZWQyZWI5OThlZmZlZWU0YmQyZjI3M2UyMjEyZiJ9',1,'2017-11-06 16:37:17','2017-11-21 19:49:23',1),(3,'Elyse-9248','eyJpdiI6ImNrY3lEa3hJQlk1SFpLaUtienBGU1E9PSIsInZhbHVlIjoiTlR4TFZmeFJ2djVCRlU2amNhUVpBQT09IiwibWFjIjoiZWVmODE1NTcxYTVkMTI4NTJiYmViZDUxMDU2ZGY3NDc2OTA4MDQ4MWQ5MTlmOGQ1NTk1OTdiOTY5ZjU1ZTZjNiJ9','eyJpdiI6IkVIMWpzc20zSG1ZRitYdStqcXlUQ2c9PSIsInZhbHVlIjoiMkhCc0pDMkd5eURMbzM4NTB5cEVBUT09IiwibWFjIjoiYTBjNzNlZTNhMzVhODM2ODg5MTQ4NmFiMzE2N2I5NTJhNzEzYjQ5MGJhNDY1NjkzMDc3YzAwM2EyZDY2NGMzNyJ9','eyJpdiI6ImFIdVV1YnJ1TXRcL2NqMmFoODBXV01RPT0iLCJ2YWx1ZSI6Im9lY2c5eE9XVlJrMURKRTVNcjdIVHc9PSIsIm1hYyI6ImYyYWIxN2RkNzFlNWQ4ODg2OGVmYmUzZTM3MzA5NjdiM2RlZTAzZDAzOWUwYWNjN2E3Nzg0ODY3YzA1ODkyNjcifQ==',1,'2017-11-06 16:37:17','2017-11-21 19:49:24',1),(4,'Royal-2877','eyJpdiI6ImI4eVF4VXpITjlWT1lZMWxYSGwwaUE9PSIsInZhbHVlIjoiZUY5NkZ0VzJxTEMzMm5nNnZVanoxUT09IiwibWFjIjoiZTM2M2MyYmM2OTQ0YjRmNGUzYzNhMGZhYzQyNTRlMzkzMjFkY2EzMzBjZTIxOGJlYzE4ZjI5Y2E3MDU3NThkZiJ9','eyJpdiI6IkNMWXJtOHdIYWhtYVNWZzNvTHRuYlE9PSIsInZhbHVlIjoiWTVTSHFBT0xhUGRcL0FGRWQzbG0xckE9PSIsIm1hYyI6IjZmMjgyNjYxYWIwODc4YjA0YmY2OGQzMTlhMjg2NDNiMGExMzFjMGJmNWY0YTkyZTNmOTgzMTJiMGRmNDZlZmYifQ==','eyJpdiI6IlJTXC9MRlwvMHUxOUFzK0ViZktpb08rZz09IiwidmFsdWUiOiJEQzhFTmdSNjlicTJQMEpqYWU0dUpnPT0iLCJtYWMiOiI1NmIyMTk0YjY5YjNmZjg0ZDg0ZDQ4M2Y4NTQ3NTgxZWU5YzlkNjE1MmYyZTc5ODgzYWE5YzY3NjZmNDQzZDBiIn0=',1,'2017-11-06 16:37:17','2017-11-21 19:49:24',1),(5,'Patri-1173','eyJpdiI6IjY5Q3FsNExpVFFZWWtVQTJqVnUzNVE9PSIsInZhbHVlIjoiZldrNGlnTVpIU25vSjQ5S3VKRE5pWDNlS1J3eHRGekRMMFNFN3ZDSmZIOD0iLCJtYWMiOiJiZmZiNzhlNTk4NTlmNWUyMGE5NmE1YmU4ODg0NDEwZjNhODBmMGU1YjZkMWVkMjhkMThjNDMwZTU3YjgwNDk3In0=','eyJpdiI6Im0rN2tXYzdJaHIxT1BpMkRwTmlFUGc9PSIsInZhbHVlIjoiTlZJRHBWVnJDMkZkOVkxNDNsUTA4dz09IiwibWFjIjoiNGY1ODkzMmNjMTJjMjhmN2IxZWIyMDU4NGRhNzgwMTUyMmQ4NTRkYzBiNWZjYmQxNmI5NzYxOTk2MzZkMzlkYiJ9','eyJpdiI6ImlPNEpSTGpJdEtDR005cVVTVnExK1E9PSIsInZhbHVlIjoiUWdCUjZaRlFkRUhRUGxuRDYrSGlVUT09IiwibWFjIjoiYzkzNDMxMWI5ODFhMmMyZGIwM2FkOTEyYjJhN2UxZTEzMjEyZWU1Y2I5ZTc0YTUzNzUzYjRhZmYzYTA5OTNhOSJ9',1,'2017-11-06 16:37:17','2017-11-21 19:49:24',1),(6,'Treve-7465','eyJpdiI6InZjbU1uMFZlNEl5ajlLa0d4TkFnTFE9PSIsInZhbHVlIjoiN1lCblhlNEtCaXpsdllpRDdzQ3VKdz09IiwibWFjIjoiNTUxNGRjNDMwMTk1NjUyNzdiMGY2YWIyOWRkYzI2ZWI2ZGQ3NmQ0M2JiODY3MjhmMmRhZjg5MWRjYjQ1MDE3ZiJ9','eyJpdiI6ImUxVVwvWHVcL2o4MFZBeXF5UG1rMHFOZz09IiwidmFsdWUiOiJ1U0JZN1J0VkJCZkFoTGRQa3BmVnVBPT0iLCJtYWMiOiI4MjYzNzA3MjM4ZWEzMmQ4NWIwYzBlNDU1NzliYzkxYTA4YjdhZDQ4M2VkN2M3MzMzYzdjNzgyMTVjMjhlODZjIn0=','eyJpdiI6Ik9VSVBoTWZQMGs1MW5EZzVGOUxtdXc9PSIsInZhbHVlIjoicDlteWFaWFFiV0pRREllYjNPN0hvQT09IiwibWFjIjoiYmQ0MzJjOTkwMzM4NTQyNTUxOTAyZTM3NDgyOWQzNGM0ZGM0ZmMzYWNmMWZlMGU2YWM1NTIyYTUzMzdmOGU5OCJ9',1,'2017-11-06 16:37:17','2017-11-21 19:49:24',1),(7,'Caraj-3442','eyJpdiI6ImhKWlwvbDBGbkJJcHZqOUJVSUNiOTdnPT0iLCJ2YWx1ZSI6ImYzMGNhU3dvZnlDRm5vS3p6OXRtTlE9PSIsIm1hYyI6ImVjMjY5NTE0NDYzYmZkNWNhMTAxNzc4ZmM2ZmQ0ZmQ0OGJjNjgwNmFiYWZmNGQ3NDViZTQxMzgzZTk3MzBlODEifQ==','eyJpdiI6IlFJVXRvSWRCOWdUK2VKZFp4bVN0bGc9PSIsInZhbHVlIjoiR0dwTDB2UkQxTytCQlY5OXpUT2FjQT09IiwibWFjIjoiOWZmNjk1MTFlYmRlYTFmMWE4NGE0ZmY4MTRjMmYzNThiMTRkYjVhYWYxYWVkZDdiYTU2ZGYzMWE5ODZhODY4MiJ9','eyJpdiI6IlhoMUpFbkxaMDIwenplWDZtODZjbFE9PSIsInZhbHVlIjoiRklLRzRWSDhGcWtpSVVscERkYVE1UT09IiwibWFjIjoiNzMxZmNiOThlMGE4N2E1NGQxYzRiYjhiYmZiZmQyNmI1YWI5NWIwMjFhYmE2NGFhMzQ4MTBjNzJlODg3MzU3YiJ9',1,'2017-11-06 16:37:17','2017-11-21 19:49:24',1),(8,'Prof.-5149','eyJpdiI6IjRhU1pJVmJDSzkrdmtCR2M0bE5JckE9PSIsInZhbHVlIjoiOVF3UlMzUGtIT3lWNTFWdzVNUUxSNHV2SnB6bzBSNDVheUdpQnNRVjBuaz0iLCJtYWMiOiI1ODQzMWU5MGQyNDlhMDBiZDM3NTJjOTRlNDhkZjJkNjg3Mzc1MTY4NGZkNmMxNDI0Y2Y0NTg4NDE0M2YyNDQyIn0=','eyJpdiI6IkpWeTEzck1Hd0pHTmRYdlo3MkpLVGc9PSIsInZhbHVlIjoiVUUwenBVTjl4XC9QdFNZbjRIWForWEE9PSIsIm1hYyI6IjhhZDIwY2I3YjM3MDE2NGNjMTFlZjE1NGEwMjNlYTc4MjhlOWJhMGVkZWIxMzA2NmRiMzZhZjQ4YjkwZTQ2NmQifQ==','eyJpdiI6Im9SeVUzTzdWaTFoNU16ZnB2NXFMQkE9PSIsInZhbHVlIjoiXC9BRk1EaXRFMjhXeUQ2aDJTRGMzMmc9PSIsIm1hYyI6IjIxZGE3M2ExNjMzN2NhYzY0OGJjNzVhNzc3YTU3MzFmMmFhN2FlZDRkNTFhN2FmMDk5NDhiNGRmYTY5Y2QwZDUifQ==',1,'2017-11-06 16:37:17','2017-11-21 19:49:24',1),(9,'Margo-4643','eyJpdiI6Im1mWStzNmE4WmhvTGJ2YnhzUlRzVGc9PSIsInZhbHVlIjoiMnIxTzRCMVV1UE1TNjUxcXZMSVpjUT09IiwibWFjIjoiZmFjMmI0ZTNlYThlZDVmYTI5OTNhZDA4N2NhNGE3NmRiM2Q2ZDVlNDNiMjExZGFkNWVlMjIwMTFlOTFkZjRhNyJ9','eyJpdiI6ImpxdHZSMGRmR1N3bk9HU3ljQWt2THc9PSIsInZhbHVlIjoiUEUyZGdnTytJT1VRcUNBOEFSRUxDUT09IiwibWFjIjoiNWEzYTRjYzhhY2YzN2JkNjRjYjkzOTRjZTRmMDczNzQyODA4MWYxZDgwM2YzMjk3ZDg5Y2I4OTE3YzY2OWZiOSJ9','eyJpdiI6IlJYN0RZWmdUd1hjTExnXC8zMVNcL3lSdz09IiwidmFsdWUiOiI1S2JuMmVJZXh2c0s3OGZORW5zQWN3PT0iLCJtYWMiOiJiNDBjODBmMDNkOGZjNTE2OWUxMGE1NGExYjI1ZTM4ZWY3ZGZmNTRjZDFiMjIwMWEzYzBkOWQ5YzgwMTJkNTMwIn0=',1,'2017-11-06 16:37:17','2017-11-21 19:49:24',1),(10,'Prof.-3639','eyJpdiI6IjJnb1NNUjh2UjBDWkYreHQ3QVJzblE9PSIsInZhbHVlIjoiS1dOc0J5MTdFQVRjSVlQZkNFb0wrdz09IiwibWFjIjoiNDhmY2UwMTk1NDdhNWFhYjFjYzg0NGQ5N2FkM2Y3YWZiMjAxMjg1NmFmNGM5MGQyMDFlZjYwMDFhZmI2YTZlNSJ9','eyJpdiI6InQ3XC8wb0F6WGZGSER5MFErMVU1UjBRPT0iLCJ2YWx1ZSI6IjdtcnkzMW5pK0pQaG53blVHS0w5bFE9PSIsIm1hYyI6ImQ0ZmZhYmQ0ODc4ZTBkNGZmNDhlNjUyMmQ0YjRlNTBlOGUzODcyMDA2MjMwODQ3ODM2ZDU0YzVhOTg2MGRmNGEifQ==','eyJpdiI6Inh5NGpQNHF2b0ljSlhWeVVWWjFGMGc9PSIsInZhbHVlIjoiUmJhRDNxa3FBblwvWnVxa2ROemNEamc9PSIsIm1hYyI6IjcxOTVkMGQ4Y2NiYTYyYjIwMGY1NTU5MmQyMzhjOTgzMTUyNzhmMWY1MjI5Nzc2MTlmMGU1OTNiMmY1NGFjYzkifQ==',1,'2017-11-06 16:37:17','2017-11-21 19:49:24',1),(11,'Berna-3128','eyJpdiI6InIyVDd5dkVBckNpMFM3MDRldmpBXC9BPT0iLCJ2YWx1ZSI6IkliSnI0QVFnWng0NUZORmJ0M2Z4OUYra0RENjN0U3BGWVhkVFQxZGhUcGM9IiwibWFjIjoiYThmMjJmMGNjOTdhNjBlZDdlZDQ4OGY2NGVkMGRlZDUxM2ZiYzcxODRiNmRiYmI1Y2IzOGI2MDM2Y2U1MmY0NyJ9','eyJpdiI6ImhVdnRzNVlsSjc5XC9xb3J1R1lMNTFBPT0iLCJ2YWx1ZSI6IjUwVk9jbzlySDVkSVwvbmk3NmYyZUhBPT0iLCJtYWMiOiI3YjliYmY3ZTMyNGIwNTZjOWY4NDU4OWQyNTBiOWVhNWFjMDc3ODRkMTE3NzQ0YjhlYzRkOTA3MmQwYWNlY2E0In0=','eyJpdiI6IkFBVTBlYzNGcmtRTDBPNm93aFwvYTZRPT0iLCJ2YWx1ZSI6InFyMkZcL1pUWXNwaitic2pqa01mc3FRPT0iLCJtYWMiOiIxZjRmNDU4MDhmNGUxNGE4MTVlZmQ3ZTIwYjJkZjMwODI4YTc2YjUxYjFjNjQ5YjU5YWU1ZTY5Y2NkOTcxYTU1In0=',1,'2017-11-06 16:37:18','2017-11-21 19:49:24',1),(12,'Chels-3482','eyJpdiI6Im9YdFdGeVdqVlwvSHlabm9zNGtnSTh3PT0iLCJ2YWx1ZSI6IkRSTzJXbk1oY2NnTzU2WE41SGxERld5T1wvZnRsZ3FRbndiVmN6bkZZd1wvaz0iLCJtYWMiOiJmOTEwZTk4MzAzYzhlOWY1MTYzZTY4MzlmZmZlNjY4Mjk1Zjc4YjhjZmJmYjQ5NTM1OTg2OTYyNjJmZmMyNTIxIn0=','eyJpdiI6ImZMbG1pUFJvejA2SzlVdGlGMHVlamc9PSIsInZhbHVlIjoiSCtCQURGYVZDQ1wvVklRQ1ZialZFS1E9PSIsIm1hYyI6IjZkODZhNmQ1OGI3NWZiMzIwMDMxZWM4YzBkNDM3N2ZkNWVkMGY1MDMyNjVjMGRkMzNjZTMxMDIwMzBhYmExZjEifQ==','eyJpdiI6IkpBMHgzalg5U2kwSWMyMG9KWEFIUmc9PSIsInZhbHVlIjoiUndSdXE4ZTA3eUlGbVVDRk1vR3R1UT09IiwibWFjIjoiYTVmOTlmNmNhZGUzMDNhMGU1YmJmMDk0OTliMjM4MjBmYzY3ZWU1MzcwM2Q2ZTNhZmZlOGViN2IwNzA4ODkxYiJ9',1,'2017-11-06 16:37:18','2017-11-21 19:49:24',1),(13,'Conne-7737','eyJpdiI6IkFpNTlUOFpkbllcL3hjMkxUK1psXC94QT09IiwidmFsdWUiOiJLcUJtSHIxU3dwNTM0Zk9rK3dCVVNnPT0iLCJtYWMiOiIzMThmMjQzYmQ5ZjVjMmQ5NjVhNzA4YTlmZDA0YTYzYWZiZDcwNzc4Y2ZiMmFjMjI0YjA1MDQzNGVhMjlhNDE0In0=','eyJpdiI6IlZKY3VvTWdlM2ZOa2FWRGVEeldWelE9PSIsInZhbHVlIjoiQjI1aXdqYzJUY2hMV3dwUWdYbVwvQ1E9PSIsIm1hYyI6IjNmMTdmNDg1NTUzNjgxMWZkMTNkYzVlZWI2N2I4MWVkYzJhNTg0MDE5YWE0ODk1NTU0MmE0YzIwZGYxY2RkMjcifQ==','eyJpdiI6Ik56TFE3XC9oUmhyNjVORG9TTUNrVHhnPT0iLCJ2YWx1ZSI6InFTUEdDdzBQSzRRWTJpR0FUUktrTFE9PSIsIm1hYyI6IjMwYTIxMDVjNjc2MDFjOTAxMDU3NDYxOGFmNDk0NmNhN2U5MTlkZTQyZWE4NmEwYTA5ZTA0NTlhNGFmMGVhZGMifQ==',1,'2017-11-06 16:37:18','2017-11-21 19:49:24',1),(14,'Manle-5054','eyJpdiI6ImhybnpSUExBbDdvM2VwV3RjTHdBQ0E9PSIsInZhbHVlIjoiTTc5dHlzOVlJbUY3cFNRQW9qbXl2dlZiNDJDXC8zTCttSkVhV2l3K0twTFk9IiwibWFjIjoiNDlmYWQwODFmNjAwYzM1M2M0YzdiNjE0ZGRjOGJlYTAxNWM3OWVmODUyNzg5YTE3MGY0NTkwMWIwZTJiNTYwMCJ9','eyJpdiI6IlM0KzNZRFNyZ2RnVVBlVlJGM2t2MWc9PSIsInZhbHVlIjoiXC9cL283aVhwSitoTjZzTVpwcUxPK0l3PT0iLCJtYWMiOiJkMmRlZDcxMzVmYmQwZDk3MzJiMjRhMTU4ZWMyNDMwZTNhNWI5MTBhNjA3NDM0MjFjYzU0ZTE1Zjk0NDI5OTcxIn0=','eyJpdiI6InNremk1MTJmOVwvTWUzT2RnanF3UmhBPT0iLCJ2YWx1ZSI6ImtVaG1YSkgxQ0U5U01tblhjcHQ2TkE9PSIsIm1hYyI6IjBhODRkM2QwODFlZjY4ZTlkYWU3ZTRlYWQ3MGFhMjA2YzMxYmQ3MTY3M2NiY2U5MmVjNmJiNTQwMDFjNDg3NTAifQ==',1,'2017-11-06 16:37:18','2017-11-21 19:49:24',1),(15,'Lucie-3472','eyJpdiI6IjhlR1V5cFNhU2JJWHpkd1A0WlJtUEE9PSIsInZhbHVlIjoiNXFkWG1VSXVtcUZValFHTVwvUmpad3c9PSIsIm1hYyI6Ijc4N2NmNzg3YWI1ZjEyYWYwOGMxN2Y4MDc1NGI3YzUyOGIxMWRjMzgzMjRkNjY0YWEzMzc5ZTg4ZmI1OWZjNzAifQ==','eyJpdiI6ImhsMVwvSHA4cEFjaExVaTVwS2VxTkdRPT0iLCJ2YWx1ZSI6InM3aTYweHN2U0JkeUJBUWRua2k0MXc9PSIsIm1hYyI6IjJmY2Q0MWJjZmM1MjNhOTQyMDc3YzNkZTc5YzY3YmVkZjYwMTk3MmJlMGQyYzViMDI5ZTcwMWYzNzI1YWU0MjIifQ==','eyJpdiI6InRrbUdBOG9IN1pTWDdDbHVuQWFkVVE9PSIsInZhbHVlIjoiajBBakJoUEdhd0VpOXVZSUpBNnVaUT09IiwibWFjIjoiNmYwMGJhNmE0MzAzNzFhOGNmZjBkMTE2MWViNWMyMjNmNDM5ZTlhMmNiNjM1N2U5MmJiNDM4ZDlkMjZkNTk2MCJ9',1,'2017-11-06 16:37:18','2017-11-21 19:49:24',1),(16,'Missj-6419','eyJpdiI6ImdXSUQxT1ZWdmVSZkxUR054N2Y4M1E9PSIsInZhbHVlIjoibzZ1S1VXeDlCdVwvYWlPYlBkUzl6VWc9PSIsIm1hYyI6IjM3ZDRjYjYzNjgyMmJjMWJmMDExOGNiZTFiNDZmZWNmY2E5YTRiM2RhOTIwOWE4MmI5NWMyNTBhN2JkOTJlYTMifQ==','eyJpdiI6IndQcXZHcko1a2ZtTWp0T0FrbWtvK3c9PSIsInZhbHVlIjoiTVBYNzhYMTdsTmozUWRmUitnZXVuQT09IiwibWFjIjoiZWI0NzViOTQzNzg4NDhjODRhMjQ0NWFkYTUxMzdhNDlkNzZkYjRmMmU1ZjI5ZTgwNDYxNjE3MWY0N2EwZmFmOCJ9','eyJpdiI6InlDRUp6VEtaKytjRXg3OUlIMU45cVE9PSIsInZhbHVlIjoiczArTDJYMVJxSFwvMFRcL0JBZHNiS3lBPT0iLCJtYWMiOiIyZWMzZTNmZDczMTk4M2RhZmJmODI3NjY2ZmNmYTdmOTQ3ZWNlN2YzZThkODg1YWJhMDgyYThlZGI2YThmOTA0In0=',1,'2017-11-06 16:37:18','2017-11-21 19:49:24',1),(17,'Prof.-1225','eyJpdiI6Im9xRE12U2orZXl5cG1YWDNENituZnc9PSIsInZhbHVlIjoidEFFN2duSGpjT2JzRkNNZHZiQ3N2dz09IiwibWFjIjoiM2VlZDNhN2I3ZWEyMWFhYmMyMjA4MjI4NzE5YzcwOTU1YmRjNDNjZDgzNWYyOTEwNjUwOGU0MzFiNGY2MTkzYyJ9','eyJpdiI6IldBT2JmSnkyOGpwM3plalJ5cGo5Rmc9PSIsInZhbHVlIjoiMHNBbEc3ZnRGUUo3QTRlWkg0VWJUUT09IiwibWFjIjoiMDJmOWIzNzIwMTVmYmE3Nzc0MGRlMWE0ZGMyNjEyZTFmNDBmODU2NjlmNDdjNTZjOWQyOWY3YTFkMzFmZDYxNSJ9','eyJpdiI6Ik9ZZkZIZk05elZIY1VcL3h6a1ViQXd3PT0iLCJ2YWx1ZSI6ImtvUWxhNFY1bml6djU0MlN1XC9tMTVRPT0iLCJtYWMiOiI0MGE2MjRhNzFhYjdhMzk0N2NjOGU3OTY2YTVkNDA5ZmM5NzY1OTdjNzczNTE3Nzg4MmE5YmRiMjVhYTk5M2NkIn0=',1,'2017-11-06 16:37:18','2017-11-21 19:49:24',1),(18,'Tyree-2073','eyJpdiI6ImxHUjhSOWZuVXdFTXJkY0sxWDhYVkE9PSIsInZhbHVlIjoiUnBrUFIyR1lMclRxM3JCYm0zTEFBcUM2ak1IeW94a0tWcW1Ed3lSbmJuND0iLCJtYWMiOiIyM2FlZWNkMjlmNzRiYjA5MDRhYTE2YjY5N2M1MjVkM2QwMTViNTg5Y2Q2ZWI2NWYzNzhlNmIyNGU0MzIwM2I2In0=','eyJpdiI6InVZZmIxTEVuVzg1QlwvcjhLc3ptOW9RPT0iLCJ2YWx1ZSI6InppdkZlWnlHb09ibU01SDZHQ2twOFE9PSIsIm1hYyI6IjYyNmVhOTI1YTVjODA1MWVkMWViODhlYWVkNGNhZTEyM2JmNmNkMDc0ZTA2YTY3Y2YzMjRmMDM0ZmIwOTA2ZjcifQ==','eyJpdiI6IkdFeU5RVFo3eVp5ZFhFUit4SXhcL3dnPT0iLCJ2YWx1ZSI6InNXREJUWXBDVmZscHJUYjBKWUxEQ0E9PSIsIm1hYyI6IjZlNDk3ZGQ3NTIyYzU2NGIzMjMzNTg2MWE5MjA0ZjNiOGI1MWM3YjBiMzNiYTNmYWFjZjBmNmQ3MDdjYWU0YWQifQ==',1,'2017-11-06 16:37:18','2017-11-21 19:49:25',1),(19,'Jazmy-1744','eyJpdiI6ImZHT2d5ZUNVdW5KK1NOeWZSV2dcLzlnPT0iLCJ2YWx1ZSI6Ik5PajhNaHZtQUtjMms2S3dWREpZRlV3Y1p3ZFRERFRXTU5HblBxTHVDUlE9IiwibWFjIjoiMzA2NDZkNmYxYzNmZjVjZDQxMzFhMmNkODc1NzYzNmFmYzliNjM0MjM1NTJiZDAxZjhhNGUxZmZhMTI3YTA1ZiJ9','eyJpdiI6IkVQdk9ZT01LQkxEemJDS2c4Y25NQlE9PSIsInZhbHVlIjoiS1ozNGdnTWh3RTFRUXc1aXRuY2RuUT09IiwibWFjIjoiYjQ4NWNiNTg5NmJkMjY5ZDM0ZDk4OTdjN2MwOTlkNTQ2M2VlN2Y4YWYyMjJhODEzYzQxZjNlOTgyMThjNWNmNCJ9','eyJpdiI6ImhaVW9GNFdxR0ZzN2dzZGtHOXBLMHc9PSIsInZhbHVlIjoiMUNlR3N5UEZBTDF0M0VSOFU2U25uZz09IiwibWFjIjoiOTE3OTQwZjcwMzhlY2Q0ODI2NGZjODk5YjZkMjE0NzNkYjg3ODA5N2MyZWIxYmE2NmQwNTVkYjJhMzJkMDAzMCJ9',1,'2017-11-06 16:37:18','2017-11-21 19:49:25',1),(20,'Modes-2486','eyJpdiI6InhtWW5MNUxLbDBKb2FtNDFMNjBrQnc9PSIsInZhbHVlIjoiWWExdjc5M1hjaFZEOGx1YmdoODVVQT09IiwibWFjIjoiODMxMDA4ZjgzNGEzYzFmODNkYjk5NGQ0OTU4YjkxMDFjZGM4NWY4M2E4ZWM2NTlhMzhmNjA4NTU0Y2IwM2Y2ZSJ9','eyJpdiI6IkZZK05EZkNLazcwWXFjaW9WdDBrWFE9PSIsInZhbHVlIjoiS051Ykc3NzNhYng4UEpFanBjN2w5dz09IiwibWFjIjoiYTg5MmNmMGNjMDFhMDQyN2Q1NmFlZDg0ZGU5ZGQ5ZTc3M2U3N2FkNDYyMTg5ODZlZjZjMGMzYmFkZDgwMzQ2NiJ9','eyJpdiI6ImhVQVllendqMW56aW5nQTQ4YytKZ3c9PSIsInZhbHVlIjoidmxWWGtmV21oNHRXWTBGTVZlYWFJQT09IiwibWFjIjoiZjJhM2U4OGZlZTRjMzFlMGYwYThlZmFlZDM5NDk2ZmExNGM1ODEyOTNhMzUxODU0Y2JlNDIwNWMwNmM0NmQzNSJ9',1,'2017-11-06 16:37:18','2017-11-21 19:49:25',1),(21,'Orion-4963','eyJpdiI6Imk0Z2htMU84dGpQYStPV3FxSG1tVmc9PSIsInZhbHVlIjoiQjlHQkRBOXBwRTdGY29UdEZhRmhCZz09IiwibWFjIjoiZmU5MTRjNGM5OTQxOGI2M2M5NmQ2OTM4Mjc4ZjA1YThjNGI3YmFmNTc4OGY3ZmJiNzg3ZTRkMzFlMjM5NDVkMiJ9','eyJpdiI6IlpQQVBmempkaGljS2ROaTFGT2E2b3c9PSIsInZhbHVlIjoiSkZBNmMwZHZJdk5vMUNNUjFwZWgxZz09IiwibWFjIjoiNmI0YzYzNjJjMjBmN2EyZWMzYTY4YzdjYjgwYTMxZDE1ZjkwMDE1ODk2NTE0YWIxN2JkOTlkMzM1YmZiYmRiMyJ9','eyJpdiI6IkpRMFwvaUhQRkQwYkdwdFNDUE5FZXJnPT0iLCJ2YWx1ZSI6ImxcLzhwYURJQklJR29aZ2xsWitRNjVRPT0iLCJtYWMiOiJlN2UyNDA2OTkyOTYyYjVjMjk1ZWJkNDk4NGNkYzVkZWY3MjRhY2E2N2M1MWRkMjYxNTQyZjk5ZTUxOWM1NWQzIn0=',1,'2017-11-06 16:37:18','2017-11-21 19:49:25',1),(22,'Dr.jN-5755','eyJpdiI6IkJiQUgrSE9qVUxiemlJN1FmVTNEQUE9PSIsInZhbHVlIjoiV0dJa0M2azlBdXFaUGNWdHByZnNTZz09IiwibWFjIjoiNjVjMTU5OGUyMzdjM2I5NmVlMGViYTdkNjQyMWViNGVhZDkxMzNmNjYxN2Y4ZmFlNjIwOGQ2NWQ0OGYyZTdjNSJ9','eyJpdiI6IjlhZTV1SGJMdEF4KzgrRFllMWxJcGc9PSIsInZhbHVlIjoiSm1yMkhuS3dwYis2NFhpZlozbUhkQT09IiwibWFjIjoiYjkxMjAxMWRiZTU3ZDBiZWJlMjA3ZDBkMDYwNjdmYWUyMjdjNTYyZTgwZDIxNWMwMjcxNTY3OWJjMzVjNDY4ZSJ9','eyJpdiI6InkwZ28wXC9RK2ozN01WcXFcLzZtYkdodz09IiwidmFsdWUiOiJRSVhNeVM5RDVqMFlcL2N3UE1wOWJTUT09IiwibWFjIjoiZjBkYWE4NmVmMTdjYWI3Zjk4YjAxNWY2YmJhNTY1MGFhNDQ0MzJhNjRkYzUwNTU3MzhkZTQ0OTcwN2ZhOTM3YSJ9',1,'2017-11-06 16:37:18','2017-11-21 19:49:25',1),(23,'EvajE-1180','eyJpdiI6IjVaN21YY2hieVY3MTRndUtCd0NUanc9PSIsInZhbHVlIjoiYmtvdnFJeHZkMVRzQkxGRWRZdkJXVlFla3F6bVRnbFwvdE9uZ3drazlNbkk9IiwibWFjIjoiYjBlYTQxMTI3NTFmM2QyZjNjZDcyYjAwNWM4YjQzMjQ4NmIyNTQ4NzUwYjQxYjMwNGM4ODI3Y2EzN2MyNTk1MyJ9','eyJpdiI6IjBhbEc2RXJBWmhFRVRzOEJlcHlvSmc9PSIsInZhbHVlIjoicjY1VFJnV0JDVzdLcVk0RUR1d3VXQT09IiwibWFjIjoiNWI3YTNhZDRiM2FjNjQ2NWRkZmVhNGJjOWNmZDg4Y2U0YjU1NTU2NTk2NWE0NmMxMWRiYjY0NDk3ZWJkMGU4MSJ9','eyJpdiI6ImlnZXkrVisxNHhjT25uTUx1U0pNZHc9PSIsInZhbHVlIjoiUUdpVTRYY3psdFQ0dUZcL1RmTU4rbXc9PSIsIm1hYyI6IjIyYWQ1NTc3YWViNzA2NDhiYTYwODA3ODM2NDZjMmVhNjcxOWFjZjllZDA4OGM1NzcyZTYxYjNiOWZmNTkwMjkifQ==',1,'2017-11-06 16:37:18','2017-11-21 19:49:25',1),(24,'Saige-4813','eyJpdiI6InhtK3h2TUJMcUtPSFU4MVpJZTllMlE9PSIsInZhbHVlIjoieU45S3ladEg5SllzMkIyWDFmR0NhZz09IiwibWFjIjoiN2IwYmRhMDVjZjBmNTY1YjgxZmZjZjU0MzIzOWExNTY3MjRhZmU4ZjFjZmEzNTYwNzEzYWI4OWIwZjQ5NDEwNSJ9','eyJpdiI6IlJHQWY4a1pJcEhETGlBXC9SUzRNbXBnPT0iLCJ2YWx1ZSI6IndwMHJuZ2xYcWE4MzcyeGNQWTFvS0E9PSIsIm1hYyI6IjRlZDkwNmZkMjExNThjMTRkNzdlNDVhN2Y0OTgwNjZmMTE2NzVmNDViZWMyZjFhZTczY2EzMjE0NDY2ZGIwNDUifQ==','eyJpdiI6IlwvZEJ6RE1LMElqNzBNcDUrQlE1QmxBPT0iLCJ2YWx1ZSI6IlFNb0EwcFZVZkdQN2tsMWZtVjBZWWc9PSIsIm1hYyI6IjY3MjI4ZDczM2JhOTU1ZTk0MDBkNTYxMjZlMjM2OGYyNmYzMzAxNTI0YjM4YzUxZjI3YmI2ZjhjY2ExN2QzOTcifQ==',1,'2017-11-06 16:37:18','2017-11-21 19:49:25',1),(25,'Missj-4132','eyJpdiI6IisyRjdmZThFa1FEYk8rNlwva1lzOTlBPT0iLCJ2YWx1ZSI6IllHT2JwenM1RGVVMVFuNUJWUERDWVE9PSIsIm1hYyI6ImVjNmU3YjBjZDE3OWE5MGU0MDA2M2M5MTY1ZmNlOTIwMjk2Mzg2ZTgyMThkYTNmMDY0MTRkNzljNDAxODNkY2QifQ==','eyJpdiI6ImhLdHErZzlvUDB3dGc3M29xZE1RK0E9PSIsInZhbHVlIjoiSGdWMVVId3FOUFM2MmlsdkhRSFJhZz09IiwibWFjIjoiNTIwZDU5NDE2MTIzOGJkMWI1ZmZiYTFhMzBmMjVmNGVhMDQwOTIyMmEyOWRiMmNkMTMwNjEwNTk5N2I2NWIyMCJ9','eyJpdiI6Inh6MmdJN0dRdXBGWWtiRUZMZmVsQ2c9PSIsInZhbHVlIjoib3lUOHVKWWEzV0FUQjlRV3dEMHBzQT09IiwibWFjIjoiYjJkNWQ1YjIyNjk4NzQxNDExMThiNWY3MDcwZTM2NTc5YTA0MGQxZThjNTNmM2ZhZmQwNjhjMTljMjI3OWZhNSJ9',1,'2017-11-06 16:37:18','2017-11-21 19:49:25',1);
/*!40000 ALTER TABLE `meg_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_procedures`
--

DROP TABLE IF EXISTS `meg_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_procedures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `procedures_id_index` (`id`),
  KEY `fk_meg_procedures_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_procedures_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_procedures`
--

LOCK TABLES `meg_procedures` WRITE;
/*!40000 ALTER TABLE `meg_procedures` DISABLE KEYS */;
INSERT INTO `meg_procedures` VALUES (1,'Indicaciones',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(2,'Inserción',1,'2017-11-06 16:37:18','2017-11-06 16:37:18',1),(3,'Mantenimiento',1,'2017-11-06 16:37:19','2017-11-06 16:37:19',1);
/*!40000 ALTER TABLE `meg_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_roles`
--

DROP TABLE IF EXISTS `meg_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `level` int(11) NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `parent_role_id` int(10) unsigned DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_rol_title_unique` (`title`),
  KEY `roles_id_index` (`id`),
  KEY `roles_organization_foreign` (`organization_id`),
  KEY `fk_meg_roles_parent_role_id1` (`parent_role_id`),
  CONSTRAINT `fk_meg_roles_parent_role_id1` FOREIGN KEY (`parent_role_id`) REFERENCES `meg_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `roles_organization_foreign` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_roles`
--

LOCK TABLES `meg_roles` WRITE;
/*!40000 ALTER TABLE `meg_roles` DISABLE KEYS */;
INSERT INTO `meg_roles` VALUES (1,'enfermeras',2,1,1,NULL,'2017-11-06 16:37:16','2017-11-06 16:37:16');
/*!40000 ALTER TABLE `meg_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_rooms`
--

DROP TABLE IF EXISTS `meg_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `identity` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rooms_room_identity_unique` (`identity`),
  KEY `rooms_id_index` (`id`),
  KEY `fk_meg_rooms_meg_organizations1_idx` (`organization_id`),
  CONSTRAINT `fk_meg_rooms_meg_organizations1` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_rooms`
--

LOCK TABLES `meg_rooms` WRITE;
/*!40000 ALTER TABLE `meg_rooms` DISABLE KEYS */;
INSERT INTO `meg_rooms` VALUES (1,'2668','Operación',1,'2017-11-06 16:37:20','2017-11-06 16:37:20',1),(2,'8976','UCI',1,'2017-11-06 16:37:20','2017-11-06 16:37:20',1),(3,'5781','UTI',1,'2017-11-06 16:37:20','2017-11-06 16:37:20',1);
/*!40000 ALTER TABLE `meg_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_users`
--

DROP TABLE IF EXISTS `meg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `firstname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `lastname` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `picture` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `mobile_phone` varchar(12) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role_id` int(10) unsigned NOT NULL,
  `organization_id` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `last_login` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_email_unique` (`email`),
  UNIQUE KEY `mobile_phone_UNIQUE` (`mobile_phone`),
  KEY `users_user_type_foreign` (`role_id`),
  KEY `users_main_organization_foreign` (`organization_id`),
  CONSTRAINT `users_organization_id_FK` FOREIGN KEY (`organization_id`) REFERENCES `meg_organizations` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `users_role_id_FK` FOREIGN KEY (`role_id`) REFERENCES `meg_roles` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_users`
--

LOCK TABLES `meg_users` WRITE;
/*!40000 ALTER TABLE `meg_users` DISABLE KEYS */;
INSERT INTO `meg_users` VALUES (1,'eyJpdiI6IlYzT2RIQmxnNFVsYTRsRDBcL1luS0NBPT0iLCJ2YWx1ZSI6IlhhWVVTOUgrQmJhendwR2Z3TlM4MlE9PSIsIm1hYyI6ImI4MjFjYTFjMjQxODJhOWM3MjM0NDYzNGEzYmM1ZDE3MDQyZjcyNjAxY2IyMTk2OTFhNzgwYTZmZjU4MzRkYmQifQ==','eyJpdiI6IjRaY3pHdlAwemlDbGJVZDNRaXQ3QUE9PSIsInZhbHVlIjoiQ0gxWFZSQWN5OXZhVFBcL0JURFArMmE5VHFWa2J2MmcrT2ZLZlpkaEJ0S3c9IiwibWFjIjoiZmEwY2QzZjllMjU1MmEyMDMwYzRhZWJjMWRjNTBhMTc0Y2ZlYjk1Y2E3NTNmYzhmN2RiZjU5YTVlZjUyMzdkZiJ9','$2y$10$K5pmIsgi96kgPLw..oJFbO1w81o9AOG5Ir24sJ.473rCYCmZgzo2.','','norberto44@example.com',NULL,1,1,1,'2017-11-06 05:00:00','2017-11-06 16:37:17','2017-11-21 20:25:03');
/*!40000 ALTER TABLE `meg_users` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-21 15:31:21
