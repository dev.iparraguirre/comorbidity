-- MySQL dump 10.16  Distrib 10.1.28-MariaDB, for Win32 (AMD64)
--
-- Host: 127.0.0.1    Database: co_morbidity
-- ------------------------------------------------------
-- Server version	10.1.28-MariaDB

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meg_activity_procedures`
--

DROP TABLE IF EXISTS `meg_activity_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_activity_procedures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL,
  `notification_type_id` int(10) unsigned NOT NULL,
  `procedures_id` int(10) unsigned NOT NULL,
  `alert_type_id` int(10) unsigned NOT NULL,
  `has_alert` tinyint(4) NOT NULL DEFAULT '0',
  `sequence` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `messages_id` int(10) unsigned DEFAULT NULL,
  `is_title` tinyint(4) NOT NULL DEFAULT '0',
  `parent_activity_id` int(10) unsigned DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`id`),
  KEY `activity_procedures_id_index` (`id`),
  KEY `activity_procedures_activity_id_foreign` (`procedures_id`),
  KEY `activity_procedures_parent_activity_id_idx` (`parent_activity_id`),
  KEY `activity_procedures_messages_id_foreign_idx` (`messages_id`),
  CONSTRAINT `activity_procedures_activity_id_foreign` FOREIGN KEY (`procedures_id`) REFERENCES `meg_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activity_procedures_messages_id_foreign` FOREIGN KEY (`messages_id`) REFERENCES `meg_messages` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `activity_procedures_parent_activity_id_foreign` FOREIGN KEY (`parent_activity_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_activity_procedures`
--

LOCK TABLES `meg_activity_procedures` WRITE;
/*!40000 ALTER TABLE `meg_activity_procedures` DISABLE KEYS */;
INSERT INTO `meg_activity_procedures` VALUES (1,'Retención urinaria aguda',0,1,1,1,1,'2017-11-06 16:37:19','2017-11-06 19:57:31',2,0,NULL,1),(2,'Inserte tubo a la longitud apropiada',0,2,1,1,6,'2017-11-06 16:37:19','2017-11-06 16:37:19',1,0,29,1),(3,'Bolsa situada debajo de la vejiga',0,3,0,0,1,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(4,'Herida sacra abierta o perinal',0,1,0,0,2,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(5,'Flujo sin obstrucciones',0,3,0,0,2,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(6,'Sistema de drenaje cerrado',0,3,0,0,3,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(7,'Contención fecal apropiada',0,3,0,0,4,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(8,'La familia entiende el mantenimiento',0,3,0,0,5,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(9,'Comodidad al final de la vida',0,1,0,0,3,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(10,'Vacíe la bolsa cada ocho (8) horas',0,3,0,0,7,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(11,'Tubo no toca la bolsa al vaciar',0,3,0,0,8,'2017-11-06 16:37:19','2017-11-06 16:37:19',NULL,0,NULL,1),(13,'Necesidad de una medición precisa',0,1,0,0,4,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,0,NULL,1),(14,'Inmovilización prolongada',0,1,0,0,5,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,0,NULL,1),(15,'IUC más pequeño seleccionado',0,2,0,0,2,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,0,28,1),(16,'Cuidado personal e higiene de manos',0,2,0,0,3,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,0,28,1),(19,'Vacíe la bolsa antes que se llene',0,3,2,1,6,'2017-11-06 16:37:20','2017-11-06 16:37:20',3,0,NULL,1),(21,'Indicación no conforme',0,1,0,0,6,'2017-11-10 11:05:20','2017-11-10 11:05:20',NULL,0,NULL,1),(23,'Guantes y equipo estériles',0,2,0,0,5,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,0,29,1),(24,'Inflar el globo según las instrucciones',0,2,0,0,7,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,0,29,1),(25,'Bolsa de denaje a nivel de la cintura',0,2,0,0,8,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,0,29,1),(26,'Conexión cerrada',0,2,0,0,9,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,0,29,1),(28,'Antes de la Inserción',0,2,0,0,1,'2017-11-06 16:37:20','2017-11-06 16:37:20',NULL,1,NULL,1),(29,'Inserción',0,2,0,0,4,'2017-11-10 11:12:20','2017-11-10 11:12:20',NULL,1,NULL,1);
/*!40000 ALTER TABLE `meg_activity_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_alerts_activities`
--

DROP TABLE IF EXISTS `meg_alerts_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_alerts_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `activity_procedures_id` int(10) unsigned NOT NULL,
  `alert_type_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  `patients_id` int(11) unsigned NOT NULL,
  `status` int(11) NOT NULL DEFAULT '0',
  `next_alert` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `alerts_activities_id_index` (`id`),
  KEY `alerts_activities_alert_main_foreign` (`activity_procedures_id`),
  KEY `meg_patients_id_idx` (`patients_id`),
  CONSTRAINT `alerts_activities_meg_activity_procedures_id_foreign` FOREIGN KEY (`activity_procedures_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `alerts_activities_meg_patients_id_foreign` FOREIGN KEY (`id`) REFERENCES `meg_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_alerts_activities`
--

LOCK TABLES `meg_alerts_activities` WRITE;
/*!40000 ALTER TABLE `meg_alerts_activities` DISABLE KEYS */;
INSERT INTO `meg_alerts_activities` VALUES (1,2,2,'2017-11-12 12:41:43','2017-11-15 00:03:28',11,0,NULL),(2,19,3,'2017-11-13 12:46:29','2017-11-15 00:03:28',11,0,NULL),(3,2,2,'2017-11-13 23:32:50','2017-11-15 00:03:28',2,0,NULL),(4,1,1,'2017-11-14 22:10:34','2017-11-15 00:03:28',1,0,NULL),(5,4,1,'2017-11-14 22:10:34','2017-11-15 00:03:28',1,0,NULL),(6,3,3,'2017-11-14 22:37:31','2017-11-15 00:03:28',1,0,NULL),(7,19,3,'2017-11-14 22:37:31','2017-11-15 00:03:28',1,0,NULL),(8,10,3,'2017-11-14 22:37:31','2017-11-15 00:03:28',1,0,NULL),(9,19,3,'2017-11-15 18:37:26',NULL,1,0,NULL),(10,10,3,'2017-11-15 18:37:26',NULL,1,0,NULL);
/*!40000 ALTER TABLE `meg_alerts_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_config_platform`
--

DROP TABLE IF EXISTS `meg_config_platform`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_config_platform` (
  `alerts_dashboard` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `bgcolor` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `mail_domain` varchar(45) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_config_platform`
--

LOCK TABLES `meg_config_platform` WRITE;
/*!40000 ALTER TABLE `meg_config_platform` DISABLE KEYS */;
INSERT INTO `meg_config_platform` VALUES ('2-19/1-2/2',NULL,NULL,1,'2017-11-15 15:01:28','0000-00-00 00:00:00'),('',NULL,NULL,1,'2017-11-15 15:01:28','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `meg_config_platform` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_devices`
--

DROP TABLE IF EXISTS `meg_devices`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_devices` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(100) CHARACTER SET utf8mb4 COLLATE utf8mb4_unicode_ci NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `procedures_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_devices`
--

LOCK TABLES `meg_devices` WRITE;
/*!40000 ALTER TABLE `meg_devices` DISABLE KEYS */;
INSERT INTO `meg_devices` VALUES (1,'Sonda Foley',1,1,'2017-11-14 16:30:56','0000-00-00 00:00:00'),(2,'Sonda Nelaton',2,1,'2017-11-14 16:30:56','0000-00-00 00:00:00');
/*!40000 ALTER TABLE `meg_devices` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_devices_procedures`
--

DROP TABLE IF EXISTS `meg_devices_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_devices_procedures` (
  `meg_devices_id` int(10) unsigned NOT NULL,
  `meg_procedures_id` int(10) unsigned NOT NULL,
  `sequence` int(10) unsigned NOT NULL,
  PRIMARY KEY (`meg_devices_id`,`meg_procedures_id`),
  KEY `fk_meg_devices_procedures_meg_devices1_idx` (`meg_devices_id`),
  KEY `fk_meg_devices_procedures_meg_procedures1` (`meg_procedures_id`),
  CONSTRAINT `fk_meg_devices_procedures_meg_devices1` FOREIGN KEY (`meg_devices_id`) REFERENCES `meg_devices` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_meg_devices_procedures_meg_procedures1` FOREIGN KEY (`meg_procedures_id`) REFERENCES `meg_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_devices_procedures`
--

LOCK TABLES `meg_devices_procedures` WRITE;
/*!40000 ALTER TABLE `meg_devices_procedures` DISABLE KEYS */;
INSERT INTO `meg_devices_procedures` VALUES (1,1,1),(1,2,2),(1,3,3),(2,1,1),(2,2,2),(2,3,3);
/*!40000 ALTER TABLE `meg_devices_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_log_access`
--

DROP TABLE IF EXISTS `meg_log_access`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_log_access` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `acces_type_id` int(11) NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `log_access_id_index` (`id`),
  KEY `meg_log_access_user_id_FK_idx` (`user_id`),
  CONSTRAINT `meg_log_access_user_id_FK` FOREIGN KEY (`user_id`) REFERENCES `meg_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_log_access`
--

LOCK TABLES `meg_log_access` WRITE;
/*!40000 ALTER TABLE `meg_log_access` DISABLE KEYS */;
/*!40000 ALTER TABLE `meg_log_access` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_logs_activities`
--

DROP TABLE IF EXISTS `meg_logs_activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_logs_activities` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `generate_alert` tinyint(4) NOT NULL,
  `activity_procedures_id` int(10) unsigned NOT NULL,
  `patient_id` int(10) unsigned NOT NULL,
  `user_id` int(10) unsigned NOT NULL,
  `room_id` int(10) unsigned NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `logs_activities_id_index` (`id`),
  KEY `logs_activities_main_activity_foreign` (`activity_procedures_id`),
  KEY `logs_activities_main_patient_foreign` (`patient_id`),
  KEY `logs_activities_main_user_foreign` (`user_id`),
  KEY `logs_activities_main_room_foreign` (`room_id`),
  CONSTRAINT `logs_activities_activity_procedures_id_FK` FOREIGN KEY (`activity_procedures_id`) REFERENCES `meg_activity_procedures` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_patient_id_FK` FOREIGN KEY (`patient_id`) REFERENCES `meg_patients` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_room_id_FK` FOREIGN KEY (`room_id`) REFERENCES `meg_rooms` (`id`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `logs_activities_user_id_FK` FOREIGN KEY (`user_id`) REFERENCES `meg_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=290 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_logs_activities`
--

LOCK TABLES `meg_logs_activities` WRITE;
/*!40000 ALTER TABLE `meg_logs_activities` DISABLE KEYS */;
INSERT INTO `meg_logs_activities` VALUES (1,1,1,1,1,1,'2017-11-08 07:37:35','2017-11-15 17:28:01'),(2,0,4,1,1,1,'2017-11-08 07:37:35','2017-11-15 17:28:01'),(3,0,9,1,1,1,'2017-11-08 07:37:35','2017-11-15 17:28:01'),(4,0,13,1,1,1,'2017-11-08 07:37:35','2017-11-15 17:28:01'),(5,0,14,1,1,1,'2017-11-08 07:37:35','2017-11-15 17:28:01'),(6,1,2,1,1,1,'2017-11-08 07:37:39','2017-11-15 17:28:01'),(7,0,15,1,1,1,'2017-11-08 07:37:39','2017-11-15 17:28:01'),(8,0,16,1,1,1,'2017-11-08 07:37:39','2017-11-15 17:28:01'),(10,1,1,1,1,1,'2017-11-08 07:38:41','2017-11-15 17:28:01'),(11,0,4,1,1,1,'2017-11-08 07:38:41','2017-11-15 17:28:01'),(12,0,9,1,1,1,'2017-11-08 07:38:41','2017-11-15 17:28:01'),(13,0,13,1,1,1,'2017-11-08 07:38:41','2017-11-15 17:28:01'),(14,0,14,1,1,1,'2017-11-08 07:38:41','2017-11-15 17:28:01'),(15,1,1,1,1,1,'2017-11-08 08:08:47','2017-11-15 17:28:01'),(16,0,4,1,1,1,'2017-11-08 08:08:47','2017-11-15 17:28:01'),(17,0,9,1,1,1,'2017-11-08 08:08:47','2017-11-15 17:28:01'),(18,0,13,1,1,1,'2017-11-08 08:08:47','2017-11-15 17:28:01'),(19,0,14,1,1,1,'2017-11-08 08:08:47','2017-11-15 17:28:01'),(20,0,3,1,1,1,'2017-11-08 08:12:13','2017-11-15 17:28:01'),(21,0,5,1,1,1,'2017-11-08 08:12:13','2017-11-15 17:28:01'),(22,0,6,1,1,1,'2017-11-08 08:12:13','2017-11-15 17:28:01'),(23,0,7,1,1,1,'2017-11-08 08:12:13','2017-11-15 17:28:01'),(24,0,8,1,1,1,'2017-11-08 08:12:13','2017-11-15 17:28:01'),(25,0,10,1,1,1,'2017-11-08 08:12:13','2017-11-15 17:28:01'),(26,0,11,1,1,1,'2017-11-08 08:12:13','2017-11-15 17:28:01'),(29,1,19,1,1,1,'2017-11-08 08:12:13','2017-11-15 17:28:01'),(31,1,1,1,1,1,'2017-11-08 08:13:20','2017-11-15 17:28:01'),(32,0,4,1,1,1,'2017-11-08 08:13:20','2017-11-15 17:28:01'),(33,0,9,1,1,1,'2017-11-08 08:13:20','2017-11-15 17:28:01'),(34,0,13,1,1,1,'2017-11-08 08:13:20','2017-11-15 17:28:01'),(35,0,14,1,1,1,'2017-11-08 08:13:20','2017-11-15 17:28:01'),(36,0,3,1,1,1,'2017-11-08 08:13:24','2017-11-15 17:28:01'),(37,0,5,1,1,1,'2017-11-08 08:13:24','2017-11-15 17:28:01'),(38,0,6,1,1,1,'2017-11-08 08:13:24','2017-11-15 17:28:01'),(39,0,7,1,1,1,'2017-11-08 08:13:24','2017-11-15 17:28:01'),(40,0,8,1,1,1,'2017-11-08 08:13:24','2017-11-15 17:28:01'),(41,0,10,1,1,1,'2017-11-08 08:13:24','2017-11-15 17:28:01'),(42,0,11,1,1,1,'2017-11-08 08:13:24','2017-11-15 17:28:01'),(45,1,19,1,1,1,'2017-11-08 08:13:24','2017-11-15 17:28:01'),(47,0,3,1,1,1,'2017-11-08 08:14:27','2017-11-15 17:28:01'),(48,0,5,1,1,1,'2017-11-08 08:14:27','2017-11-15 17:28:01'),(49,0,6,1,1,1,'2017-11-08 08:14:27','2017-11-15 17:28:01'),(50,0,7,1,1,1,'2017-11-08 08:14:27','2017-11-15 17:28:01'),(51,0,8,1,1,1,'2017-11-08 08:14:27','2017-11-15 17:28:01'),(52,0,10,1,1,1,'2017-11-08 08:14:27','2017-11-15 17:28:01'),(53,0,11,1,1,1,'2017-11-08 08:14:27','2017-11-15 17:28:01'),(56,1,19,1,1,1,'2017-11-08 08:14:27','2017-11-15 17:28:01'),(58,1,1,1,1,1,'2017-11-08 08:14:34','2017-11-15 17:28:01'),(59,0,4,1,1,1,'2017-11-08 08:14:34','2017-11-15 17:28:01'),(60,0,9,1,1,1,'2017-11-08 08:14:34','2017-11-15 17:28:01'),(61,0,13,1,1,1,'2017-11-08 08:14:34','2017-11-15 17:28:01'),(62,0,14,1,1,1,'2017-11-08 08:14:34','2017-11-15 17:28:01'),(63,1,1,2,1,2,'2017-11-08 08:39:01','2017-11-15 17:28:01'),(64,0,4,2,1,2,'2017-11-08 08:39:01','2017-11-15 17:28:01'),(65,0,9,2,1,2,'2017-11-08 08:39:01','2017-11-15 17:28:01'),(66,0,13,2,1,2,'2017-11-08 08:39:01','2017-11-15 17:28:01'),(67,0,14,2,1,2,'2017-11-08 08:39:01','2017-11-15 17:28:01'),(68,0,3,2,1,2,'2017-11-08 08:41:23','2017-11-15 17:28:01'),(69,0,5,2,1,2,'2017-11-08 08:41:23','2017-11-15 17:28:01'),(70,1,6,2,1,2,'2017-11-08 08:41:23','2017-11-15 17:28:01'),(71,0,7,2,1,2,'2017-11-08 08:41:23','2017-11-15 17:28:01'),(72,0,8,2,1,2,'2017-11-08 08:41:23','2017-11-15 17:28:01'),(73,0,10,2,1,2,'2017-11-08 08:41:23','2017-11-15 17:28:01'),(74,1,11,2,1,2,'2017-11-08 08:41:23','2017-11-15 17:28:01'),(77,1,19,2,1,2,'2017-11-08 08:41:23','2017-11-15 17:28:01'),(79,1,2,2,1,2,'2017-11-08 08:41:29','2017-11-15 17:28:01'),(80,0,15,2,1,2,'2017-11-08 08:41:29','2017-11-15 17:28:01'),(81,1,16,2,1,2,'2017-11-08 08:41:29','2017-11-15 17:28:01'),(83,1,1,1,1,1,'2017-11-08 20:15:32','2017-11-15 17:28:01'),(84,0,4,1,1,1,'2017-11-08 20:15:32','2017-11-15 17:28:01'),(85,0,9,1,1,1,'2017-11-08 20:15:32','2017-11-15 17:28:01'),(86,0,13,1,1,1,'2017-11-08 20:15:32','2017-11-15 17:28:01'),(87,0,14,1,1,1,'2017-11-08 20:15:32','2017-11-15 17:28:01'),(88,1,2,2,1,2,'2017-11-09 16:12:08','2017-11-15 17:28:01'),(89,0,15,2,1,2,'2017-11-09 16:12:08','2017-11-15 17:28:01'),(90,0,16,2,1,2,'2017-11-09 16:12:08','2017-11-15 17:28:01'),(92,1,1,2,1,2,'2017-11-09 16:45:13','2017-11-15 17:28:01'),(93,0,4,2,1,2,'2017-11-09 16:45:13','2017-11-15 17:28:01'),(94,0,9,2,1,2,'2017-11-09 16:45:13','2017-11-15 17:28:01'),(95,0,13,2,1,2,'2017-11-09 16:45:13','2017-11-15 17:28:01'),(96,0,14,2,1,2,'2017-11-09 16:45:13','2017-11-15 17:28:01'),(97,1,1,2,1,2,'2017-11-09 13:01:03','2017-11-15 17:28:01'),(98,0,4,2,1,2,'2017-11-09 19:01:03','2017-11-15 17:28:01'),(99,0,9,2,1,2,'2017-11-09 19:01:03','2017-11-15 17:28:01'),(100,0,13,2,1,2,'2017-11-09 19:01:03','2017-11-15 17:28:01'),(101,0,14,2,1,2,'2017-11-09 19:01:03','2017-11-15 17:28:01'),(102,1,1,1,1,1,'2017-11-10 10:40:35','2017-11-15 17:28:01'),(103,0,4,1,1,1,'2017-11-10 10:40:35','2017-11-15 17:28:01'),(104,0,9,1,1,1,'2017-11-10 10:40:35','2017-11-15 17:28:01'),(105,0,13,1,1,1,'2017-11-10 10:40:35','2017-11-15 17:28:01'),(106,0,14,1,1,1,'2017-11-10 10:40:35','2017-11-15 17:28:01'),(107,1,2,1,1,1,'2017-11-10 10:41:03','2017-11-15 17:28:01'),(108,0,15,1,1,1,'2017-11-10 10:41:03','2017-11-15 17:28:01'),(109,0,16,1,1,1,'2017-11-10 10:41:03','2017-11-15 17:28:01'),(111,1,2,1,1,1,'2017-11-10 10:41:17','2017-11-15 17:28:01'),(112,0,15,1,1,1,'2017-11-10 10:41:17','2017-11-15 17:28:01'),(113,0,16,1,1,1,'2017-11-10 10:41:17','2017-11-15 17:28:01'),(115,0,3,1,1,2,'2017-11-13 10:19:22','2017-11-15 17:28:01'),(116,0,5,1,1,2,'2017-11-13 10:19:22','2017-11-15 17:28:01'),(117,0,6,1,1,2,'2017-11-13 10:19:22','2017-11-15 17:28:01'),(118,0,7,1,1,2,'2017-11-13 10:19:22','2017-11-15 17:28:01'),(119,0,8,1,1,2,'2017-11-13 10:19:22','2017-11-15 17:28:01'),(120,0,10,1,1,2,'2017-11-13 10:19:22','2017-11-15 17:28:01'),(121,0,11,1,1,2,'2017-11-13 10:19:22','2017-11-15 17:28:01'),(124,1,19,1,1,2,'2017-11-13 10:19:22','2017-11-15 17:28:01'),(126,0,3,2,1,3,'2017-11-13 16:31:36','2017-11-15 17:28:01'),(127,0,5,2,1,3,'2017-11-13 16:31:36','2017-11-15 17:28:01'),(128,0,6,2,1,3,'2017-11-13 16:31:36','2017-11-15 17:28:01'),(129,0,7,2,1,3,'2017-11-13 16:31:36','2017-11-15 17:28:01'),(130,0,8,2,1,3,'2017-11-13 16:31:36','2017-11-15 17:28:01'),(131,0,10,2,1,3,'2017-11-13 16:31:36','2017-11-15 17:28:01'),(132,0,11,2,1,3,'2017-11-13 16:31:36','2017-11-15 17:28:01'),(133,1,19,2,1,3,'2017-11-13 16:31:36','2017-11-15 17:28:01'),(134,0,2,2,1,3,'2017-11-13 16:31:49','2017-11-15 17:28:01'),(135,0,15,2,1,3,'2017-11-13 16:31:49','2017-11-15 17:28:01'),(136,0,16,2,1,3,'2017-11-13 16:31:49','2017-11-15 17:28:01'),(139,0,23,2,1,3,'2017-11-13 16:31:49','2017-11-15 17:28:01'),(140,0,24,2,1,3,'2017-11-13 16:31:49','2017-11-15 17:28:01'),(141,0,25,2,1,3,'2017-11-13 16:31:49','2017-11-15 17:28:01'),(142,0,26,2,1,3,'2017-11-13 16:31:49','2017-11-15 17:28:01'),(143,1,2,2,1,3,'2017-11-13 16:32:15','2017-11-15 17:28:01'),(144,0,15,2,1,3,'2017-11-13 16:32:15','2017-11-15 17:28:01'),(145,0,16,2,1,3,'2017-11-13 16:32:15','2017-11-15 17:28:01'),(148,0,23,2,1,3,'2017-11-13 16:32:15','2017-11-15 17:28:01'),(149,0,24,2,1,3,'2017-11-13 16:32:15','2017-11-15 17:28:01'),(150,0,25,2,1,3,'2017-11-13 16:32:15','2017-11-15 17:28:01'),(151,0,26,2,1,3,'2017-11-13 16:32:15','2017-11-15 17:28:01'),(152,1,2,2,1,3,'2017-11-13 16:32:22','2017-11-15 17:28:01'),(153,0,15,2,1,3,'2017-11-13 16:32:22','2017-11-15 17:28:01'),(154,0,16,2,1,3,'2017-11-13 16:32:22','2017-11-15 17:28:01'),(157,0,23,2,1,3,'2017-11-13 16:32:22','2017-11-15 17:28:01'),(158,0,24,2,1,3,'2017-11-13 16:32:22','2017-11-15 17:28:01'),(159,0,25,2,1,3,'2017-11-13 16:32:22','2017-11-15 17:28:01'),(160,0,26,2,1,3,'2017-11-13 16:32:22','2017-11-15 17:28:01'),(161,1,1,11,1,3,'2017-11-13 16:34:10','2017-11-15 17:28:01'),(162,0,4,11,1,3,'2017-11-13 16:34:10','2017-11-15 17:28:01'),(163,1,9,11,1,3,'2017-11-13 16:34:10','2017-11-15 17:28:01'),(164,0,13,11,1,3,'2017-11-13 16:34:10','2017-11-15 17:28:01'),(165,0,14,11,1,3,'2017-11-13 16:34:10','2017-11-15 17:28:01'),(166,0,21,11,1,3,'2017-11-13 16:34:10','2017-11-15 17:28:01'),(167,1,2,11,1,3,'2017-11-13 16:34:18','2017-11-15 17:28:01'),(168,0,15,11,1,3,'2017-11-13 16:34:18','2017-11-15 17:28:01'),(169,0,16,11,1,3,'2017-11-13 16:34:18','2017-11-15 17:28:01'),(172,0,23,11,1,3,'2017-11-13 16:34:18','2017-11-15 17:28:01'),(173,0,24,11,1,3,'2017-11-13 16:34:18','2017-11-15 17:28:01'),(174,0,25,11,1,3,'2017-11-13 16:34:18','2017-11-15 17:28:01'),(175,0,26,11,1,3,'2017-11-13 16:34:18','2017-11-15 17:28:01'),(177,1,15,2,1,2,'2017-11-13 19:54:31','2017-11-15 17:28:01'),(178,0,16,2,1,2,'2017-11-13 19:54:31','2017-11-15 17:28:01'),(180,0,23,2,1,2,'2017-11-13 19:54:31','2017-11-15 17:28:01'),(181,1,2,2,1,2,'2017-11-13 19:54:31','2017-11-15 17:28:01'),(182,1,24,2,1,2,'2017-11-13 19:54:31','2017-11-15 17:28:01'),(183,1,25,2,1,2,'2017-11-13 19:54:31','2017-11-15 17:28:01'),(184,0,26,2,1,2,'2017-11-13 19:54:31','2017-11-15 17:28:01'),(186,0,15,11,1,2,'2017-11-13 21:17:07','2017-11-15 17:28:01'),(187,0,16,11,1,2,'2017-11-13 21:17:07','2017-11-15 17:28:01'),(189,0,23,11,1,2,'2017-11-13 21:17:07','2017-11-15 17:28:01'),(190,1,2,11,1,2,'2017-11-13 15:17:07','2017-11-15 17:28:01'),(191,0,24,11,1,2,'2017-11-13 21:17:07','2017-11-15 17:28:01'),(192,1,25,11,1,2,'2017-11-13 15:17:07','2017-11-15 17:28:01'),(193,0,26,11,1,2,'2017-11-13 21:17:07','2017-11-15 17:28:01'),(195,0,15,11,1,2,'2017-11-13 21:35:19','2017-11-15 17:28:01'),(196,0,16,11,1,2,'2017-11-13 21:35:19','2017-11-15 17:28:01'),(198,0,23,11,1,2,'2017-11-13 21:35:19','2017-11-15 17:28:01'),(199,1,2,11,1,2,'2017-11-13 21:35:19','2017-11-15 17:28:01'),(200,0,24,11,1,2,'2017-11-13 21:35:19','2017-11-15 17:28:01'),(201,0,25,11,1,2,'2017-11-13 21:35:19','2017-11-15 17:28:01'),(202,0,26,11,1,2,'2017-11-13 21:35:19','2017-11-15 17:28:01'),(203,0,3,11,1,2,'2017-11-13 21:49:51','2017-11-15 17:28:01'),(204,0,5,11,1,2,'2017-11-13 21:49:51','2017-11-15 17:28:01'),(205,0,6,11,1,2,'2017-11-13 21:49:51','2017-11-15 17:28:01'),(206,0,7,11,1,2,'2017-11-13 21:49:51','2017-11-15 17:28:01'),(207,0,8,11,1,2,'2017-11-13 21:49:51','2017-11-15 17:28:01'),(208,0,19,11,1,2,'2017-11-13 21:49:51','2017-11-15 17:28:01'),(209,1,10,11,1,2,'2017-11-13 21:49:51','2017-11-15 17:28:01'),(210,0,11,11,1,2,'2017-11-13 21:49:51','2017-11-15 17:28:01'),(211,0,3,11,1,2,'2017-11-13 21:50:42','2017-11-15 17:28:01'),(212,0,5,11,1,2,'2017-11-13 21:50:42','2017-11-15 17:28:01'),(213,0,6,11,1,2,'2017-11-13 21:50:42','2017-11-15 17:28:01'),(214,0,7,11,1,2,'2017-11-13 21:50:42','2017-11-15 17:28:01'),(215,0,8,11,1,2,'2017-11-13 21:50:42','2017-11-15 17:28:01'),(216,1,19,11,1,2,'2017-11-13 21:50:42','2017-11-15 17:28:01'),(217,1,10,11,1,2,'2017-11-13 21:50:42','2017-11-15 17:28:01'),(218,0,11,11,1,2,'2017-11-13 21:50:42','2017-11-15 17:28:01'),(219,0,3,11,1,2,'2017-11-13 21:51:26','2017-11-15 17:28:01'),(220,0,5,11,1,2,'2017-11-13 21:51:26','2017-11-15 17:28:01'),(221,0,6,11,1,2,'2017-11-13 21:51:26','2017-11-15 17:28:01'),(222,0,7,11,1,2,'2017-11-13 21:51:26','2017-11-15 17:28:01'),(223,0,8,11,1,2,'2017-11-13 21:51:26','2017-11-15 17:28:01'),(224,1,19,11,1,2,'2017-11-13 21:51:26','2017-11-15 17:28:01'),(225,1,10,11,1,2,'2017-11-13 21:51:26','2017-11-15 17:28:01'),(226,0,11,11,1,2,'2017-11-13 21:51:26','2017-11-15 17:28:01'),(227,1,3,11,1,2,'2017-11-13 22:11:03','2017-11-15 17:28:01'),(228,0,5,11,1,2,'2017-11-13 22:11:03','2017-11-15 17:28:01'),(229,0,6,11,1,2,'2017-11-13 22:11:03','2017-11-15 17:28:01'),(230,0,7,11,1,2,'2017-11-13 22:11:03','2017-11-15 17:28:01'),(231,0,8,11,1,2,'2017-11-13 22:11:03','2017-11-15 17:28:01'),(232,0,19,11,1,2,'2017-11-13 22:11:03','2017-11-15 17:28:01'),(233,1,10,11,1,2,'2017-11-13 22:11:03','2017-11-15 17:28:01'),(234,0,11,11,1,2,'2017-11-13 22:11:03','2017-11-15 17:28:01'),(235,0,3,11,1,2,'2017-11-13 22:11:17','2017-11-15 17:28:01'),(236,0,5,11,1,2,'2017-11-13 22:11:17','2017-11-15 17:28:01'),(237,0,6,11,1,2,'2017-11-13 22:11:17','2017-11-15 17:28:01'),(238,0,7,11,1,2,'2017-11-13 22:11:17','2017-11-15 17:28:01'),(239,0,8,11,1,2,'2017-11-13 22:11:17','2017-11-15 17:28:01'),(240,1,19,11,1,2,'2017-11-13 22:11:17','2017-11-15 17:28:01'),(241,0,10,11,1,2,'2017-11-13 22:11:17','2017-11-15 17:28:01'),(242,0,11,11,1,2,'2017-11-13 22:11:17','2017-11-15 17:28:01'),(244,0,15,11,1,2,'2017-11-13 22:41:43','2017-11-15 17:28:01'),(245,0,16,11,1,2,'2017-11-13 22:41:43','2017-11-15 17:28:01'),(247,0,23,11,1,2,'2017-11-13 22:41:43','2017-11-15 17:28:01'),(248,1,2,11,1,2,'2017-11-13 22:41:43','2017-11-15 17:28:01'),(249,0,24,11,1,2,'2017-11-13 22:41:43','2017-11-15 17:28:01'),(250,0,25,11,1,2,'2017-11-13 22:41:43','2017-11-15 17:28:01'),(251,0,26,11,1,2,'2017-11-13 22:41:43','2017-11-15 17:28:01'),(252,0,3,11,1,2,'2017-11-13 22:46:29','2017-11-15 17:28:01'),(253,0,5,11,1,2,'2017-11-13 22:46:29','2017-11-15 17:28:01'),(254,0,6,11,1,2,'2017-11-13 22:46:29','2017-11-15 17:28:01'),(255,0,7,11,1,2,'2017-11-13 22:46:29','2017-11-15 17:28:01'),(256,0,8,11,1,2,'2017-11-13 22:46:29','2017-11-15 17:28:01'),(257,1,19,11,1,2,'2017-11-13 22:46:29','2017-11-15 17:28:01'),(258,0,10,11,1,2,'2017-11-13 22:46:29','2017-11-15 17:28:01'),(259,0,11,11,1,2,'2017-11-13 22:46:29','2017-11-15 17:28:01'),(260,0,15,2,1,3,'2017-11-13 23:32:50','2017-11-15 17:28:01'),(261,0,16,2,1,3,'2017-11-13 23:32:50','2017-11-15 17:28:01'),(263,0,23,2,1,3,'2017-11-13 23:32:50','2017-11-15 17:28:01'),(264,1,2,2,1,3,'2017-11-13 23:32:50','2017-11-15 17:28:01'),(265,0,24,2,1,3,'2017-11-13 23:32:50','2017-11-15 17:28:01'),(266,0,25,2,1,3,'2017-11-13 23:32:50','2017-11-15 17:28:01'),(267,1,1,1,1,1,'2017-11-14 22:10:34','2017-11-15 17:28:01'),(268,1,4,1,1,1,'2017-11-14 22:10:34','2017-11-15 17:28:01'),(269,0,9,1,1,1,'2017-11-14 22:10:34','2017-11-15 17:28:01'),(270,0,13,1,1,1,'2017-11-14 22:10:34','2017-11-15 17:28:01'),(271,0,14,1,1,1,'2017-11-14 22:10:34','2017-11-15 17:28:01'),(272,0,21,1,1,1,'2017-11-14 22:10:34','2017-11-15 17:28:01'),(273,1,3,1,1,1,'2017-11-14 22:37:31','2017-11-15 17:28:01'),(274,0,5,1,1,1,'2017-11-14 22:37:31','2017-11-15 17:28:01'),(275,0,6,1,1,1,'2017-11-14 22:37:31','2017-11-15 17:28:01'),(276,0,7,1,1,1,'2017-11-14 22:37:31','2017-11-15 17:28:01'),(277,0,8,1,1,1,'2017-11-14 22:37:31','2017-11-15 17:28:01'),(278,1,19,1,1,1,'2017-11-14 22:37:31','2017-11-15 17:28:01'),(279,1,10,1,1,1,'2017-11-14 22:37:31','2017-11-15 17:28:01'),(280,0,11,1,1,1,'2017-11-14 22:37:31','2017-11-15 17:28:01'),(282,0,3,1,1,1,'2017-11-15 18:37:26',NULL),(283,0,5,1,1,1,'2017-11-15 18:37:26',NULL),(284,0,6,1,1,1,'2017-11-15 18:37:26',NULL),(285,0,7,1,1,1,'2017-11-15 18:37:26',NULL),(286,0,8,1,1,1,'2017-11-15 18:37:26',NULL),(287,1,19,1,1,1,'2017-11-15 18:37:26',NULL),(288,1,10,1,1,1,'2017-11-15 18:37:26',NULL),(289,0,11,1,1,1,'2017-11-15 18:37:26',NULL);
/*!40000 ALTER TABLE `meg_logs_activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_main_organizations`
--

DROP TABLE IF EXISTS `meg_main_organizations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_main_organizations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `organization_name` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `organization_title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `logo` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `background_color` char(16) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `main_organizations_organization_title_unique` (`organization_title`),
  KEY `main_organizations_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_main_organizations`
--

LOCK TABLES `meg_main_organizations` WRITE;
/*!40000 ALTER TABLE `meg_main_organizations` DISABLE KEYS */;
INSERT INTO `meg_main_organizations` VALUES (1,'Hermann Hilll','Hermann Hilll','','','2017-11-06 16:37:16','2017-11-06 16:37:16');
/*!40000 ALTER TABLE `meg_main_organizations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_messages`
--

DROP TABLE IF EXISTS `meg_messages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_messages` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `type` int(11) DEFAULT NULL,
  `title_message` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_messages`
--

LOCK TABLES `meg_messages` WRITE;
/*!40000 ALTER TABLE `meg_messages` DISABLE KEYS */;
INSERT INTO `meg_messages` VALUES (1,1,'El Catéter está insertado más de :x horas'),(2,1,'Vacíar la Bolsa urinaria en :x  horas'),(3,1,'La Bolsa urinaria está llena más de :x horas');
/*!40000 ALTER TABLE `meg_messages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_migrations`
--

DROP TABLE IF EXISTS `meg_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_migrations` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_migrations`
--

LOCK TABLES `meg_migrations` WRITE;
/*!40000 ALTER TABLE `meg_migrations` DISABLE KEYS */;
INSERT INTO `meg_migrations` VALUES (1,'2017_10_30_165049_create_main_migrations_app',1);
/*!40000 ALTER TABLE `meg_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_patients`
--

DROP TABLE IF EXISTS `meg_patients`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_patients` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `patient_identity` char(10) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_firstname` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_age` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `patient_gender` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `last_interaction` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `patients_patient_identity_unique` (`patient_identity`),
  KEY `patients_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=26 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_patients`
--

LOCK TABLES `meg_patients` WRITE;
/*!40000 ALTER TABLE `meg_patients` DISABLE KEYS */;
INSERT INTO `meg_patients` VALUES (1,'Missj-4854','Howell Ortiz','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(2,'Naomi-8578','Sylvan Feil','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(3,'Elyse-9248','Frederik Waters','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(4,'Royal-2877','Cooper Deckow','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(5,'Patri-1173','Ms. Diana Halvorson','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(6,'Treve-7465','Amaya Harvey','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(7,'Caraj-3442','Karen Bogan','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(8,'Prof.-5149','Joaquin Marquardt','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(9,'Margo-4643','Denis Beahan','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(10,'Prof.-3639','Kenneth West','23','M','2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17'),(11,'Berna-3128','Aurore Hirthe IV','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(12,'Chels-3482','Mr. Henderson Toy PhD','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(13,'Conne-7737','Boyd Hamill','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(14,'Manle-5054','Prof. Margret Schmidt','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(15,'Lucie-3472','Lexus Spinka','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(16,'Missj-6419','Rebekah O\'Hara','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(17,'Prof.-1225','Rosendo Will','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(18,'Tyree-2073','Ms. Marielle Gutmann','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(19,'Jazmy-1744','Dr. Timothy Murazik Jr.','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(20,'Modes-2486','Jovan O\'Conner','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(21,'Orion-4963','Elza Davis PhD','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(22,'Dr.jN-5755','Demarcus Cronin','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(23,'EvajE-1180','Prof. Kaitlyn Bartoletti','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(24,'Saige-4813','Dion Wisozk','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18'),(25,'Missj-4132','Abbey Harber','23','M','2017-11-06 00:00:00','2017-11-06 16:37:18','2017-11-06 16:37:18');
/*!40000 ALTER TABLE `meg_patients` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_procedures`
--

DROP TABLE IF EXISTS `meg_procedures`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_procedures` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `title` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_enabled` tinyint(4) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`),
  KEY `procedures_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_procedures`
--

LOCK TABLES `meg_procedures` WRITE;
/*!40000 ALTER TABLE `meg_procedures` DISABLE KEYS */;
INSERT INTO `meg_procedures` VALUES (1,'Indicaciones',1,'2017-11-06 16:37:18','2017-11-06 16:37:18'),(2,'Inserción',1,'2017-11-06 16:37:18','2017-11-06 16:37:18'),(3,'Mantenimiento',1,'2017-11-06 16:37:19','2017-11-06 16:37:19');
/*!40000 ALTER TABLE `meg_procedures` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_roles`
--

DROP TABLE IF EXISTS `meg_roles`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_roles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `rol_title` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `rol_level` int(11) NOT NULL,
  `organization` int(10) unsigned NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `roles_rol_title_unique` (`rol_title`),
  KEY `roles_id_index` (`id`),
  KEY `roles_organization_foreign` (`organization`),
  CONSTRAINT `roles_organization_foreign` FOREIGN KEY (`organization`) REFERENCES `meg_main_organizations` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_roles`
--

LOCK TABLES `meg_roles` WRITE;
/*!40000 ALTER TABLE `meg_roles` DISABLE KEYS */;
INSERT INTO `meg_roles` VALUES (1,'enfermeras',2,1,'2017-11-06 16:37:16','2017-11-06 16:37:16');
/*!40000 ALTER TABLE `meg_roles` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_rooms`
--

DROP TABLE IF EXISTS `meg_rooms`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_rooms` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `room_identity` varchar(25) COLLATE utf8mb4_unicode_ci NOT NULL,
  `room_name` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `rooms_room_identity_unique` (`room_identity`),
  KEY `rooms_id_index` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_rooms`
--

LOCK TABLES `meg_rooms` WRITE;
/*!40000 ALTER TABLE `meg_rooms` DISABLE KEYS */;
INSERT INTO `meg_rooms` VALUES (1,'2668','Operación','2017-11-06 16:37:20','2017-11-06 16:37:20'),(2,'8976','UCI','2017-11-06 16:37:20','2017-11-06 16:37:20'),(3,'5781','UTI','2017-11-06 16:37:20','2017-11-06 16:37:20');
/*!40000 ALTER TABLE `meg_rooms` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `meg_users`
--

DROP TABLE IF EXISTS `meg_users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meg_users` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_firstname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_lastname` varchar(50) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_password` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_picture` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_email` varchar(150) COLLATE utf8mb4_unicode_ci NOT NULL,
  `user_type` int(10) unsigned NOT NULL,
  `main_organization` int(10) unsigned NOT NULL,
  `last_login` datetime NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `users_user_email_unique` (`user_email`),
  KEY `users_id_index` (`id`),
  KEY `users_user_type_foreign` (`user_type`),
  KEY `users_main_organization_foreign` (`main_organization`),
  CONSTRAINT `users_main_organization_foreign` FOREIGN KEY (`main_organization`) REFERENCES `meg_main_organizations` (`id`),
  CONSTRAINT `users_user_type_foreign` FOREIGN KEY (`user_type`) REFERENCES `meg_roles` (`id`) ON DELETE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meg_users`
--

LOCK TABLES `meg_users` WRITE;
/*!40000 ALTER TABLE `meg_users` DISABLE KEYS */;
INSERT INTO `meg_users` VALUES (1,'Rhea Bernier','Selena Jenkins Sr.','$2y$10$BLqz9oru8k8oQZUZpbHpcepcKeICplANSJVfpmTddh50pWTBWJrrO','','norberto44@example.com',1,1,'2017-11-06 00:00:00','2017-11-06 16:37:17','2017-11-06 16:37:17');
/*!40000 ALTER TABLE `meg_users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'co_morbidity'
--

--
-- Dumping routines for database 'co_morbidity'
--
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2017-11-15 13:48:56
