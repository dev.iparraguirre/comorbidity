DROP PROCEDURE IF EXISTS Refactor_meg_users;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_users()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='firstname' AND TABLE_NAME='meg_users') ) THEN

    ALTER TABLE `meg_users`
    DROP FOREIGN KEY `users_main_organization_foreign`,
    DROP FOREIGN KEY `users_user_type_foreign`;
    ALTER TABLE `meg_users`
    CHANGE COLUMN `user_firstname` `firstname` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `user_lastname` `lastname` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `user_password` `password` VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `user_picture` `picture` VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `user_email` `email` VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `user_type` `role_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `main_organization` `organization_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `last_login` `last_login` TIMESTAMP NOT NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    ADD COLUMN `mobile_phone` VARCHAR(12) NULL AFTER `email`,
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `organization_id`,
    ADD UNIQUE INDEX `mobile_phone_UNIQUE` (`mobile_phone` ASC);
    ALTER TABLE `meg_users`
    ADD CONSTRAINT `users_organization_id_FK`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_main_organizations` (`id`)
      ON UPDATE CASCADE
      ON DELETE CASCADE,
    ADD CONSTRAINT `users_role_id_FK`
      FOREIGN KEY (`role_id`)
      REFERENCES `meg_roles` (`id`)
      ON UPDATE CASCADE
      ON DELETE CASCADE;

    ALTER TABLE `meg_users`
    DROP INDEX `users_id_index` ;

END IF;
END //
DELIMITER ;

CALL Refactor_meg_users();

DROP PROCEDURE IF EXISTS Refactor_meg_users;