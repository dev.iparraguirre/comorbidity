DROP PROCEDURE IF EXISTS Refactor_meg_logs_activities;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_logs_activities()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='activity_procedures_id' AND TABLE_NAME='meg_logs_activities') ) THEN

    ALTER TABLE `meg_logs_activities`
    DROP FOREIGN KEY `logs_activities_main_activity_foreign`,
    DROP FOREIGN KEY `logs_activities_main_patient_foreign`,
    DROP FOREIGN KEY `logs_activities_main_room_foreign`,
    DROP FOREIGN KEY `logs_activities_main_user_foreign`;
    ALTER TABLE `meg_logs_activities`
    CHANGE COLUMN `type` `generate_alert` TINYINT NOT NULL ,
    CHANGE COLUMN `main_activity` `activity_procedures_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `main_patient` `patient_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `main_user` `user_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `main_room` `room_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NOT NULL ;
    ALTER TABLE `meg_logs_activities`
    ADD CONSTRAINT `logs_activities_activity_procedures_id_FK`
      FOREIGN KEY (`activity_procedures_id`)
      REFERENCES `meg_activity_procedures` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
    ADD CONSTRAINT `logs_activities_patient_id_FK`
      FOREIGN KEY (`patient_id`)
      REFERENCES `meg_patients` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
    ADD CONSTRAINT `logs_activities_room_id_FK`
      FOREIGN KEY (`room_id`)
      REFERENCES `meg_rooms` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE,
    ADD CONSTRAINT `logs_activities_user_id_FK`
      FOREIGN KEY (`user_id`)
      REFERENCES `meg_users` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;
END IF;
END //
DELIMITER ;

CALL Refactor_meg_logs_activities();

DROP PROCEDURE IF EXISTS Refactor_meg_logs_activities;

-- -----------------------------------

    ALTER TABLE `meg_activity_procedures`
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ;

    ALTER TABLE `meg_alerts_activities`
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ;

    ALTER TABLE `meg_config_platform`
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ;

    ALTER TABLE `meg_devices`
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ;

    ALTER TABLE `meg_log_access`
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ;

    ALTER TABLE `meg_logs_activities`
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ;
