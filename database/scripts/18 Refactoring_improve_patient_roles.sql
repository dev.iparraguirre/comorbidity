DROP PROCEDURE IF EXISTS Refactor_patient_roles;
DELIMITER //
 CREATE PROCEDURE Refactor_patient_roles()
   BEGIN
-- add a column safely
IF EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='parent_role_id' AND TABLE_NAME='meg_roles') ) THEN

    ALTER TABLE `meg_roles`
    ADD CONSTRAINT `fk_meg_roles_parent_role_id1`
      FOREIGN KEY (`parent_role_id`)
      REFERENCES `meg_roles` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

END IF;
END //
DELIMITER ;

CALL Refactor_patient_roles();

DROP PROCEDURE IF EXISTS Refactor_patient_roles;

-- ---------------------------------------

CREATE TABLE IF NOT EXISTS `meg_patient_admission process` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `patient_id` INT(10) UNSIGNED NOT NULL,
  `status_id` INT(10) UNSIGNED NOT NULL,
  `reason_process` VARCHAR(100) COLLATE utf8mb4_unicode_ci NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  INDEX `fk_meg_patient_admission process_meg_patients1_idx` (`patient_id` ASC),
  CONSTRAINT `fk_meg_patient_admission process_meg_patients1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `meg_patients` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
    ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
