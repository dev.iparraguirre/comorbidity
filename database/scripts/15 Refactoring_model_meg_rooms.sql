DROP PROCEDURE IF EXISTS Refactor_meg_rooms;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_rooms()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='identity' AND TABLE_NAME='meg_rooms') ) THEN

    ALTER TABLE `meg_rooms`
    CHANGE COLUMN `room_identity` `identity` VARCHAR(25) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `room_name` `name` VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ,
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `name`;

END IF;
END //
DELIMITER ;

CALL Refactor_meg_rooms();

DROP PROCEDURE IF EXISTS Refactor_meg_rooms;