DROP PROCEDURE IF EXISTS AddColumnProcedure_Activity_sequence;
DELIMITER //
 CREATE PROCEDURE AddColumnPuntoServicio_esActivo()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='procedure_sequence' AND TABLE_NAME='meg_procedures') ) THEN
  ALTER TABLE `meg_procedures`
  ADD COLUMN `procedure_sequence` INT(10) UNSIGNED NOT NULL AFTER `procedure_title`;

  ALTER TABLE `meg_activity_procedures`
  ADD COLUMN `activity_sequence` INT(10) UNSIGNED NOT NULL AFTER `activity_type`;


END IF;
END //
DELIMITER ;

CALL AddColumnProcedure_Activity_sequence();

DROP PROCEDURE IF EXISTS AddColumnProcedure_Activity_sequence;

-- ---------------------------------------
-- Procedures
-- ---------------------------------------
UPDATE `meg_procedures` SET `procedure_sequence`='1' WHERE `id`='1';
UPDATE `meg_procedures` SET `procedure_sequence`='2' WHERE `id`='2';
UPDATE `meg_procedures` SET `procedure_sequence`='3' WHERE `id`='3';


-- ---------------------------
-- Activities Indicaciones
-- ---------------------------
UPDATE `meg_activity_procedures` SET `activity_sequence`='1' WHERE `id`='1';
UPDATE `meg_activity_procedures` SET `activity_sequence`='2' WHERE `id`='4';
UPDATE `meg_activity_procedures` SET `activity_sequence`='3' WHERE `id`='9';
UPDATE `meg_activity_procedures` SET `activity_sequence`='4' WHERE `id`='13';
UPDATE `meg_activity_procedures` SET `activity_sequence`='5' WHERE `id`='14';
UPDATE `meg_activity_procedures` SET `activity_sequence`='6' WHERE `id`='21';


-- ---------------------------
-- Activities Insercion
-- ---------------------------
UPDATE `meg_activity_procedures` SET `activity_sequence`='1' WHERE `id`='18';
UPDATE `meg_activity_procedures` SET `activity_sequence`='2' WHERE `id`='15';
UPDATE `meg_activity_procedures` SET `activity_sequence`='3' WHERE `id`='16';
UPDATE `meg_activity_procedures` SET `activity_sequence`='4' WHERE `id`='22';
UPDATE `meg_activity_procedures` SET `activity_sequence`='5' WHERE `id`='23';
UPDATE `meg_activity_procedures` SET `activity_sequence`='6' WHERE `id`='2';
UPDATE `meg_activity_procedures` SET `activity_sequence`='7' WHERE `id`='24';
UPDATE `meg_activity_procedures` SET `activity_sequence`='8' WHERE `id`='25';
UPDATE `meg_activity_procedures` SET `activity_sequence`='9' WHERE `id`='26';

-- ---------------------------
-- Activities Mantenimiento
-- ---------------------------

UPDATE `meg_activity_procedures` SET `activity_sequence`='1' WHERE `id`='3';
UPDATE `meg_activity_procedures` SET `activity_sequence`='2' WHERE `id`='5';
UPDATE `meg_activity_procedures` SET `activity_sequence`='3' WHERE `id`='6';
UPDATE `meg_activity_procedures` SET `activity_sequence`='4' WHERE `id`='7';
UPDATE `meg_activity_procedures` SET `activity_sequence`='5' WHERE `id`='8';
UPDATE `meg_activity_procedures` SET `activity_sequence`='6' WHERE `id`='19';
UPDATE `meg_activity_procedures` SET `activity_sequence`='7' WHERE `id`='10';
UPDATE `meg_activity_procedures` SET `activity_sequence`='8' WHERE `id`='11';

