DROP PROCEDURE IF EXISTS Refactor_meg_activity_procedures;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_activity_procedures()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='title' AND TABLE_NAME='meg_activity_procedures') ) THEN

    ALTER TABLE `meg_activity_procedures`
    DROP FOREIGN KEY `activity_procedures_activity_id_foreign`;
    ALTER TABLE `meg_activity_procedures`
    CHANGE COLUMN `activity_title` `title` VARCHAR(200) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `activity_recordatory` `notification_type_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `activity_id` `meg_procedures_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `activity_level` `alert_type_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `activity_type` `has_alert` TINYINT NOT NULL ,
    CHANGE COLUMN `activity_sequence` `sequence` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `activity_message` `meg_messages_id` INT(10) UNSIGNED NULL DEFAULT NULL ,
    CHANGE COLUMN `main_master` `parent_activity_id` INT(10) NULL ;
    ALTER TABLE `meg_activity_procedures`
    ADD CONSTRAINT `activity_procedures_activity_id_foreign`
      FOREIGN KEY (`meg_procedures_id`)
      REFERENCES `meg_procedures` (`id`);

    ALTER TABLE `meg_activity_procedures`
    CHANGE COLUMN `has_alert` `has_alert` TINYINT(4) NOT NULL DEFAULT 0 ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NOT NULL ;

    ALTER TABLE `meg_activity_procedures`
    DROP COLUMN `master`;

    ALTER TABLE `meg_activity_procedures`
    ADD COLUMN `is_title` TINYINT NOT NULL DEFAULT 0 AFTER `meg_messages_id`;

    UPDATE `meg_activity_procedures` SET `parent_activity_id`=NULL WHERE `parent_activity_id`='0';
    UPDATE `meg_activity_procedures` SET `is_title`='1' WHERE meg_procedures_id = 2 and parent_activity_id IS NULL;

    ALTER TABLE `meg_activity_procedures`
    CHANGE COLUMN `parent_activity_id` `parent_activity_id` INT(10) UNSIGNED NULL DEFAULT NULL ;

    ALTER TABLE `meg_activity_procedures`
    ADD INDEX `activity_procedures_parent_activity_id_idx` (`parent_activity_id` ASC);
    ALTER TABLE `meg_activity_procedures`
    ADD CONSTRAINT `activity_procedures_parent_activity_id_foreign`
      FOREIGN KEY (`parent_activity_id`)
      REFERENCES `meg_activity_procedures` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_procedures`
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `parent_activity_id`;

END IF;
END //
DELIMITER ;

CALL Refactor_meg_activity_procedures();

DROP PROCEDURE IF EXISTS Refactor_meg_activity_procedures;