DROP PROCEDURE IF EXISTS Refactor_meg_patients;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_patients()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='identity' AND TABLE_NAME='meg_patients') ) THEN

    ALTER TABLE `meg_patients`
    DROP COLUMN `last_interaction`,
    CHANGE COLUMN `patient_identity` `identity` CHAR(10) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `patient_firstname` `firstname` VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `patient_age` `age` VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `patient_gender` `gender` CHAR(1) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ,
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `gender`;

    ALTER TABLE `meg_patients`
    DROP INDEX `patients_id_index` ;

END IF;
END //
DELIMITER ;

CALL Refactor_meg_patients();

DROP PROCEDURE IF EXISTS Refactor_meg_patients;