-- MySQL Workbench Synchronization
-- Generated: 2017-11-24 15:54
-- Model: New Model
-- Version: 1.0
-- Project: Name of the project
-- Author: Deusdit

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

CREATE TABLE IF NOT EXISTS `meg_patient_lab_results` (
  `id` INT(10) UNSIGNED NOT NULL,
  `patient_id` INT(10) UNSIGNED NOT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_meg_patient_lab_results_meg_patients1_idx` (`patient_id` ASC),
  CONSTRAINT `fk_meg_patient_lab_results_meg_patients1`
    FOREIGN KEY (`patient_id`)
    REFERENCES `meg_patients` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

CREATE TABLE IF NOT EXISTS `meg_patient_lab_result_details` (
  `id` INT(10) UNSIGNED NOT NULL,
  `patient_lab_result_id` INT(10) UNSIGNED NOT NULL,
  `analysis_name` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `analysis_value` VARCHAR(25) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `analysis_unit_name` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `analysis_range` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL,
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`),
  INDEX `fk_meg_patient_lab_result_details_meg_patient_lab_results1_idx` (`patient_lab_result_id` ASC),
  CONSTRAINT `fk_meg_patient_lab_result_details_meg_patient_lab_results1`
    FOREIGN KEY (`patient_lab_result_id`)
    REFERENCES `meg_patient_lab_results` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB
DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;


SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
