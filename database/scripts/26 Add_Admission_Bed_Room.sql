DROP PROCEDURE IF EXISTS Add_Admission_Bed_Room;
DELIMITER //
 CREATE PROCEDURE Add_Admission_Bed_Room()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='bed' AND TABLE_NAME='meg_patient_admission process') ) THEN

  ALTER TABLE `meg_patient_admission process`
  ADD COLUMN `bed` VARCHAR(45) NOT NULL AFTER `updated_at`,
  ADD COLUMN `room_id` INT(10) UNSIGNED NULL DEFAULT NULL AFTER `bed`,
  ADD INDEX `fk_meg_patient_admission process_meg_rooms1_idx` (`room_id` ASC);

  ALTER TABLE `meg_patient_admission process`
  ADD CONSTRAINT `fk_meg_patient_admission process_meg_rooms1`
    FOREIGN KEY (`room_id`)
    REFERENCES `meg_rooms` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE;

END IF;
END //
DELIMITER ;

CALL Add_Admission_Bed_Room();

DROP PROCEDURE IF EXISTS Add_Admission_Bed_Room;