UPDATE `meg_activity_procedures` SET `activity_title`='Antes de la Inserción', `activity_recordatory`='0', `created_at`='2017-11-06 16:37:20', `updated_at`='2017-11-06 16:37:20' WHERE `id`='28';
UPDATE `meg_activity_procedures` SET `activity_title`='Inserción', `activity_sequence`='4', `activity_recordatory`='0', `created_at`='2017-11-10 11:12:20', `updated_at`='2017-11-10 11:12:20' WHERE `id`='29';
UPDATE `meg_activity_procedures` SET `main_master`='29' WHERE `id`='26';

DELETE FROM `meg_alerts_activities` WHERE alert_main IN ('18', '22');
DELETE FROM `meg_logs_activities` WHERE main_activity IN ('18', '22');
DELETE FROM `meg_activity_procedures` WHERE `id` IN ('18', '22');