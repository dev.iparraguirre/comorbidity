DROP PROCEDURE IF EXISTS Refactor_meg_alerts_activities;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_alerts_activities()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='patients_id' AND TABLE_NAME='meg_alerts_activities') ) THEN

    ALTER TABLE `meg_alerts_activities`
    CHANGE COLUMN `alert_main_patient` `meg_patients_id` INT(11) UNSIGNED NOT NULL ,
    ADD INDEX `meg_patients_id_idx` (`meg_patients_id` ASC);
    ALTER TABLE `meg_alerts_activities`
    ADD CONSTRAINT `alerts_activities_meg_patients_id_foreign`
      FOREIGN KEY (`id`)
      REFERENCES `meg_patients` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;


    ALTER TABLE `meg_alerts_activities`
    DROP FOREIGN KEY `alerts_activities_alert_main_foreign`;
    ALTER TABLE `meg_alerts_activities`
    CHANGE COLUMN `alert_main` `activity_procedures_id` INT(10) UNSIGNED NOT NULL ;
    ALTER TABLE `meg_alerts_activities`
    ADD CONSTRAINT `alerts_activities_meg_activity_procedures_id_foreign`
      FOREIGN KEY (`activity_procedures_id`)
      REFERENCES `meg_activity_procedures` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_alerts_activities`
    CHANGE COLUMN `meg_patients_id` `patients_id` INT(11) UNSIGNED NOT NULL ;

    ALTER TABLE `meg_alerts_activities`
    CHANGE COLUMN `alert_type` `alert_type_id` INT(11) NOT NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NOT NULL ,
    CHANGE COLUMN `status` `status` INT(11) NOT NULL DEFAULT 0 ;

    ALTER TABLE `meg_alerts_activities`
    DROP FOREIGN KEY `alerts_activities_meg_patients_id_foreign`;
    ALTER TABLE `meg_alerts_activities`
    DROP PRIMARY KEY;
    ALTER TABLE `meg_alerts_activities`
    ADD CONSTRAINT `alerts_activities_meg_patients_id_foreign`
      FOREIGN KEY (`patients_id`)
      REFERENCES `meg_patients` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;



END IF;
END //
DELIMITER ;

CALL Refactor_meg_alerts_activities();

DROP PROCEDURE IF EXISTS Refactor_meg_alerts_activities;