DROP PROCEDURE IF EXISTS Add_Patient_InternalId;
DELIMITER //
 CREATE PROCEDURE Add_Patient_InternalId()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='internal_id' AND TABLE_NAME='meg_patients') ) THEN

    ALTER TABLE `meg_patients`
    ADD COLUMN `internal_id` VARCHAR(50) NOT NULL AFTER `organization_id`;

END IF;
END //
DELIMITER ;

CALL Add_Patient_InternalId();

DROP PROCEDURE IF EXISTS Add_Patient_InternalId;

-- -------------------------------------------------
