TRUNCATE `meg_logs_activities`;

TRUNCATE `meg_alerts_activities`;

-- ----------------------------------

DROP PROCEDURE IF EXISTS Add_Device_Log_Activities;
DELIMITER //
 CREATE PROCEDURE Add_Device_Log_Activities()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='device_id' AND TABLE_NAME='meg_logs_activities') ) THEN

    ALTER TABLE `meg_logs_activities`
    ADD COLUMN `device_id` INT(10) UNSIGNED NOT NULL AFTER `room_id`;

    ALTER TABLE `meg_logs_activities`
    ADD CONSTRAINT `logs_activities_devices_id_FK`
      FOREIGN KEY (`device_id`)
      REFERENCES `meg_devices` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

END IF;
END //
DELIMITER ;

CALL Add_Device_Log_Activities();

DROP PROCEDURE IF EXISTS Add_Device_Log_Activities;

-- -------------------------------------------------
