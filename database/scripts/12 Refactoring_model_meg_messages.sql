DROP PROCEDURE IF EXISTS Refactor_meg_messages;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_messages()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='type_id' AND TABLE_NAME='meg_messages') ) THEN

    ALTER TABLE `meg_messages`
    CHANGE COLUMN `type` `type_id` INT(11) NOT NULL ,
    CHANGE COLUMN `title_message` `title_message` VARCHAR(45) NOT NULL ,
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `title_message`,
    ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `is_enabled`,
    ADD COLUMN `updated_at` TIMESTAMP NULL AFTER `created_at`;

END IF;
END //
DELIMITER ;

CALL Refactor_meg_messages();

DROP PROCEDURE IF EXISTS Refactor_meg_messages;