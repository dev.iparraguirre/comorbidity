UPDATE `meg_procedures` SET `procedure_title`='Inserción' WHERE `id`='2';

UPDATE `meg_rooms` SET `room_name`='Operación' WHERE `id`='1';
UPDATE `meg_rooms` SET `room_name`='UCI' WHERE `id`='2';
UPDATE `meg_rooms` SET `room_name`='UTI' WHERE `id`='3';

UPDATE `meg_activity_procedures` SET `activity_title`='Retención urinaria aguda' WHERE `id`='1';
UPDATE `meg_activity_procedures` SET `activity_title`='Herida sacra abierta o perinal' WHERE `id`='4';
UPDATE `meg_activity_procedures` SET `activity_title`='Comodidad al final de la vida' WHERE `id`='9';
UPDATE `meg_activity_procedures` SET `activity_title`='Necesidad de una medición precisa' WHERE `id`='13';
UPDATE `meg_activity_procedures` SET `activity_title`='Inmovilización prolongada' WHERE `id`='14';

INSERT INTO `meg_activity_procedures`
(`activity_title`, `activity_recordatory`, `activity_id`, `activity_level`, `activity_type`,`created_at`,`updated_at`)
VALUES ('Indicación no conforme', '0', '1', '0', '0','2017-11-10 11:05:20', '2017-11-10 11:05:20');


UPDATE `meg_activity_procedures` SET `activity_title`='Inserte tubo a la longitud apropiada' WHERE `id`='2';
UPDATE `meg_activity_procedures` SET `activity_title`='IUC más pequeño seleccionado' WHERE `id`='15';
UPDATE `meg_activity_procedures` SET `activity_title`='Cuidado personal e higiene de manos' WHERE `id`='16';
UPDATE `meg_activity_procedures` SET `activity_title`='Antes de la Inserción' WHERE `id`='18';

INSERT INTO `meg_activity_procedures` (`activity_title`, `activity_recordatory`, `activity_id`, `activity_level`, `activity_type`, `created_at`, `updated_at`) VALUES ('Inserción', '0', '2', '0', '0', '2017-11-10 11:12:20', '2017-11-10 11:12:20');
INSERT INTO `meg_activity_procedures` (`activity_title`, `activity_recordatory`, `activity_id`, `activity_level`, `activity_type`, `created_at`, `updated_at`) VALUES ('Guantes y equipo estériles', '0', '2', '0', '0', '2017-11-10 11:12:20', '2017-11-10 11:12:20');
INSERT INTO `meg_activity_procedures` (`activity_title`, `activity_recordatory`, `activity_id`, `activity_level`, `activity_type`, `created_at`, `updated_at`) VALUES ('Inflar el globo según las instrucciones', '0', '2', '0', '0', '2017-11-10 11:12:20', '2017-11-10 11:12:20');
INSERT INTO `meg_activity_procedures` (`activity_title`, `activity_recordatory`, `activity_id`, `activity_level`, `activity_type`, `created_at`, `updated_at`) VALUES ('Bolsa de denaje a nivel de la cintura', '0', '2', '0', '0', '2017-11-10 11:12:20', '2017-11-10 11:12:20');
INSERT INTO `meg_activity_procedures` (`activity_title`, `activity_recordatory`, `activity_id`, `activity_level`, `activity_type`, `created_at`, `updated_at`) VALUES ('Conexión cerrada', '0', '2', '0', '0', '2017-11-10 11:12:20', '2017-11-10 11:12:20');

UPDATE `meg_activity_procedures` SET `activity_title`='Bolsa situada debajo de la vejiga' WHERE `id`='3';
UPDATE `meg_activity_procedures` SET `activity_title`='Flujo sin obstrucciones' WHERE `id`='5';
UPDATE `meg_activity_procedures` SET `activity_title`='Sistema de drenaje cerrado' WHERE `id`='6';
UPDATE `meg_activity_procedures` SET `activity_title`='Contención fecal apropiada' WHERE `id`='7';
UPDATE `meg_activity_procedures` SET `activity_title`='La familia entiende el mantenimiento' WHERE `id`='8';
UPDATE `meg_activity_procedures` SET `activity_title`='Vacíe la bolsa antes que se llene' WHERE `id`='19';
UPDATE `meg_activity_procedures` SET `activity_title`='Vacíe la bolsa cada ocho (8) horas' WHERE `id`='10';
UPDATE `meg_activity_procedures` SET `activity_title`='Tubo no toca la bolsa al vaciar' WHERE `id`='11';

-- -----------------------------------------------------------------------------

UPDATE `meg_messages` SET `title_message`='inserción.' WHERE `id`='1';
UPDATE `meg_messages` SET `title_message`='se insertó.' WHERE `id`='2';
