DROP PROCEDURE IF EXISTS Refactor_meg_procedures;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_procedures()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='title' AND TABLE_NAME='meg_procedures') ) THEN

    ALTER TABLE `meg_procedures`
    DROP COLUMN `procedure_sequence`,
    CHANGE COLUMN `procedure_title` `title` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NOT NULL ,
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `title`;

    ALTER TABLE `meg_procedures`
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NULL ;

END IF;
END //
DELIMITER ;

CALL Refactor_meg_procedures();

DROP PROCEDURE IF EXISTS Refactor_meg_procedures;