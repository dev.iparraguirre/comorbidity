DROP PROCEDURE IF EXISTS Add_tokens_url_timestamps;
DELIMITER //
 CREATE PROCEDURE Add_tokens_url_timestamps()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='created_at' AND TABLE_NAME='meg_tokens_url') ) THEN

  ALTER TABLE `meg_tokens_url`
  ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `user`,
  ADD COLUMN `updated_at` TIMESTAMP NULL AFTER `created_at`;

END IF;
END //
DELIMITER ;

CALL Add_tokens_url_timestamps();

DROP PROCEDURE IF EXISTS Add_tokens_url_timestamps;
