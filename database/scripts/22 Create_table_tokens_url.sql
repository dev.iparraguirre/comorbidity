DROP TABLE IF EXISTS `meg_tokens_url`;

CREATE TABLE `meg_tokens_url` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `token` varchar(250) NOT NULL,
  `ip` varchar(15) NOT NULL,
  `status` int(11) NOT NULL,
  `user` int(10) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  KEY `FK_USER_idx` (`user`),
  CONSTRAINT `FK_USER` FOREIGN KEY (`user`) REFERENCES `meg_users` (`id`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;