DROP PROCEDURE IF EXISTS Refactor_meg_activity_procedures;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_activity_procedures()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='procedures_id' AND TABLE_NAME='meg_activity_procedures') ) THEN

    ALTER TABLE `meg_messages`
    CHANGE COLUMN `id` `id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT ;

    ALTER TABLE `meg_activity_procedures`
    DROP FOREIGN KEY `activity_procedures_activity_id_foreign`;
    ALTER TABLE `meg_activity_procedures`
    CHANGE COLUMN `meg_procedures_id` `procedures_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `meg_messages_id` `messages_id` INT(10) UNSIGNED NULL DEFAULT NULL ;
    ALTER TABLE `meg_activity_procedures`
    ADD CONSTRAINT `activity_procedures_activity_id_foreign`
      FOREIGN KEY (`procedures_id`)
      REFERENCES `meg_procedures` (`id`);

    ALTER TABLE `meg_activity_procedures`
    ADD INDEX `activity_procedures_messages_id_foreign_idx` (`messages_id` ASC);
    ALTER TABLE `meg_activity_procedures`
    ADD CONSTRAINT `activity_procedures_messages_id_foreign`
      FOREIGN KEY (`messages_id`)
      REFERENCES `meg_messages` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_activity_procedures`
    DROP FOREIGN KEY `activity_procedures_activity_id_foreign`;
    ALTER TABLE `meg_activity_procedures`
    ADD CONSTRAINT `activity_procedures_activity_id_foreign`
      FOREIGN KEY (`procedures_id`)
      REFERENCES `meg_procedures` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;


END IF;
END //
DELIMITER ;

CALL Refactor_meg_activity_procedures();

DROP PROCEDURE IF EXISTS Refactor_meg_activity_procedures;