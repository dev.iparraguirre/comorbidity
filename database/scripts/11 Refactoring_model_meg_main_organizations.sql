DROP PROCEDURE IF EXISTS Refactor_meg_main_organizations;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_main_organizations()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='name' AND TABLE_NAME='meg_main_organizations') ) THEN

    ALTER TABLE `meg_main_organizations`
    CHANGE COLUMN `organization_name` `name` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `organization_title` `title` VARCHAR(100) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `logo` `logo` VARCHAR(150) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL ,
    CHANGE COLUMN `background_color` `bgcolor` CHAR(16) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ;

    ALTER TABLE `meg_main_organizations`
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `updated_at`;

    ALTER TABLE `meg_main_organizations`
    RENAME TO  `meg_organizations` ;


END IF;
END //
DELIMITER ;

CALL Refactor_meg_main_organizations();

DROP PROCEDURE IF EXISTS Refactor_meg_main_organizations;