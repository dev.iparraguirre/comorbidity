DROP PROCEDURE IF EXISTS Refactor_meg_roles;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_roles()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='title' AND TABLE_NAME='meg_roles') ) THEN

    ALTER TABLE `meg_roles`
    DROP FOREIGN KEY `roles_organization_foreign`;
    ALTER TABLE `meg_roles`
    CHANGE COLUMN `rol_title` `title` VARCHAR(50) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NOT NULL ,
    CHANGE COLUMN `rol_level` `level` INT(11) NOT NULL ,
    CHANGE COLUMN `organization` `organization_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `organization_id`,
    ADD COLUMN `parent_role_id` INT(10) UNSIGNED NULL AFTER `is_enabled`;
    ALTER TABLE `meg_roles`
    ADD CONSTRAINT `roles_organization_foreign`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_main_organizations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;


END IF;
END //
DELIMITER ;

CALL Refactor_meg_roles();

DROP PROCEDURE IF EXISTS Refactor_meg_roles;