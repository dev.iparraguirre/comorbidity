DROP PROCEDURE IF EXISTS Refactor_meg_config_platform_meg_log_access;
DELIMITER //
 CREATE PROCEDURE Refactor_meg_config_platform_meg_log_access()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='bgcolor' AND TABLE_NAME='meg_config_platform') ) THEN

    ALTER TABLE `meg_config_platform`
    CHANGE COLUMN `color_platform` `bgcolor` VARCHAR(45) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL ,
    CHANGE COLUMN `mail_extension` `mail_domain` VARCHAR(45) CHARACTER SET 'utf8mb4' COLLATE 'utf8mb4_unicode_ci' NULL DEFAULT NULL ,
    ADD COLUMN `is_enabled` TINYINT NOT NULL DEFAULT 1 AFTER `mail_domain`;

    ALTER TABLE `meg_config_platform`
    ADD COLUMN `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP AFTER `is_enabled`,
    ADD COLUMN `updated_at` TIMESTAMP NOT NULL AFTER `created_at`;

    ALTER TABLE `meg_log_access`
    CHANGE COLUMN `type` `acces_type_id` INT(11) NOT NULL ,
    CHANGE COLUMN `main_user` `user_id` INT(10) UNSIGNED NOT NULL ,
    CHANGE COLUMN `created_at` `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    CHANGE COLUMN `updated_at` `updated_at` TIMESTAMP NOT NULL ;

    ALTER TABLE `meg_log_access`
    ADD INDEX `meg_log_access_user_id_FK_idx` (`user_id` ASC);
    ALTER TABLE `meg_log_access`
    ADD CONSTRAINT `meg_log_access_user_id_FK`
      FOREIGN KEY (`user_id`)
      REFERENCES `meg_users` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;
END IF;
END //
DELIMITER ;

CALL Refactor_meg_config_platform_meg_log_access();

DROP PROCEDURE IF EXISTS Refactor_meg_config_platform_meg_log_access;