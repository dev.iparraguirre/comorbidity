DROP PROCEDURE IF EXISTS Add_Type_Activities;
DELIMITER //
 CREATE PROCEDURE Add_Type_Activities()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='type_id' AND TABLE_NAME='meg_activity_procedures') ) THEN

    ALTER TABLE `meg_activity_procedures`
    CHANGE COLUMN `is_title` `type_id` INT(10) UNSIGNED NOT NULL DEFAULT '2' ;

    -- Observation
    UPDATE `meg_activity_procedures` SET `type_id`='2' WHERE `type_id`='0';

END IF;
END //
DELIMITER ;

CALL Add_Type_Activities();

DROP PROCEDURE IF EXISTS Add_Type_Activities;

-- ---------------------------------------

-- Action
-- Inserción
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='23';
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='2';
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='24';
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='25';
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='26';

-- Mantenimiento
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='8';
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='19';
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='10';
UPDATE `meg_activity_procedures` SET `type_id`='3' WHERE `id`='11';
