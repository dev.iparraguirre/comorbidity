CREATE TABLE IF NOT EXISTS `meg_devices` (
  `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(100) COLLATE 'utf8mb4_unicode_ci' NOT NULL,
  `sequence` INT(10) UNSIGNED NOT NULL,
  `is_enabled` TINYINT NOT NULL DEFAULT '1',
  `created_at` TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` TIMESTAMP NOT NULL,
  PRIMARY KEY (`id`),
  INDEX `procedures_id_index` (`id` ASC))
ENGINE = InnoDB
AUTO_INCREMENT = 1;

-- ---------------------------------

INSERT IGNORE INTO `meg_devices` (`id`, `name`, `sequence`) VALUES ('1', 'Sonda Foley', '1');
INSERT IGNORE INTO `meg_devices` (`id`, `name`, `sequence`) VALUES ('2', 'Sonda Nelaton', '2');

-- -----------------------------------

CREATE TABLE IF NOT EXISTS `meg_devices_procedures` (
  `meg_devices_id` INT(10) UNSIGNED NOT NULL,
  `meg_procedures_id` INT(10) UNSIGNED NOT NULL,
  `sequence` INT UNSIGNED NOT NULL,
  PRIMARY KEY (`meg_devices_id`, `meg_procedures_id`),
  INDEX `fk_meg_devices_procedures_meg_devices1_idx` (`meg_devices_id` ASC),
  CONSTRAINT `fk_meg_devices_procedures_meg_procedures1`
    FOREIGN KEY (`meg_procedures_id`)
    REFERENCES `meg_procedures` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE,
  CONSTRAINT `fk_meg_devices_procedures_meg_devices1`
    FOREIGN KEY (`meg_devices_id`)
    REFERENCES `meg_devices` (`id`)
    ON DELETE CASCADE
    ON UPDATE CASCADE)
ENGINE = InnoDB

-- -----------------------------------

INSERT IGNORE INTO `meg_devices_procedures` (`meg_devices_id`, `meg_procedures_id`, `sequence`) VALUES ('1', '1', '1');
INSERT IGNORE INTO `meg_devices_procedures` (`meg_devices_id`, `meg_procedures_id`, `sequence`) VALUES ('1', '2', '2');
INSERT IGNORE INTO `meg_devices_procedures` (`meg_devices_id`, `meg_procedures_id`, `sequence`) VALUES ('1', '3', '3');
INSERT IGNORE INTO `meg_devices_procedures` (`meg_devices_id`, `meg_procedures_id`, `sequence`) VALUES ('2', '1', '1');
INSERT IGNORE INTO `meg_devices_procedures` (`meg_devices_id`, `meg_procedures_id`, `sequence`) VALUES ('2', '2', '2');
INSERT IGNORE INTO `meg_devices_procedures` (`meg_devices_id`, `meg_procedures_id`, `sequence`) VALUES ('2', '3', '3');

