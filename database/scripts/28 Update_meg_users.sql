DROP PROCEDURE IF EXISTS UpdateMegTable;
DELIMITER //
 CREATE PROCEDURE UpdateMegTable()
   BEGIN
  IF NOT EXISTS( (SELECT * FROM meg_users WHERE email='admin@admin.com') ) THEN

  	ALTER TABLE `meg_users`
	  CHANGE COLUMN `firstname` `firstname` BLOB NOT NULL ,
    CHANGE COLUMN `lastname` `lastname` BLOB NOT NULL ;

  	INSERT INTO meg_users (firstname,lastname,password,picture,organization_id,email,role_id)
  	VALUES ( aes_encrypt('admin', 'z7rBYmXRRTkwXdvZ6o0R'),aes_encrypt('admin', 'z7rBYmXRRTkwXdvZ6o0R'),'$2y$10$I/5IaMB13MfLQJlgAgiVpOZmD9pQ07oUlfCfbLnjNhBsRk6vT6v6S','',1,'admin@admin.com',2);

  END IF;
END //
DELIMITER ;

CALL UpdateMegTable();

DROP PROCEDURE IF EXISTS UpdateMegTable;