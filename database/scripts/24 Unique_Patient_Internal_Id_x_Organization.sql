UPDATE `meg_patients`
SET internal_id = identity
WHERE internal_id IS NULL OR internal_id = '';

-- -------------------------------------------------------

DROP PROCEDURE IF EXISTS Add_Patient_InternalId_Unique_Index;
DELIMITER //
 CREATE PROCEDURE Add_Patient_InternalId_Unique_Index()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM INFORMATION_SCHEMA.STATISTICS
           WHERE table_schema=DATABASE() AND table_name='meg_patients' AND index_name='internal_id_organization_UNQ') ) THEN

    ALTER TABLE `meg_patients`
    ADD UNIQUE INDEX `internal_id_organization_UNQ` (`organization_id` ASC, `internal_id` ASC);

END IF;
END //
DELIMITER ;
CALL Add_Patient_InternalId_Unique_Index();

DROP PROCEDURE IF EXISTS Add_Patient_InternalId_Unique_Index;

-- -------------------------------------------------
