DROP PROCEDURE IF EXISTS Refactor_organization;
DELIMITER //
 CREATE PROCEDURE Refactor_organization()
   BEGIN
-- add a column safely
IF NOT EXISTS( (SELECT * FROM information_schema.COLUMNS WHERE TABLE_SCHEMA=DATABASE()
        AND COLUMN_NAME='organization_id' AND TABLE_NAME='meg_activity_procedures') ) THEN

    SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
    SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
    SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

    ALTER TABLE `meg_activity_procedures`
    DROP COLUMN `organization_id`,
    ADD COLUMN `organization_id` INT(10) UNSIGNED NOT NULL AFTER `is_enabled`,
    ADD INDEX `fk_meg_activity_procedures_meg_organizations1_idx` (`organization_id` ASC),
    DROP INDEX `fk_meg_activity_procedures_meg_organizations1_idx` ;

    ALTER TABLE `meg_config_platform`
    DROP COLUMN `organization_id`,
    ADD COLUMN `organization_id` INT(10) UNSIGNED NOT NULL AFTER `updated_at`,
    ADD INDEX `fk_meg_config_platform_meg_organizations1_idx` (`organization_id` ASC),
    DROP INDEX `fk_meg_config_platform_meg_organizations1_idx` ;

    ALTER TABLE `meg_devices`
    DROP COLUMN `organization_id`,
    ADD COLUMN `organization_id` INT(10) UNSIGNED NOT NULL AFTER `updated_at`,
    ADD INDEX `fk_meg_devices_meg_organizations1_idx` (`organization_id` ASC),
    DROP INDEX `fk_meg_devices_meg_organizations1_idx` ;

    ALTER TABLE `meg_messages`
    DROP COLUMN `organization_id`,
    ADD COLUMN `organization_id` INT(10) UNSIGNED NOT NULL AFTER `updated_at`,
    ADD INDEX `fk_meg_messages_meg_organizations1_idx` (`organization_id` ASC),
    DROP INDEX `fk_meg_messages_meg_organizations1_idx` ;

    ALTER TABLE `meg_patients`
    DROP COLUMN `organization_id`,
    ADD COLUMN `organization_id` INT(10) UNSIGNED NOT NULL AFTER `updated_at`,
    ADD INDEX `fk_meg_patients_meg_organizations1_idx` (`organization_id` ASC),
    DROP INDEX `fk_meg_patients_meg_organizations1_idx` ;

    ALTER TABLE `meg_procedures`
    DROP COLUMN `organization_id`,
    ADD COLUMN `organization_id` INT(10) UNSIGNED NOT NULL AFTER `updated_at`,
    ADD INDEX `fk_meg_procedures_meg_organizations1_idx` (`organization_id` ASC),
    DROP INDEX `fk_meg_procedures_meg_organizations1_idx` ;

    ALTER TABLE `meg_rooms`
    DROP COLUMN `organization_id`,
    ADD COLUMN `organization_id` INT(10) UNSIGNED NOT NULL AFTER `updated_at`,
    ADD INDEX `fk_meg_rooms_meg_organizations1_idx` (`organization_id` ASC),
    DROP INDEX `fk_meg_rooms_meg_organizations1_idx` ;

    ALTER TABLE `meg_activity_procedures`
    ADD CONSTRAINT `fk_meg_activity_procedures_meg_organizations1`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_organizations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_config_platform`
    ADD CONSTRAINT `fk_meg_config_platform_meg_organizations1`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_organizations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_devices`
    ADD CONSTRAINT `fk_meg_devices_meg_organizations1`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_organizations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_messages`
    ADD CONSTRAINT `fk_meg_messages_meg_organizations1`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_organizations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_patients`
    ADD CONSTRAINT `fk_meg_patients_meg_organizations1`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_organizations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_procedures`
    ADD CONSTRAINT `fk_meg_procedures_meg_organizations1`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_organizations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;

    ALTER TABLE `meg_rooms`
    ADD CONSTRAINT `fk_meg_rooms_meg_organizations1`
      FOREIGN KEY (`organization_id`)
      REFERENCES `meg_organizations` (`id`)
      ON DELETE CASCADE
      ON UPDATE CASCADE;


    SET SQL_MODE=@OLD_SQL_MODE;
    SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
    SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;


END IF;
END //
DELIMITER ;

CALL Refactor_organization();

DROP PROCEDURE IF EXISTS Refactor_organization;

-- ---------------------------------------------

UPDATE `meg_activity_procedures` SET `organization_id`='1';
UPDATE `meg_config_platform` SET `organization_id`='1';
UPDATE `meg_devices` SET `organization_id`='1';
UPDATE `meg_messages` SET `organization_id`='1';
UPDATE `meg_patients` SET `organization_id`='1';
UPDATE `meg_procedures` SET `organization_id`='1';
UPDATE `meg_rooms` SET `organization_id`='1';