<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateMainMigrationsApp extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if (env('APP_MIGRATIONS', 0)) {
            if (Schema::hasTable('main_organizations')) {
                Schema::dropAllTables();
            }
        }

        Schema::enableForeignKeyConstraints();

        Schema::defaultStringLength(150);

        Schema::create('main_organizations', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('organization_name', 100);
            $table->string('organization_title', 100);
            $table->string('logo');
            $table->char('background_color', 16);
            $table->timestamps();

            $table->unique(['organization_title']);
        });

        Schema::create('config_platform', function (Blueprint $table) {
            $table->string('alerts_dashboard', 100);
        });        

        Schema::create('roles', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('rol_title', 50);
            $table->integer('rol_level');
            $table->integer('organization')->unsigned();
            $table->timestamps();

            $table->unique(['rol_title']);
        });

        Schema::create('users', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('user_firstname', 50);
            $table->string('user_lastname', 50);
            $table->string('user_password');
            $table->string('user_picture');
            $table->string('user_email');
            $table->integer('user_type')->unsigned();
            $table->integer('main_organization')->unsigned();
            $table->dateTime('last_login');
            $table->timestamps();

            $table->unique(['user_email']);
        });

        Schema::create('patients', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->char('patient_identity', 10);
            $table->string('patient_firstname');
            $table->string('patient_age');
            $table->string('patient_gender');
            $table->dateTime('last_interaction');
            $table->timestamps();

            $table->unique(['patient_identity']);
        });

        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('room_identity', 25);
            $table->string('room_name');
            $table->timestamps();

            $table->unique(['room_identity']);
        });

        Schema::create('procedures', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('procedure_title', 60);
            $table->integer('procedure_sequence')->unsigned();
            $table->timestamps();
        });

        Schema::create('activity_procedures', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->string('activity_title', 200);
            $table->string('activity_recordatory');
            $table->integer('activity_id')->unsigned();
            $table->integer('activity_level');
            $table->integer('activity_type');
            $table->integer('activity_sequence')->unsigned();
            $table->timestamps();
        });

        Schema::create('messages', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('type');
            $table->string('title_message', 250);
        });        

        Schema::create('logs_activities', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('type');
            $table->integer('main_activity')->unsigned();
            $table->integer('main_patient')->unsigned();
            $table->integer('main_user')->unsigned();
            $table->integer('main_room')->unsigned();
            $table->timestamps();
        });

        Schema::create('alerts_activities', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('alert_main')->unsigned();
            $table->integer('alert_main_patient')->unsigned();
            $table->integer('alert_type');
            $table->timestamps();
        });

        Schema::create('log_access', function (Blueprint $table) {
            $table->increments('id')->index();
            $table->integer('type');
            $table->integer('main_user')->unsigned();
            $table->timestamps();
        });        

        Schema::table('roles', function (Blueprint $table) {
            $table->foreign('organization')->references('id')->on('main_organizations');
        });

        Schema::table('users', function (Blueprint $table) {
            $table->foreign('user_type')->references('id')->on('roles')->onDelete('cascade');
            $table->foreign('main_organization')->references('id')->on('main_organizations');
        });

        Schema::table('activity_procedures', function (Blueprint $table) {
            $table->foreign('activity_id')->references('id')->on('procedures');
        });

        Schema::table('logs_activities', function (Blueprint $table) {
            $table->foreign('main_activity')->references('id')->on('activity_procedures');
            $table->foreign('main_patient')->references('id')->on('patients');
            $table->foreign('main_user')->references('id')->on('users');
            $table->foreign('main_room')->references('id')->on('rooms');
        });

        Schema::table('alerts_activities', function (Blueprint $table) {
            $table->foreign('alert_main')->references('id')->on('activity_procedures');
            $table->foreign('alert_main_patient')->references('id')->on('patients');
        });        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       Schema::dropAllTables();
    }
}
