FROM php:7.0-apache

WORKDIR /var/www
RUN a2enmod rewrite
RUN docker-php-ext-install pdo_mysql
RUN apt-get update
RUN apt-get install -y software-properties-common
RUN apt-get update
RUN apt-get install git -y
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN apt install zip unzip
RUN apt-get install vim -y
RUN apt-get install ssh openssh-server -y
RUN addgroup --system sftp_users
RUN adduser --home /home/meg_user --ingroup sftp_users meg_user
RUN mkdir /home/meg_sftp
RUN mkdir /home/meg_sftp/input
RUN mkdir /home/meg_sftp/output
RUN mkdir /home/meg_sftp/error
CMD ["bash", "/var/www/bin/init.sh"]