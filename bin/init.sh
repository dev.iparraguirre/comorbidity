#!/bin/bash

source ~/.bashrc
bash /var/www/bin/wait-for-it.sh "meg-db:3306"
echo "RUNONCE = $RUNONCE"
if [[ $RUNONCE == "" ]];
then
    chgrp -R www-data /var/www/storage /var/www/bootstrap/cache
    chmod -R ug+rwx /var/www/storage /var/www/bootstrap/cache
    touch /var/www/.env
    composer install
    echo "APP_ENV=local" >> .env
    echo "APP_DEBUG=true" >> .env
    echo "DB_HOST=meg-db" >> .env
    echo "DB_DATABASE=$DB_DATABASE" >> .env
    echo "DB_USERNAME=$DB_USERNAME" >> .env
    echo "DB_PASSWORD=$DB_PASSWORD" >> .env
    echo "APP_KEY=" >> .env
    php artisan key:generate
    php artisan migrate --force
    php artisan db:seed --force
    echo "export RUNONCE='TRUE'" >> ~/.bashrc
fi
service ssh restart
apache2-foreground
