<?php

namespace Modules\Modifiers;

use Smalot\PdfParser\Document as OriginDocument;

class Document extends OriginDocument  {

    /**
     * @param Page $page
     *
     * @return string
     */
    public function getText(Page $page = null)
    {
        $texts = array();
        $pages = $this->getPages();

        foreach ($pages as $index => $page) {
            if ($text = trim($page->getText())) {
                $texts[] = $text;
            }
        }

        return implode("\n\n", $texts);
    }

    public function getTextArray(Page $page = null)
    {
        $texts = array();
        $pages = $this->getPages();

        foreach ($pages as $index => $page) {
          $texts[$index] = $page->getTextArray();
        }

        return $texts;
    }    
}