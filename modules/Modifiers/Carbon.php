<?php

namespace Modules\Modifiers;

use Carbon\Carbon as OriginCarbon;

class Carbon extends OriginCarbon  {

  static function setlocale($locale) 
  {
    return parent::setLocale('es');
  }

  public function tz($value) 
  {
    return parent::tz('America/Lima');
  }
}