<?php

namespace Modules\Modifiers;

use Smalot\PdfParser\XObject\Form;
use Smalot\PdfParser\XObject\Image;
use Smalot\PdfParser\PDFObject as OriginPdf;
use Smalot\PdfParser\Header;
use Smalot\PdfParser\Pages;
use Smalot\PdfParser\Font;

use Modules\Modifiers\Document;

class PDFObject extends OriginPdf  {

    /**
     * @param $document Document
     * @param $header   Header
     * @param $content  string
     *
     * @return PDFObject
     */
    public static function factory(Document $document, Header $header, $content)
    {
        switch ($header->get('Type')->getContent()) {
            case 'XObject':
                switch ($header->get('Subtype')->getContent()) {
                    case 'Image':
                        return new Image($document, $header, $content);

                    case 'Form':
                        return new Form($document, $header, $content);

                    default:
                        return new PDFObject($document, $header, $content);
                }
                break;

            case 'Pages':
                return new Pages($document, $header, $content);

            case 'Page':
                return new Page($document, $header, $content);

            case 'Encoding':
                return new Encoding($document, $header, $content);

            case 'Font':
                $subtype   = $header->get('Subtype')->getContent();
                $classname = '\Smalot\PdfParser\Font\Font' . $subtype;

                if (class_exists($classname)) {
                    return new $classname($document, $header, $content);
                } else {
                    return new Font($document, $header, $content);
                }

            default:
                return new PDFObject($document, $header, $content);
        }
    }

	/**
	 * @param Page
	 *
	 * @return array
	 * @throws \Exception
	 */
	public function getTextArray(Page $page = null)
	{
		$text                = array();
    $sections            = $this->getSectionsText($this->content);
    $current_font        = new Font($this->document);

		foreach ($sections as $section) {

			$commands = $this->getCommandsText($section);

			foreach ($commands as $command) {

				switch ($command[self::OPERATOR]) {
					// set character spacing
					case 'Tc':
						break;

					// move text current point
					case 'Td':
						break;

					// move text current point and set leading
					case 'TD':
						break;

					case 'Tf':
						list($id,) = preg_split('/\s/s', $command[self::COMMAND]);
						$id           = trim($id, '/');
						$current_font = $page->getFont($id);
						break;

					case "'":
					case 'Tj':
						$command[self::COMMAND] = array($command);
					case 'TJ':
						// Skip if not previously defined, should never happened.
						if (is_null($current_font)) {
							// Fallback
              // TODO : Improve
							$text[] = $command[self::COMMAND][0][self::COMMAND];
							continue;
						}

						$sub_text = $current_font->decodeText($command[self::COMMAND]);
          
            $text[] = $sub_text;
                        
						break;

					// set leading
					case 'TL':
						break;

					case 'Tm':
						break;

					// set super/subscripting text rise
					case 'Ts':
						break;

					// set word spacing
					case 'Tw':
						break;

					// set horizontal scaling
					case 'Tz':
						//$text .= "\n";
						break;

					// move to start of next line
					case 'T*':
						//$text .= "\n";
						break;

					case 'Da':
						break;

					case 'Do':
						if (!is_null($page)) {
							$args = preg_split('/\s/s', $command[self::COMMAND]);
							$id   = trim(array_pop($args), '/ ');
							if ($xobject = $page->getXObject($id)) {
                $text[] = $xobject->getText($page);
							}
						}
						break;

					case 'rg':
					case 'RG':
						break;

					case 're':
						break;

					case 'co':
						break;

					case 'cs':
						break;

					case 'gs':
						break;

					case 'en':
						break;

					case 'sc':
					case 'SC':
						break;

					case 'g':
					case 'G':
						break;

					case 'V':
						break;

					case 'vo':
					case 'Vo':
						break;

					default:
				}
			}
		}

		return $text;
	}

}