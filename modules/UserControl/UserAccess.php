<?php

namespace Modules\UserControl;

use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Hash;

use Modules\Models\User;

use Session;
use Event;
use DB;

class UserAccess
{
    private $inputs;

    private $sessionsKeys = array();

    public function __construct($inputs = array())
    {
        $this->inputs = $inputs;
    }

    public function login()
    {
        $userEmail  = stripString($this->inputs['useremail']);
        $userAccess = User::ActionUser('SELECT' 
                         ,array('email', 'password', 'id', 'aes_decrypt(firstname, "'.env('APP_AESKEY').'") AS firstname', 'aes_decrypt(lastname, "'.env('APP_AESKEY').'") AS lastname', 'role_id')
                         ,array('= email' => "'$userEmail'", '= is_enabled' => 1 ));

        if (is_null($userAccess) || empty($userAccess))
            return false;

        if (!empty($userAccess && 
            Hash::check($this->inputs['userpassword'], $userAccess->password))) {
            if ($userAccess->role_id === 2) 
                $sessions['is_admin'] = 1;
                
            if (isset($this->inputs['remember'])) 
                $sessions['remember'] = 1;

            $sessions['user_pass'] = $userAccess->id. '/' .$userAccess->email;
            // $sessions['user_name'] = explode(' ', Crypt::decryptString($userAccess['firstname']))[0]. ' '. explode(' ', Crypt::decryptString($userAccess['lastname']))[0];
            $sessions['user_name'] = explode(' ', $userAccess->firstname)[0]. ' '. explode(' ', $userAccess->lastname)[0];

            Event::fire( new \Modules\Http\Events\AccesLogger());

            $sessions['lastActivityTime'] = \Carbon\Carbon::now('America/Lima');
            $activity = User::where('id', $userAccess->id)->update(['last_login' => $sessions['lastActivityTime']]);
            
            $this->setSessionData($sessions);

            return true;
        } else return false;
    }

    public function setSessionData($data = array()) 
    {
        foreach ($data as $key => $value) {
            if (!Session::has($key)) 
                Session::forget($key);

            Session::put($key, $value);
        }
    }

    public function validateIfHasLogin()
    {
        foreach (Session()->all() as $key => $value) {
            if (count($value) > 10) {}
        }
    }

    public static function getSessionData($key) 
    {
        $passKeys = ['id' => 0, 
                     'email' => 1];

        $session = Session::get('user_pass');

        return explode('/', $session)[$passKeys[$key]];
    }

    public static function decryptSession($stringHash, $transportHash = '')
    {
        return Hash::check($stringHash, $transportHash);
    }
}
