<?php

namespace Modules\UserControl;

use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Crypt;

use Modules\Models\User;

use Session;
use Event;
use DB;

class UserGestion
{
    private $inputs;

    private $sessionsKeys = array();

    private static $killSessions = false;

    public static function updateProfile($inputs = array(), $killAll=null, $wantsJson=false) 
    {
      if (!$wantsJson) {
        if (empty($inputs['password']) || 
            empty($inputs['old_password'])) 
          $inputs = clearInputsPattern(['password', 'old_password'], $inputs);
        else { 
          $userData = User::where('email',  $inputs['email'])->first();

          if (!Hash::check($inputs['old_password'], $userData->password)) {
            return [
              'message' => 'La contraseña actual no es correcta',
              'error' => true
            ];
          }
          if (!$killAll) self::$killSessions = true;

          $inputs['password'] = '"'. bcrypt($inputs['password']) .'"';
          unset($inputs['old_password']);
        }

        if (\Modules\UserControl\UserAccess::getSessionData('email') == $inputs['email']) 
          Session::put('user_name', explode(' ', $inputs['firstname'])[0] . ' '. explode(' ', $inputs['lastname'])[0]);

        $inputs['email'] = '"'.$inputs['email'].'"';
        $inputs['firstname'] = " aes_encrypt('".$inputs['firstname']."', '".env('APP_AESKEY')."')";
        $inputs['lastname'] = " aes_encrypt('".$inputs['lastname']."', '".env('APP_AESKEY')."')";

        $userUpdate = User::ActionUser('UPDATE', $inputs, array('= email' => $inputs['email']));

        if(!is_null($userUpdate))
          Session::flash('Success', returnHttpMessage('http', ['user_actions' => 1]));

        if (self::$killSessions)
          return [
            'message' => '',
            'kill' => self::$killSessions
          ];
        else  return [
          'message' => returnHttpMessage('http', ['user_actions' => 0])
        ];
      } 
      else {
        $userData = User::find($inputs['id']);
        $inputs = clearInputsPattern(['method', 'id'], $inputs);

        if (!is_null($userData)) {
          $userUpdate = User::ActionUser('UPDATE', $inputs, array('= email' =>  '"'. $userData->email .'"'));          

          return [
            'message' => '',
            'status' => 200
          ];
        }
        else die();
      }
    } 

    public static function rememberPassword($inputs = array(), $update=false, $firstLoggin=false, $userData=array()) 
    {
      if (!empty($inputs) && $update == false) {
        $hasUser = User::where('email', stripString($inputs['email']))->first();
        if (!is_null($hasUser))  {
          $existeToken = DB::table('tokens_url')->where(function($query) use ($hasUser) {
            $query->where('user', $hasUser->id);
            $query->where('status', 1);
          })->count();

          if (!is_null($existeToken) 
            && $existeToken > 0) 
            Session()->flash('Success', 'Ya tiene una solicitud, revise su correo electrónico.');
          else {
            Event::fire( new \Modules\Http\Events\RememberPassword($hasUser));
            Session()->flash('Success', returnHttpMessage('http', ['remember_password' => 0]));
          }
        }
      } else {

        if (!strcmp($inputs['password'], $inputs['new_password']) && 
            !empty( $inputs['password']) && !empty($inputs['new_password'])) {

          $inputs['password'] = bcrypt($inputs['password']);
          $user_id = $inputs['user_id'];

          $inputs = clearInputsPattern(['new_password', 'user_id'], $inputs);
          $user = User::where('id', $user_id)->update($inputs);

          if (!is_null($user) && $firstLoggin) {
              $sessions['user_pass'] = $user_id. '/' .$userData->email;
              $sessions['user_name'] = explode(' ', $userData->firstname)[0]. ' '. explode(' ', $userData->lastname)[0];
              $sessions['lastActivityTime'] = \Carbon\Carbon::now('America/Lima');
    
              $set = new UserAccess();
              $set->setSessionData($sessions);

            return ['status' => 1];
          } 
        } return array();
      }
    }

    public static function addedUser($inputs = array(), $isAdmin=null) 
    {
      if (is_null(User::where(['email' => $inputs['email']])->first())) {
        $userEmail = $inputs['email'];
        $newUser = User::ActionUser('INSERT' 
                                    ,array('firstname' => " aes_encrypt('".$inputs['firstname']."', '".env('APP_AESKEY')."')", 'lastname' => "aes_encrypt('".$inputs['lastname']."', '".env('APP_AESKEY')."')", 'password' => "''", 'picture' => "''", 'organization_id' => 1, 'email' => "'$userEmail'", 'role_id' => $inputs['role_id']) );
  
        if (!is_null($newUser)) {
          $inputs['id'] = User::ActionUser('SELECT', array('MAX(id) AS id'), array(), array('ORDER BY' => array('created_at' => 'DESC')))->id;
          Event::fire( new \Modules\Http\Events\NewUserRegister($inputs));

          return ['status' => 1];
        }
        else 
          return array();
      } 
      else 
        return array();
    }
}
