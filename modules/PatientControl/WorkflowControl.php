<?php

namespace Modules\PatientControl;

use Modules\Models\Activity;
use Modules\Modifiers\Carbon;

use Session;
use DB;

class WorkflowControl
{
    private $inputs;
    private $todayDate;
    private $haveErorrs = false;
    private $diferentialAlerts = array(
        1 => 'times',
        'hours'
    );

    protected static $levelAlerts = array(
        1 => array(
          'time' => [4],
          'max_time' => [4]
        ),
        2 => array(
          'time' => [24],
          'max_time' => [36]
        ),
        3 => array(
          'time' => [4],
          'max_time' => [8]
        ),
      );

    public function __construct($inputs = array())
    {
        $this->inputs    = $inputs;
        $this->todayDate = Carbon::now();
    }

    public function saveChecklist($device = null, $mainPatient = null, $mainRoom = null)
    {
        $parsedInputs = array();
        $parsedAlerts = array();
        $iterator     = 0;
        $activityGenerateAlert = null;

        if (is_null($mainPatient) || 
            is_null($mainRoom)) 
            $this->saveChecklist = true;
        else {
            foreach (array_keys($this->inputs) as $item) {
                if ($item !== '_token') {
                    $mainId = explode('-', $item)[1];
                    $activity = Activity::where('id', $mainId)->firstOrFail();
    
                    if (count($this->inputs[$item]) > 1 &&
                        $activity->has_alert == 1) {
                        $parsedAlerts[$iterator]['activity_procedures_id'] = (int) $mainId;
                        $parsedAlerts[$iterator]['alert_type_id'] = $activity->procedures_id;
                        $parsedAlerts[$iterator]['message_id'] = "$activity->messages_id";
                        $parsedAlerts[$iterator]['device_id'] = $device;
                        $parsedAlerts[$iterator]['status'] = 1;
                        $parsedAlerts[$iterator]['patients_id'] = $mainPatient;
                        $activityGenerateAlert = (count($this->inputs[$item]) > 1 ? 1 : 0);
                    }
                    
                    $parsedInputs[$iterator]['activity_procedures_id'] = $mainId;
                    $parsedInputs[$iterator]['patient_id']  = (!is_null($mainPatient) ? $mainPatient : 0);
                    $parsedInputs[$iterator]['generate_alert'] = $activityGenerateAlert ?: $activityGenerateAlert = 0;
                    $parsedInputs[$iterator]['device_id']   = $device;
                    $parsedInputs[$iterator]['user_id']     = \Modules\UserControl\UserAccess::getSessionData('id');
                    $parsedInputs[$iterator]['room_id']     = $mainRoom;
                    $iterator++;
                }
    
                $activityGenerateAlert = null;
            }

            $result = Activity::SaveCheckActivity($parsedInputs);
            $logActivity = Activity::InsertAlerts($parsedAlerts);
    
            Session()->flash('Success', true);
        }

        return (
            $activityGenerateAlert ? false : $result);
    }

    public function calculateProcedures($deviceId=null, $mainPatientId=null)
    {
        $configProcedures = DB::table('config_platform')->first();
        $dashboardAlerts = explode('-', $configProcedures->alerts_dashboard); 
        $returnArray = array();

        if (is_array($dashboardAlerts)) {
            foreach ($dashboardAlerts as $index => $itemAlert) {
                if ((int) $itemAlert !== 0) {
                    if (strlen($itemAlert) > 1)
                        list($mainProcedure, $mainType) = explode('/', $itemAlert);
                    else $mainProcedure = $itemAlert;

                    $alertProcedure = DB::table('logs_activities')
                    ->join('activity_procedures', function($join) use ($mainProcedure) {
                        $join->on('logs_activities.activity_procedures_id', '=', 'activity_procedures.id')
                        ->where([
                            ['activity_procedures.has_alert', '=', 1],
                            ['activity_procedures.id', '=', $mainProcedure]
                        ]);
                    })
                    ->join('procedures', function($join) use ($mainProcedure) {
                        $join->on('activity_procedures.procedures_id', '=', 'procedures.id');
                    })
                    ->where([
                        ['logs_activities.generate_alert', 1],
                        ['logs_activities.device_id', $deviceId],
                        ['logs_activities.patient_id', $mainPatientId]
                    ])
                    ->orderBy('logs_activities.id', 'DESC')
                    ->select('logs_activities.created_at', 'activity_procedures.alert_type_id', 'activity_procedures.title', 'activity_procedures.id')
                    ->first();

                    if (!is_null($alertProcedure)) {
                        if (strlen($itemAlert) < 2) 
                            $alertProcedure->alert_type_id = 2;

                        switch ($alertProcedure->alert_type_id) {
                            case $this->diferentialAlerts[$alertProcedure->alert_type_id] === 'hours':
                                $createdDate = Carbon::parse($alertProcedure->created_at);
                                $dateAgo =  Carbon::now()->diffForHumans($createdDate);
                                $returnArray[] = explode(' ', $dateAgo);
                            break;
                            case $this->diferentialAlerts[$alertProcedure->alert_type_id] === 'times':
                                $groupInserctions = DB::table('logs_activities')
                                ->where([
                                    ['logs_activities.generate_alert', 1],
                                    ['logs_activities.device_id', $deviceId],
                                    ['logs_activities.activity_procedures_id', $alertProcedure->id],
                                    ['logs_activities.patient_id', $mainPatientId]
                                ])
                                ->groupBy('id', 'generate_alert', 'patient_id')
                                ->select('id', 'generate_alert', 'patient_id')
                                ->get()
                                ->count();
                            
                                $returnArray[] = [$groupInserctions];
                            break;
                        }
                    } else  $returnArray[] = '';
                } else  $returnArray[] = '';  
            }
        }
        return ( 
            $returnArray);
    }

    public static function displayAlerts($alertsObj = array()) 
    {
        static $returnParse = array();
        if (!is_array($alertsObj) && !empty($alertsObj)) {
            $nowDateCompare = Carbon::now();

            foreach ($alertsObj as $key => $item) {
                $timeSelected = self::$levelAlerts[$item->alert_type_id];
                if (!is_null($timeSelected) ) {
                    $alertDateCompare = Carbon::parse($item->created_at);
                    $diff = $nowDateCompare->diffInHours($alertDateCompare);

                    foreach ($timeSelected['time'] as $time) {
                        switch ($time) {
                            case ($time == 24 || $time == 36):
                                if ($diff >= $time) {
                                    $returnParse[$key]['diff_inhours'] = "$diff Horas";
                                    $returnParse[$key]['title_message'] = str_replace(':x', $diff, $item->title_message);
                                    $returnParse[$key]['created_at'] = $item->created_at;
                                    $returnParse[$key]['status'] = $diff > 4 && $item->status != 0 ? 'warning' : '';  
                                }
                            break;
                            case 8:
                                if ($diff >= $timeSelected['max_time'][0]) {
                                    $returnParse[$key]['diff_inhours'] = "$diff Horas";
                                    $returnParse[$key]['title_message'] = str_replace(':x', $diff, $item->title_message);
                                    $returnParse[$key]['created_at'] = $item->created_at;
                                    $returnParse[$key]['status'] = $diff > 4 && $item->status != 0 ? 'warning' : '';  
                                }
                            break;
                            case 4:
                                $max_time = (int) $timeSelected['max_time'][0];
                                if (!strpos($item->title_message, 'llena')) {
                                    $diff     = (int) $diff;
                                    if ($diff >= 4 && $diff <= $max_time) {
                                        $returnParse[$key]['diff_inhours'] = "$diff Horas";
                                        $returnParse[$key]['title_message'] = str_replace(':x', ($max_time-$diff), $item->title_message);
                                        $returnParse[$key]['created_at'] = $item->created_at;
                                        $returnParse[$key]['status'] = $diff > 4 && $item->status != 0 ? 'warning' : '';
                                    }
                                } else {
                                    if ($diff >= $max_time) {
                                        $returnParse[$key]['diff_inhours'] = "$diff Horas";
                                        $returnParse[$key]['title_message'] = str_replace(':x', $diff, $item->title_message);
                                        $returnParse[$key]['created_at'] = $item->created_at;
                                        $returnParse[$key]['status'] = $diff > 4 && $item->status != 0 ? 'warning' : '';
                                    }
                                }
                            break;
                        }
                    }
                }
            }
            return (
               empty($returnParse) ? [] : $returnParse);

        } else  die();
    }
}
