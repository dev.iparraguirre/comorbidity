<?php

namespace Modules;

use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Route;

class MegRouteServiceProvider extends ServiceProvider
{

    protected $namespace = 'Modules\Http\Controllers';

    public function boot()
    {
        parent::boot();

        $patterns = array(
            'procedureId' => '[0-9]+',
            'patientId'   => '[0-9]+',
            'device'      => '[0-9]+',
            'room'        => '[0-9]+',
        );

        Route::patterns($patterns);
    }

    public function map()
    {
        $this->mapWebRoutes();
    }

    protected function mapWebRoutes()
    {
        Route::middleware('web')
            ->namespace($this->namespace)
            ->group(__DIR__ . '/Http/Routes.php');
    }
}