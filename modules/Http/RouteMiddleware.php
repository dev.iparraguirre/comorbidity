<?php

namespace Modules\Http;

use Modules\Models\User;
use Modules\Modifiers\Carbon;

use Closure;
use Session;
use Cookie;

class RouteMiddleware
{
    const SESSION_LOGOUT = 20;

    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @param  string|null  $guard
     * @return mixed
     */
    public function handle($req, Closure $next, $guard = null)
    {
        if (Session::has('lastActivityTime')) {
            $lastLogin =  Carbon::now()->diffInMinutes(Session::get('lastActivityTime'));
            if (!Session::has('remember') && $lastLogin > self::SESSION_LOGOUT) {
                Session::flush();
            
                return ( redirect('/'));
            }

            // Session::put('lastActivityTime', Carbon::now());
            $userSessionId = Session::has('user_pass') ? \Modules\UserControl\UserAccess::getSessionData('id') : 0;
            $isAdmin = Session::has('is_admin') ? (int) Session::has('is_admin') : false;
        
            if (!$userSessionId &&
                !Session::has('user_pass')) {
                    Session::flush();
                    
                return ( redirect('/'));
            }
        
            if ($isAdmin != false) {
                $userMd = User::join('roles', 'users.role_id', '=', 'roles.id')
                            ->where(function( $query) use ($userSessionId, $isAdmin) {
                                $query->where('users.id', $userSessionId);
                                $query->where('users.role_id', 2);
                                $query->where('roles.id', 2);
                            })->firstOrFail();
        
                if ( is_null($userMd)) {
                    Session::flush();
                        
                    return ( redirect('/'));
               }
            }
            return ( $next($req));     
        } else return ( redirect('/'));   
        // }
    }
}
