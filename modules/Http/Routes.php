<?php

Route::match(['get', 'post'], '/', 'AccessController@loginAction');

Route::group([
    'middleware' => ['megroutes'],
    'prefix'     => '/core'], function () {
    Route::match(['get'], '/', 'ApplicationController@patientsAction');
    Route::match(['get'], '/encrypt', 'ApplicationController@encryptUsers');

    Route::match(['get'], '/logout', 'AccessController@logoutAction');
    Route::group(['prefix' => '/patients'], function () {
        Route::match(['get'], '/', 'ApplicationController@patientsAction');
        Route::match(['get', 'post'], '/view/{device?}/{patientId?}/{room?}/{category?}', 'ApplicationController@patientsAction');
        Route::match(['get', 'post'], '/add', 'ApplicationController@patientsAdded');
    });

    Route::group(['prefix' => '/procedures'], function () {
      Route::match(['get', 'post'], '/view/{procedureId?}', 'ApplicationController@updateProcedure');
  });

    Route::group(['prefix' => '/team'], function () {
        Route::match(['get', 'post'], '/list', 'ApplicationController@teamList');
        Route::match(['get', 'post'], '/add', 'ApplicationController@teamManagement');
        Route::match(['get', 'post'], '/management/{mainId?}', 'ApplicationController@teamManagement');
        Route::match(['get', 'post'], '/management/{mainId?}/main', 'ApplicationController@teamManagement');
    });
});

Route::match(['get', 'post'], '/remember_password/{token?}', 'AccessController@rememberPassword');
Route::match(['get', 'post'], '/update_password/{token?}', 'AccessController@updatePassword');



