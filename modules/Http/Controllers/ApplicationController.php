<?php

namespace Modules\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Crypt;

use Modules\Models\Activity;
use Modules\Models\Patient;
use Modules\Models\Procedure;
use Modules\Models\Room;
use Modules\Models\Rol;
use Modules\Models\User;
use Modules\Modifiers\Carbon;
use Modules\PatientControl\WorkflowControl;
use Modules\UserControl\UserAccess;

use Request;
use Session;
use Faker;
use Config;
use DB;

class ApplicationController extends BaseController
{
    private $returnValues = array();

    public function __construct () 
    {
        $this->returnValues['isMobile'] = false;

        if (!is_null(\Cookie::get('Mobile')) && \Cookie::get('Mobile') == 1) 
            $this->returnValues['isMobile'] = true;
    }

    public function patientsAction(Request $req, $typeProbe = null, $patientId = null, $mainRoom = null, $category = null)
    {
        $view_action = !is_null($category) ? 'patients_checkboxs' : 'patients_dashboard';
        $patients   = Patient::all();

        if (!is_null($typeProbe) && 
            !is_null($patientId)) {
 
            if (is_null(DB::table('devices')->find($typeProbe))) 
                return (
                    redirect()->to('/core')->withErrors(returnHttpMessage('http', ['filter_patients' => 0])));

            if (is_null(Patient::find($patientId))) 
                return (
                    redirect()->to('/core')->withErrors(returnHttpMessage('http', ['filter_patients' => 1])));             

            if (is_null(Room::find($mainRoom))) 
                return (
                    redirect()->to('/core')->withErrors(returnHttpMessage('http', ['filter_patients' => 2])));

            $this->returnValues['procedures'] = Procedure::GetProceduresBy($typeProbe);
            $this->returnValues['patientInfo'] = Patient::where('id', $patientId)->firstOrFail();
            $this->returnValues['detailsRoom'] = Room::where('id', $mainRoom)->firstOrFail();

            $patientsAlerts  = array();
            $this->returnValues['proceduresList'] = array();

            $objAlerts = Activity::GetAlerts($typeProbe, $patientId);
            $alerts = WorkflowControl::displayAlerts($objAlerts);

            foreach ($alerts as $index => $item) {
                $patientsAlerts[$index]['title_message'] = $item['title_message'];
                $patientsAlerts[$index]['created_at'] = $item['created_at'];
                $patientsAlerts[$index]['created_parse'] = $item['diff_inhours'];
                $patientsAlerts[$index]['status'] = $item['status'];
            }   

            Session::flash('countAlerts', count($alerts));
            // $alerts = Activity::UpdateAlerts($patientId);
            $this->returnValues['patientAlerts'] =$patientsAlerts;

            if ($req::method() === 'POST') {
                $inputs = clearInputsPattern(['_token'], $req::all());
                $workflow = new WorkflowControl($inputs);
                $status   = $workflow->saveChecklist($typeProbe, $patientId, $mainRoom);
                
                return (
                    redirect()->to("/core/patients/view/$typeProbe/$patientId/$mainRoom"));
            } else {
                $proceduresStatus = new WorkflowControl();
                $this->returnValues['alertsDashboard'] = $proceduresStatus->calculateProcedures($typeProbe, $patientId);
                $this->returnValues['requestParemeters'] = explode(',', "$typeProbe,$patientId,$mainRoom");
                $this->returnValues['styleOfCheckbox'] = array(0 => '', 2 => 'checkbox-guide', 'checkbox-action');
                foreach ($this->returnValues['procedures'] as $item) {
                    $titleProcedure = formatString($item->title);
                    if (!is_null($category) ) {
                        if ($category != $titleProcedure) 
                            $this->returnValues['proceduresList'][] = ['title' => $titleProcedure];
                    } else 
                        $this->returnValues['proceduresList'][] = ['title' => $titleProcedure];
                }

                if (!is_null($category)) {
                    $this->returnValues['activeProcedure'] = Procedure::FilterByColumn([$category => 'title']);
                    $checkLists = Activity::GetCheckLists($typeProbe, $this->returnValues['activeProcedure']->id);
                    $this->returnValues['activities'] = $checkLists;
                }
            }
        } else 
            $this->returnValues['rooms'] = Room::where('is_enabled', 1)->get();
            
        $this->returnValues['devices']  = Procedure::GetAllDevices();
        $this->returnValues['patients'] = $patients;

        return (
            view('modules_meg.' . $view_action, $this->returnValues));
    }

    public function patientsAdded(Request $req)
    {
        if ($req::method() === 'POST') {
            $inputs = clearInputsPattern(['_token'], $req::all());
            $addedPatient = Patient::CreateNewPatient($inputs);
            
            return (
                redirect()->back());
        }
        return (
            view('modules_meg.patients_added'));
    }
    
    public function teamList()
    {
        $this->returnValues['users']  = User::ActionUser('SELECT'
                                                            , array("CAST(CONCAT(AES_DECRYPT(firstname, '".env('APP_AESKEY')."'), ' ', AES_DECRYPT(lastname, '".env('APP_AESKEY')."')) as CHAR(50)) AS fullname", 'email', 'role_id', 'is_enabled', 'id')
                                                            , array('= role_id' => 1, '!= email' => '"'.\Modules\UserControl\UserAccess::getSessionData('email').'"')
                                                            , array('ORDER BY' => array("fullname" => 'ASC')), true);
                                                            
        $this->returnValues['admins'] = User::ActionUser('SELECT'
                                                            , array("CAST(CONCAT(AES_DECRYPT(firstname, '".env('APP_AESKEY')."'), ' ', AES_DECRYPT(lastname, '".env('APP_AESKEY')."')) as CHAR(50)) AS fullname", 'email', 'role_id', 'is_enabled', 'id')
                                                            , array('= role_id' => 2, '!= email' => '"'.\Modules\UserControl\UserAccess::getSessionData('email').'"')
                                                            , array('ORDER BY' => array('fullname' => 'ASC')), true);

        return (
            view('modules_meg.team_list', $this->returnValues));
    }   

    public function teamManagement(\Modules\Http\Requests\StoreUserRequest $req, $mainUserId = null, $section = null)
    {
        $fullPath = explode('/',$req->path());
        $wantsJson= false;

        $this->returnValues['session_email'] = \Modules\UserControl\UserAccess::getSessionData('email');
        $this->returnValues['full'] = Session::has('is_admin') ? 1 : 0;
        if (!$this->returnValues['full'] && end($fullPath) != 'main') 
            return ( redirect()->to('/core'));
        
        $this->returnValues['roles'] = Rol::where('is_enabled', 1)->orderBy('level')->get();
        $this->returnValues['section'] = end($fullPath);
        
        if (!is_null($mainUserId)) {
            if ($req->method() === 'POST') {
                $inputs = clearInputsPattern(['_token'], $req->all());

                if (isset($inputs['method']) && $inputs['method']=='ajax') 
                    $wantsJson = true;
                elseif ($req->ajax())
                    $wantsJson = true;
                    
                $update = \Modules\UserControl\UserGestion::updateProfile($inputs, $this->returnValues['full'], $wantsJson);

                if (!$wantsJson) {
                    if (isset($update['kill'])) {
                        return (
                            redirect()->to('/core/logout'));
                    } 
                    else if (isset($update['error']))
                        return (
                        redirect()->back()->withInput($inputs)->withErrors($update['message']));
                    else return (
                        redirect()->back()->withInput($inputs));
                } else return ( response()->json($update));
            }
            $this->returnValues['user'] = User::ActionUser('SELECT'
                                                            , array('id', 'email', 'is_enabled', 'role_id', "CAST(AES_DECRYPT(firstname, '".env('APP_AESKEY')."') AS CHAR(50)) AS firstname", "CAST(AES_DECRYPT(lastname, '".env('APP_AESKEY')."') AS CHAR(50)) AS lastname")
                                                            , array('= id' => $mainUserId));
        }   
        else {
            if (Session::has('is_admin')) {
                if ($req->method() === 'POST') {
                    $inputs = clearInputsPattern(['_token'], $req->all());
                    $newUser = \Modules\UserControl\UserGestion::addedUser($inputs);

                    if (!empty($newUser))  {
                        Session()->flash('Success', 'Usuario registrado.');

                        return (
                            redirect()->back());
                    } else return ( redirect()->back()->withInput($inputs)->withErrors('El usuario ya existe.'));
                }
            } else return ( redirect()->to('/core'));
        }

        return (
            view('modules_meg.team_added', $this->returnValues));
    }

    public function updateProcedure(Request $req, $procedureId =null) 
    {
      if ($req::method() === 'POST') {
        $inputs = clearInputsPattern(['_token'], $req::all());
        $inputs['id'] = $procedureId;

        $activity = Activity::UpdatestatusActivity($inputs);
        return (
          redirect()->back());
      }

      $this->returnValues['details']   = Activity::where('id', $procedureId)->firstOrFail();
      return (
        view('modules_meg.update_procedure', $this->returnValues));
    }

    public function encryptUsers() 
    {
			$db = User::all();
			$userData = array();
			foreach ($db as $index => $user) {
				try {
					$userData[$index]['firstname'] = Crypt::decryptString($user->firstname);
					$userData[$index]['lastname']  = Crypt::decryptString($user->lastname);
					$userData[$index]['id'] 			 = $user->id;
				} catch (\Illuminate\Contracts\Encryption\DecryptException  $e) { }
			}
			
			foreach ($userData as $user) {
				$updateUser = User::ActionUser('UPDATE' ,array('firstname' => " aes_encrypt('".$user['firstname']."', '".env('APP_AESKEY')."')", 'lastname' => "aes_encrypt('".$user['lastname']."', '".env('APP_AESKEY')."')"), array('= id' =>  '"'. $user['id'] .'"') );
				if (is_null($updateUser)) die('Ocurrio un error al actualizar los datos del usuario'. $user['id']);
			}

			return ( redirect()->to('core/logout'));
    }
}
