<?php

namespace Modules\Http\Controllers;

use Illuminate\Routing\Controller as BaseController;
use Illuminate\Support\Facades\Crypt;

use Modules\UserControl\UserAccess;
use Modules\Models\User;
use Modules\Modifiers\Carbon;

use Request;
use Session;
use Validator;
use DB;

class AccessController extends BaseController
{
    const TOKEN_LIMIT = 2;

    private $returnValues = array();

    public function loginAction(Request $req)
    {
        if (!Session::has('user_pass')) {
            if ($req::method() === 'POST') {
                if (!$req::has('remember')) 
                    \Config::set('lifetime', 20 );

                $inputs = clearInputsPattern(['_token'], $req::all());

                $validator = Validator::make($inputs, [
                    'useremail' => 'required|email',
                    'userpassword' => 'required|max:20',
                ], ['required'   => 'Complete todos los campos requeridos.',
                    'useremail.email' => 'Debe ingresar un correo electrónico válido.',
                    'between'        => 'La contraseña no debe superar los :max dígitos.']);
    
                if (!$validator->fails()) {
                    $userAccess = new UserAccess($inputs);
                    $haveLogged = $userAccess->login();
                    return (
                        $haveLogged ? redirect('/core') : redirect()->back()->withErrors('Datos de acceso incorrectos.'));
                } else 
                    return (
                        redirect()->back()->withErrors($validator));
            }
        } 
        else return (
                redirect('/core'));

        return (
            view('modules_meg.login'));
    }

    public function rememberPassword(Request $req, $token=null)
    {
        $returnView = is_null($token) ? 'remember' : 'change';

        if (!is_null($token)) {
            $this->returnValues['token_details'] = DB::table('tokens_url')->where(function($query) use ($token) {
                $query->where('token', $token);
                $query->where('status', 1);
            })->first();
            
            if (!is_null($this->returnValues['token_details'])) {
                $tokenParse = Carbon::parse($this->returnValues['token_details']->created_at);

                if (Carbon::now()->diffInHours($tokenParse) >= self::TOKEN_LIMIT) {
                    DB::table('tokens_url')->where('token', $token)->update(['status' => 3]);

                    return $this->logOutAction('remember_password', ['Error' => 'El enlace expiró. Ingrese nuevamente su correo electrónico.']);
                }
                
                if ($req::method() === 'POST') {
                    $inputs = clearInputsPattern(['_token'], $req::all());
                    $this->returnValues['userData'] = User::ActionUser('SELECT'
                    , array("AES_DECRYPT(firstname, '".env('APP_AESKEY')."') AS firstname,  AES_DECRYPT(lastname, '".env('APP_AESKEY')."') AS lastname", 'password', 'email', 'is_enabled', 'id')
                    , array('= id' => $inputs['user_id']));
                    

                    dd($this->returnValues['userData']);
                    if (!is_null($this->returnValues['userData'])) {
                        $update = \Modules\UserControl\UserGestion::rememberPassword($inputs, true, true, $this->returnValues['userData']);

                        if (!empty($update))  {
                            DB::table('tokens_url')->where('token', $token)->update(['status' => 0]);
                            Session()->flash('Success', 'Cambio de contraseña con éxito, puede acceder.');
                            
                            return redirect('/');
                        } else return (
                            redirect()->back()->withErrors(['Asegurese de ingresar las contraseñas correctamente.']));
                    } else return $this->logOutAction();
                }
            } else return $this->logOutAction();
        } else {
            if ($req::method() === 'POST') {
                $inputs = clearInputsPattern(['_token'], $req::all());
                $existUser = User::where('email', $inputs['email'])->first();

                if (is_null($existUser)) 
                    return (
                        redirect()->back()->withErrors('El correo electrónico no está registrado.'));
                else {
                    $update = \Modules\UserControl\UserGestion::rememberPassword($inputs);
                    Session::put('success', 'Se envio un mensaje para realizar el cambio de contraseña.');

                    return (
                        redirect()->back());
                }
            }            
        }

        return (
            view('modules_meg.password.'.$returnView, $this->returnValues));
    }

    public function updatePassword(Request $req, $token=null) 
    {
        if (!is_null($token)) {

            $token = (int) Crypt::decryptString($token);
            $inputs = clearInputsPattern(['_token'], $req::all());
            
            $this->returnValues['userData'] = User::ActionUser('SELECT'
            , array("AES_DECRYPT(firstname, '".env('APP_AESKEY')."') AS firstname,  AES_DECRYPT(lastname, '".env('APP_AESKEY')."') AS lastname", 'email', 'is_enabled', 'id', 'password')
            , array('= id' => $token));

            if (is_null($this->returnValues['userData']) || 
                !empty($this->returnValues['userData']->password))
                return redirect('/');

            if ($req::method() === 'POST') {
                $updatePassword = \Modules\UserControl\UserGestion::rememberPassword($inputs, true, true, $this->returnValues['userData']);

                if (!empty($updatePassword))  {
                    Session::flush();
                    Session()->flash('Success', 'Bienvenido(a), sus datos fueron actualizados.');
                    
                    return (
                        redirect('/core'));
               } else return (
                    redirect()->back()->withErrors(['Asegurese de ingresar las contraseñas correctamente.']));
            } else return (
                view('modules_meg.password.update', $this->returnValues)); 
            
        } return redirect('/');
    }

    public function logOutAction($path= null, $flashSessions = array())
    {
        Session::flush();
        if (\Cookie::has('mobile'))
            \Cookie::forget('mobile');

        if (!empty($flashSessions)) {
            foreach ($flashSessions as $type => $session) {
                Session::flash($type, $session);
            }
        }

        return (
            redirect(!is_null($path) ? $path : '/'));
    }

    public function profileEdit()
    {
       return (
                view('modules_meg.user_profile'));
    }    
}
