<?php

namespace Modules\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Modules\Models\Rol;

class StoreUserRequest extends FormRequest
{
    public function authorize()
    {
        return true;
    }

    public function rules()
    {
        if ($this->method() == 'POST') {

            $rols = Rol::get();
            $roles_id = '';
            foreach ($rols as $rol)
                $roles_id .= (strlen($roles_id) > 0 ? ',' : '') . $rol->id;

            if (!$this->has('old_password')) {
                if (!$this->ajax()) 
                    return ([
                        'firstname'  => 'required',
                        'lastname'   => 'required',
                        'email'      => 'required|email',
                        // 'password'   => 'required|min:5|max:20',
                        'role_id'    => 'required|integer|between:'.$roles_id,
                        // 'is_enabled' => 'required|integer'
                    ]);
                else return [
                    'is_enabled' => 'required|integer|between:0,1'
                    ];             
            }
            else {
                return [
                'firstname'  => 'required',
                'lastname'   => 'required',
                // 'password'   => 'required|min:5|max:20',
                // 'old_password'   => 'required|min:5|max:20'
                ];
            }
        } else return[];
    }

    public function messages() 
    {
        return [
            'firstname.required' => 'El Nombre es requerido.',
            'lastname.required' => 'Los Apellidos son requeridos.',
            'email.required'   => 'El Correo Electrónico es requerido.',
            'password.required'=> 'Debe ingresar una contraseña.',
            'old_password.required'=> 'Debe repetir la contraseña ingresada.',
            'role_id.required' => 'Debe seleccionar un Rol.',
            'email.email' => 'Debe ingresar un Correo Electrónico válido.',
            'password.max'=> 'La Contraseña no debe superar los :max digitos.',
            'password.min'=> 'La Contraseña no debe tener menos de :min digitos.',
            'old_password.max'=> 'La repetición de Contraseña no debe superar los :max digitos.',
            'role_id.between'=> 'Debe seleccionar un Rol válido.',
            'is_enabled.between'=> 'Debe seleccionar un Estado válido.'
        ];
    }
}
