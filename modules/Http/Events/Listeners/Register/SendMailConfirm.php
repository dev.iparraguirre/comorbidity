<?php

namespace Modules\Http\Events\Listeners\Register;

use Modules\Http\Events\NewUserRegister;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;
use Mail;

class SendMailConfirm
{
    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(NewUserRegister $event)
    {
      Mail::send('emails.confirm_new_user', ['data' => $event->inputsUser], function ($m) use ($event) {
        $toCc = array('deusdit.correa@tallertechnologies.com',
                      'jonatan.iparraguirre@tallertechnologies.com');

        $m->from(env('MAIL_FROM', 'no-reply@no-reply.com'), 'MEG Comorbidity Platform');

        $m->to($event->inputsUser['email'])->bcc($toCc)->subject('MEG - Bienvenido');
    });
    }
}
