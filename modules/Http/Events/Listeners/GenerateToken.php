<?php

namespace Modules\Http\Events\Listeners;

use Modules\Http\Events\RememberPassword;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;
use Session;
use Request;

class GenerateToken
{
    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(RememberPassword $event)
    { 
      $saveData['ip']     = Request::ip();
      $saveData['token']  = Session::token();
      $saveData['user']   = $event->userData->id;
      $saveData['status'] = 1;

      $event->tokenpassword = $saveData['token'];
      $saveToken = DB::table('tokens_url')->insert($saveData);
      
      Session::flush();
    }
}
