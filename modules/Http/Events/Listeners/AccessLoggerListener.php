<?php

namespace Modules\Http\Events\Listeners;

use Modules\Http\Events\AccesLogger;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;

class AccessLoggerListener
{
    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(AccesLogger $event)
    {

    }
}
