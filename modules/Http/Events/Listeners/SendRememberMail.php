<?php

namespace Modules\Http\Events\Listeners;

use Modules\Http\Events\RememberPassword;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

use DB;
use Mail;

class SendRememberMail
{
    /**
     * Handle the event.
     *
     * @param  Event  $event
     * @return void
     */
    public function handle(RememberPassword $event)
    {
        Mail::send('emails.restore_password', ['data' => $event], function ($m) use ($event) {
            $toCc = array('deusdit.correa@tallertechnologies.com',
                          'jonatan.iparraguirre@tallertechnologies.com');

            $m->from(env('MAIL_FROM', 'no-reply@no-reply.com'), 'MEG Comorbidity Platform');

            $m->to($event->userData->email)->bcc($toCc)->subject('MEG - Solicitud de cambio de contraseña');
        });
    }
}
