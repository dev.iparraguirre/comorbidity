<?php 

function stripString($string = '')
{
  return stripslashes(strip_tags($string));
}

function formatString($textFormatter) 
{
  $textFormatter = htmlentities($textFormatter, ENT_QUOTES, 'UTF-8');
  $textFormatter = strtolower($textFormatter);
  
  $pattern = array (
      '/\+/' => '',
      '/&agrave;/' => 'a',
      '/&egrave;/' => 'e',
      '/&igrave;/' => 'i',
      '/&ograve;/' => 'o',
      '/&ugrave;/' => 'u',

      '/&aacute;/' => 'a',
      '/&eacute;/' => 'e',
      '/&iacute;/' => 'i',
      '/&oacute;/' => 'o',
      '/&uacute;/' => 'u',

      '/&acirc;/' => 'a',
      '/&ecirc;/' => 'e',
      '/&icirc;/' => 'i',
      '/&ocirc;/' => 'o',
      '/&ucirc;/' => 'u',

      '/&atilde;/' => 'a',
      '/&etilde;/' => 'e',
      '/&itilde;/' => 'i',
      '/&otilde;/' => 'o',
      '/&utilde;/' => 'u',

      '/&auml;/' => 'a',
      '/&euml;/' => 'e',
      '/&iuml;/' => 'i',
      '/&ouml;/' => 'o',
      '/&uuml;/' => 'u',

      '/&auml;/' => 'a',
      '/&euml;/' => 'e',
      '/&iuml;/' => 'i',
      '/&ouml;/' => 'o',
      '/&uuml;/' => 'u',

      '/&aring;/' => 'a',
      '/&ntilde;/' => 'n',
  );

  return $textFormatter = preg_replace(array_keys($pattern), array_values($pattern), $textFormatter);
}


function clearInputsPattern($filters = array(), $inputs= array()) 
{
  foreach ($filters as $index => $item) {
    if (array_key_exists($item, $inputs)) 
      unset($inputs[$item]);
  }

  return $inputs;
}