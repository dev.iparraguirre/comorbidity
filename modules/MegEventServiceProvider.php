<?php

namespace Modules;

use Illuminate\Support\Facades\Event;
use Illuminate\Foundation\Support\Providers\EventServiceProvider as ServiceProvider;

class MegEventServiceProvider extends ServiceProvider
{
    /**
     * The event listener mappings for the application.
     *
     * @var array
     */
    protected $listen = [
        'Modules\Http\Events\AccesLogger' => [
             'Modules\Http\Events\Listeners\AccessLoggerListener'
        ],
        'Modules\Http\Events\RememberPassword' => [
            'Modules\Http\Events\Listeners\GenerateToken',
            'Modules\Http\Events\Listeners\SendRememberMail'
       ],
        'Modules\Http\Events\NewUserRegister' => [
            'Modules\Http\Events\Listeners\Register\SendMailConfirm',
       ],
    ];

    /**
     * Register any events for your application.
     *
     * @return void
     */
    public function boot()
    {
        parent::boot();
    }
}
